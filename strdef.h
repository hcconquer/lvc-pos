/*
 * Declare all character string pointer
 */

#include "typedef.h"

#ifndef _STRDEF_H
#define _STRDEF_H

extern const uchar *Yes;
extern const uchar *No;
extern const uchar *SureExit;
extern const uchar *Userid;
extern const uchar *Passwd;
extern const uchar *UserIdErr;
extern const uchar *PasswdErr;
extern const uchar *KaiFa;

extern const uchar *Project;
extern const uchar *PosName;

extern const uchar *ReadDataStr;
extern const uchar *LVCParamStr;
extern const uchar *MaintainLVCStr;
extern const uchar *ArchivesStr;
extern const uchar *DataManageStr;
extern const uchar *SystemManageStr;

extern const uchar *SetVPNModeStr;
extern const uchar *SetVPNIdPwdStr;
extern const uchar *SetIPPortAPNStr;
extern const uchar *SetPhoneMsgNumStr;

extern const uchar *LVCStatStr;
extern const uchar *RdDiagnoseInfoStr;
extern const uchar *SetLVCClockStr;
extern const uchar *QueryLVCRtcStr;
extern const uchar *ResetLVCStr;

extern const uchar *ResetMainBoardStr;
extern const uchar *ResetPlcBoardStr;
extern const uchar *ResetWirelessModemStr;

extern const uchar *MtArchivesStr;
extern const uchar *ClientArchivesStr;

extern const uchar *SetLVCAddrStr;
extern const uchar *QueryLVCAddrStr;
extern const uchar *SetUpCommParamStr;
extern const uchar *RdUpCommParamStr;
extern const uchar *SetCasLVCCommParamStr;
extern const uchar *RdCasLVCCommParamStr;
extern const uchar *SetCascadeLVCAddrStr;
extern const uchar *RdCascadeLVCAddrStr;

extern const uchar *QueryAutoFlagStr;
extern const uchar *QuerySoftHardVersionStr;
extern const uchar *QueryProtocolVersionStr;
extern const uchar *QueryChooseMtCntStr;
extern const uchar *QueryFinishRdMtCntStr;
extern const uchar *QueryStatMeterCntStr;

extern const uchar *AddMtArchStr;
extern const uchar *DelMtArchStr;
extern const uchar *RdMtArchStr;
extern const uchar *QueryMtArchByIdxStr;
extern const uchar *QueryMtArchByNumStr;  

extern const uchar *ManageMeasPointArchStr;
extern const uchar *QueryClientRdMtPeriodStr;
extern const uchar *SetClientRdMtPeriodStr;
extern const uchar *QueryClientFrozenParamStr;
extern const uchar *SetClientFrozenParamStr;

extern const uchar *ConfigMeasPointArchStr;
extern const uchar *RdMeasPointArchStr;
//extern const uchar *QueryMeasPointArchStr;
extern const uchar *DelMeasPointArchStr;

extern const uchar *MtDayFrozenEnergyStr;
extern const uchar *MtMonFrozenEnergyStr;
extern const uchar *MeasPointDataStr;
extern const uchar *CurrMtDataStr;

extern const uchar *MPCurrEngyMusterStr;
extern const uchar *MPCurrPlusEngyStr;
extern const uchar *MPClockStatWordStr;

extern const uchar *MtCurrEngyStr;
extern const uchar *MtClockStr;
extern const uchar *MtStatWordStr;

extern const uchar *QueryVPNModeStr;
extern const uchar *QueryVPNIdPwdStr;
extern const uchar *QueryIpPortAPNStr;
extern const uchar *QueryPhoneMsgNumStr;

extern const uchar *QueryCascadeLVCAddrStr;
extern const uchar *QueryLVCCommParamStr;

extern const uchar *CalibrateTimeStr;
extern const uchar *ModifyPwdStr;
extern const uchar *COMConfigStr;

extern const uchar *COMPortStr;
extern const uchar *COMBaudStr;

extern const uchar *IPADDR;
extern const uchar *PORT;

extern const uchar *QMeterSetting;
extern const uchar *QLVCSetting;
extern const uchar *QAlarm;
extern const uchar *QImportantCustomer;

extern const uchar *ViewDayFroDataStr;
extern const uchar *ClearDayFroDataStr;
extern const uchar *ViewMonFroDataStr;
extern const uchar *ClearMonFroDataStr;
extern const uchar *ClearMtArchStr;
/******************************************************************/
extern const uchar *Sending;

extern const uchar *Sunday;
extern const uchar *Monday;
extern const uchar *Tuesday;
extern const uchar *Wednesday;
extern const uchar *Thursday;
extern const uchar *Friday;
extern const uchar *Saturday;

extern const uchar *OldPasswd;
extern const uchar *NewPasswd;
extern const uchar *TypeAgain;
extern const uchar *ModifyPasswdOk;
extern const uchar *PasswdNotMatch;
/******************************************************************/
extern const uchar *YesNo;
/******************************************************************/
void ChineseVerInit(void);


#endif // end ifndef _STRDEF_H
#ifndef _INPUT_H
#define _INPUT_H

#include "typedef.h"

#define EXIT_VALUE 0xbb
#define AUTOSKIP	1
#define NOAUTOSKIP	0

void KeyCheckTimeout(uint TenMsec);
uchar KeyCheck(void);
void KeyWait(void);
void KeyWaitFree(void);
uchar KeyScan(void);
uchar LcdInput(uchar *buf0, uchar rol, uchar col,uchar length,uchar flag, boolean isAutoSkip);

#endif	// end ifndef _INPUT_H
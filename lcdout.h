#ifndef _LCDOUT_H
#define _LCDOUT_H

#include "typedef.h"

void LcdStart(void);
void ClearWindow(int StartX, int StartY, int EndX, int EndY);
void LcdPrintC(uchar x_line,uchar y_colum,uchar ascii);
void LcdTypeNum(uchar isRow,uchar isCol, uchar isNum);
void LcdTypeMem(uchar isRow,uchar isCol,uchar isMem);
void LcdPrintS(uchar isRow,uchar isCol,const uchar *isStr);
void LcdShowChar(uchar row, uchar col, uchar n, const uchar graph[][16]);
void ShowReverse(uchar StartLine, uchar StartCol, uchar EndLine, uchar EndCol);

#endif	// end ifndef _LCDOUT_H
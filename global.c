 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-11
 */
#include "typedef.h"
#include "global.h"

LastInputInfo LastInfo;
uchar GSetStr[7] = "\x02\x19\x30\x11\x17\x00";
uchar GAddr[5];
uchar GSEQ;
long GBaud;
unsigned int G_COMPort;
char G_UserID[10];
char G_PSW[10];

::Batch file, for Turbo C 2.0 and Borland C 3.1
set CC_HOME=D:/BORLANDC
set BIN=%CC_HOME%/BIN
set INCLUDE=%CC_HOME%/INCLUDE;./INCLUDE
set LIB=%CC_HOME%/LIB;./LIB

set PATH=%PATH%;%BIN%
set CC=%BIN%/bcc

set DIST=Release
set TARGET=pos.exe

set CFLAGS=-c -mm -O2 -G -I%INCLUDE% -n%DIST%
set LFLAGS=-mm -L%LIB% -n%DIST%

if not exist %DIST% mkdir %DIST%
%CC% %CFLAGS% *.c
%CC% -e%TARGET% %LFLAGS% %DIST%/*.obj

 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-02-03
 */
 
#ifndef _FUNC_
#define _FUNC_

#include "typedef.h"

int FillQueryLVCData(int fn, uchar *data, uchar ReadMode);
int FillQueryLVCAddrData(uchar *data);
void LVCParam(void);
void Archives(void);
void MaintainLVC(void);
void ReadData(void);
void ManageData(void);
void ManageSys(void);


#endif
 

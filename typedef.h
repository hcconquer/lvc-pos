/*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 *
 *	Modify: 2008-09-21
 *				1.添加BITS,BOOL,BYTE,WORD,DWORD的定义
 */

#ifndef _TYPEDEF_
#define _TYPEDEF_

typedef unsigned char BITS;
typedef unsigned char BOOL;
typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long DWORD;

#define	uchar	unsigned char
#define	uint	unsigned int
#define	ulong	unsigned long

#ifndef SCREENX
#define SCREENX	16
#define SCREENY	8
#endif

#ifndef COM1
#define COM1 1
#define COM2 2
#endif

#define DEFAULT_ID		"1234"
#define DEFAULT_PSW		"1234"
#define DEFAULT_LVCADDR	"\xff\xff\xff\xff"
#define DEFAULT_COM		"COM1"
#define DEFAULT_BAUD	9600

#define COMMFAIL	0
#define SETFAIL		1
#define SETSUC		2
#define SAVEFAIL	3
#define SAVESUC		4
#define RECVPACKERR	5
#define EMPTY		6
#define DELSUC		7

#define COMMINGSTAT			0
#define DATAEMPSTAT			1
#define COMMFAILSTAT		2
#define SAVESUCSTAT			3
#define SAVEFAILSTAT		4
#define PACKERRSTAT			5
#define FEILVCNTOVER		6

#define RDFROZEN			0
#define RDMTARCH			1

#define NOTORDER			0
#define ORDER				1

#define NOSPAC				0
#define SPACE				1

#define TYPE_NUMBER			0
#define TYPE_FLOAT			1
#define TYPE_HEX			2
#define TYPE_ALPHANUMERIC	3
#define TYPE_PASSWORD		4

#define MtDayFroDBFName		"MtDayFro.dbf"
#define MtMonFroDBFName		"MtMonFro.dbf"
#define MtArchivesDBFName	"MtArch.dbf"
#define MeasPoArchDBFName	"MPArch.dbf"

#define DayFroRecLen	66 /*66 = 1+8+4+8+9+9+9+9+9  The length of a record in dbf file, including the 1 byte delete flag*/
#define MonFroRecLen	30
#define MtArchRecLen	23
#define MeasPointRecLen	18

#define FNPN_LEN		4

typedef enum boolean
{
	False = 0, True = 1
} boolean;

typedef struct
{
	uchar Userid[5];
	uint CollectorId;
	uchar MeterId;
	ulong Value;
	uchar Decimal;
	uchar Type;
	uint PulseConst;
	uchar Time_d[8];
} LastInputInfo;

typedef struct
{
	uchar userid[4];
	uchar passwd[4];
	uchar auth;
} USERINFO;

typedef struct
{
	uchar start; // start = 0xff.
	uchar flag; // upload to PC: flag = 0xaa, else flag = 0x00.
	uint zkjno; // master machine No.
	uint id; // collector No.
	uchar userid[4]; // user ID
	uint len; // data length
	uchar buf[600]; // data
} ZBCDATA;

typedef struct st_msg
{
	uchar code; /* link layer function code *//*Control Field*/
	uchar addr[5]; /* address*/
	uchar afn; /* application layer function code */
	uchar seq; /* frame sequence */
	uint size; /* size of data */
	uchar *data; /* pointer of the data */
	uchar aux_flag; /* exist flag, bit 0 for pw, bit 1 for ec, bit 2 for tp */
	uchar aux_pw[2]; /* additional field for password */
	uchar aux_ec[2]; /* additional field for event count */
	uchar aux_tp[6]; /* additional field for time stamp */
} ST_MSG;

union intval
{
	uint val;
	uchar d[2];
};

union longunit
{
	ulong four;
	uchar d[4];
};

#define DAYFIELDCNT		8/* 定义要创建的数据库的字段数 */
#define MONFIELDCNT		4/* 定义要创建的数据库的字段数 */
#define MTARCHFIELDCNT	9/* 定义要创建的数据库的字段数 */

/* user authority */
#define READ_PRIV    0x01
#define SETTIME_PRIV 0X02
#define SETMID_PRIV  0x04
#define SETID_PRIV   0x08
#define TOPC_PRIV    0x10

#define MAX_USER_NUM	256
#define MAX_COLLECTOR_NUM	256

#define LowToHigh	1
#define HighToLow	0

#define FPARAM_CON_ADDRESS      0x9001

#define DIR_BIT             (0 << 7)
#define PRM_BIT             (1 << 6)
#define FCB_BIT				(0 << 5)
#define FCV_BIT				(0 << 4)

#define TPV_BIT				(0 << 7)
#define FIR_BIT				(1 << 6)
#define FIN_BIT				(1 << 5)
#define CON_BIT				(1 << 4)

#define mod_8N1	3		/* 8 bits no check bit. */
#define mod_7E1	147		/* 7 bits 1 even */
#define mod_8E1 179		/* 8 bits 1 even */
#define RECVTIMEOUT_LONG	6000			/* 6 seconds maximum time between 2 characters */
#define RECVTIMEOUT_SHORT	3000

#endif

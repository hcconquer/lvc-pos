/*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <conio.h>
#include <mem.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <dir.h>
#include "htxlcd.h"
#include "lcdout.h"
#include "input.h"
#include "func.h"
#include "strdef.h"
#include "lvc_pos.h"
#include "global.h"
#include "commplus.h"
#include "common.h"
#include "menu.h"
#include "dbf.h"
#include "rs232.h"

extern struct FIELD_RECORD MtDayFroRecField[DAYFIELDCNT];
extern struct FIELD_RECORD MtMonFroRecField[MONFIELDCNT];
extern struct FIELD_RECORD MtArchivesField[MTARCHFIELDCNT];

#define VERSION	"CS Ver1.03"
uchar pointer[4];
uchar rcvbuf[200];

static void ShowTitle(void)
{
	uchar i;
	const uchar BeFlgKF[2][16] =
	{
	{ 0x00, 0x00, 0xFE, 0xFC, 0xF9, 0xF3, 0xE7, 0xCF, 0xE7, 0xF3, 0xF9, 0xFC,
			0xFE, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x7E, 0xFE, 0xFE, 0xF0, 0xF0, 0xFC, 0xFE, 0xFC, 0xF0, 0xF0,
			0x70, 0x00, 0x00, 0x00 }, };

	LcdStart();
	for (i = 0; i < SCREENY; i++)
	{
		LcdPrintS(i, 0, "※");
		LcdPrintS(i, 14, "※");
	}
	LcdShowChar(1, 4, 2, BeFlgKF);
	LcdPrintS(1, 6, KaiFa);
	LcdPrintS(3, 4, Project);
	LcdPrintS(5, 3, PosName);
	LcdPrintS(7, 3, VERSION);
}

/*boolean chkpass(void)
 {
 uchar UseridAscii[5] = "    ", PasswdAscii[5] = "    ";

 GetLastInput(pointer, -1, LastInfo.Userid, 4);
 while (1) {
 LcdStart();
 LcdPrintS(2, 2, Userid);
 LcdPrintS(4, 2, Passwd);
 gotoxy(10, 3);
 pointer[0] = 0xff; // Non-auto display last userid
 LcdInput(pointer, 2, 10, 4, 0, NOAUTOSKIP);
 if (pointer[0] == EXIT_VALUE)
 return 0;
 BcdToAscii(UseridAscii, pointer, 2);
 LcdInput(pointer, 4, 10, 4, 1, NOAUTOSKIP);
 if (pointer[0] == EXIT_VALUE)
 return 0;
 BcdToAscii(PasswdAscii, pointer, 2);
 if (useridcmp(UseridAscii) == True) {
 memcpy(LastInfo.Userid, UseridAscii, 4);
 break;
 }
 LcdPrintS(7, 1, UserIdErr);
 pointer[0] = 0xff; // next input userid, not auto display.
 KeyWait();
 }
 while (1) {
 if (passwdcmp(PasswdAscii) == True)
 break;
 LcdPrintS(4, 10, "    ");
 LcdPrintS(7, 1, PasswdErr);
 KeyWait();
 LcdPrintS(7, 1, "             ");
 LcdInput(pointer, 4, 10, 4, 1, NOAUTOSKIP);
 if (pointer[0] == EXIT_VALUE)
 return 0;
 BcdToAscii(PasswdAscii, pointer, 2);
 }
 return 1;
 }*/

static boolean YesorNo(uchar rol_no, uchar col_no)
{
	uchar key = 0;

	LcdStart();
	LcdPrintS(rol_no, col_no, SureExit);
	LcdPrintS(7, 0, No);
	LcdPrintS(7, 13, Yes);
	while (1)
	{
		key = KeyScan();
		if (key == '\r' || key == 'y')
			return 1;
		else if (key == 27 || key == 'n')
			return 0;
	}
}

static int GetLVCAddr(char *CurrAddr)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;

	DataLen = FillQueryLVCAddrData(data);
	FillMsgOut(&MsgOut, 0x8a, data, DataLen, 0x00, 1, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	//TestBuf(PacketOut, PacketOutLen);
	LcdStart();
	LcdPrintS(1, 0, "读集中器地址...");
	OpenCom(G_COMPort, GBaud);
	Send(PacketOut, PacketOutLen);
	PacketInLen = Receive(PacketIn, 0x16, 0x16, 0, RECVTIMEOUT_SHORT);
	CloseCom(G_COMPort);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			return 0;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if (MsgIn.afn != 0x8a)
			return 0;
		else
			//memcpy(CurrAddr, MsgIn.data+6, 4);
			CopyLVCAddr(CurrAddr, MsgIn.data + 6, 4);
		return 1;
	}
	else
	{
		PrintCommStat(COMMFAIL);
		return 0;
	}
}

static int CheckBaud(uint baud)
{
	switch (baud)
	{
	case 1200:
		return 0;
	case 2400:
		return 0;
	case 4800:
		return 0;
	case 9600:
		return 1;
	case 19200:
		return 0;
	case 38400:
		return 0;
	case 57600:
		return 0;
	case 0xfff0:
		return 0;
	default:
		return -1;
	}
}

static void DBFInit(void)
{
	struct DBF DayFroDBF, MonFroDBF, MtArch;
	int errornum;
	struct ffblk ffblk;

	strcpy(DayFroDBF.filename, MtDayFroDBFName);
	if (findfirst(DayFroDBF.filename, &ffblk, 0) != 0) //原文件不存在
	{
		if ((errornum = d_create(&DayFroDBF, DayFroDBF.filename,
				MtDayFroRecField, DAYFIELDCNT)) != 0)
		{
			d_error(errornum, DayFroDBF.filename);
			exit(1);
		}
	}

	strcpy(MonFroDBF.filename, MtMonFroDBFName);
	if (findfirst(MonFroDBF.filename, &ffblk, 0) != 0) //原文件不存在
	{
		if ((errornum = d_create(&MonFroDBF, MonFroDBF.filename,
				MtMonFroRecField, MONFIELDCNT)) != 0)
		{
			d_error(errornum, MonFroDBF.filename);
			exit(1);
		}
	}

	strcpy(MtArch.filename, MtArchivesDBFName);
	if (findfirst(MtArch.filename, &ffblk, 0) != 0) //原文件不存在
	{
		if ((errornum = d_create(&MtArch, MtArch.filename, MtArchivesField,
				MTARCHFIELDCNT)) != 0)
		{
			d_error(errornum, MtArch.filename);
			exit(1);
		}
	}
}

static int GetINI(void)
{
	FILE *fp;
	char UserID[20] = "\0", PSW[20] = "\0", LVCAddr[20] = "\0", COM[20] = "\0",
			Baud[20] = "\0";
	char addr[20], baud[20], AddrHex[4];
	int i = 0, ret = 1;

	fp = fopen("LVCPOS.ini", "r");
	if (fp != NULL)
	{
		while (feof(fp) == 0)
		{
			i++;
			switch (i)
			{
			case 1:
				fgets(UserID, 20, fp);
				break;
			case 2:
				fgets(PSW, 20, fp);
				break;
			case 3:
				fgets(LVCAddr, 20, fp);
				break;
			case 4:
				fgets(COM, 20, fp);
				break;
			case 5:
				fgets(Baud, 20, fp);
				break;
			default:
				break;
			}
		}
	}
	fclose(fp);

	if ((UserID[0] == '\0') && (strlen(UserID) <= 8))
	{
		strcpy(G_UserID, DEFAULT_ID);
		ret = 0;
	}
	else
	{
		memcpy(G_UserID, UserID + 7, strlen(UserID) - 7 - 1);
		G_UserID[strlen(UserID) - 7 - 1] = '\0';
	}

	if ((PSW[0] == '\0') && (strlen(PSW) <= 5))
	{
		strcpy(G_PSW, DEFAULT_PSW);
		ret = 0;
	}
	else
	{
		memcpy(G_PSW, PSW + 4, strlen(PSW) - 4 - 1);
		G_PSW[strlen(PSW) - 4 - 1] = '\0';
	}

	if ((COM[0] == '\0') && (strlen(COM) <= 5))
	{
		G_COMPort = 1;
		ret = 0;
	}
	else
	{
		if (memcmp(COM + 4, "COM1", 4) == 0)
			G_COMPort = 1;
		else if (memcmp(COM + 4, "COM2", 4) == 0)
			G_COMPort = 2;
		else
			return 0;
	}

	if ((LVCAddr[0] == '\0') && (strlen(LVCAddr) != 17))
	{
		memcpy(GAddr, DEFAULT_LVCADDR, 4);
		GAddr[4] = 0x2c;
		ret = 0;
	}
	else
	{
		memcpy(addr, LVCAddr + 8, 8);
		if (memcmp(addr, "FFFFFFFF", 4) == 0)
		{
			memcpy(GAddr, addr, 4);
		}
		else
			StrToHex(GAddr, addr, 4);
		GAddr[4] = 0x2c;
	}

	if ((Baud[0] == '\0') && (strlen(Baud) <= 6))
	{
		GBaud = DEFAULT_BAUD;
		ret = 0;
	}
	else
	{
		if (IsFindEnter(Baud) == 0)
		{
			memcpy(baud, Baud + 5, strlen(Baud) - 5);
			baud[strlen(Baud) - 5] = '\0';
		}
		else
		{
			memcpy(baud, Baud + 5, strlen(Baud) - 5 - 1);
			baud[strlen(Baud) - 5 - 1] = '\0';
		}
		GBaud = atol(baud);
	}

	return ret;
}

static int LVCPosInit(void)
{
	char CurrLVCAddr[10], str[4] = "1";
	int cmd = 0, term = 0;

	LcdStart();
	printf("\n系统初始化...");

	DBFInit();
	GSEQ = 0x10;

	while (!term)
	{
		if (GetLVCAddr(CurrLVCAddr) == 1)
			break;
		else
		{
			do
			{
				LcdStart();
				printf("   初始化失败\n");
				printf("读集中器地址失败\n");
				printf("0:退出\n");
				printf("1:重试\n");
				printf("2:更改波特率\n");
				printf("3:以默认地址运行\n");
				printf("请选择:");
				if (GetString(str, 1, TYPE_NUMBER) == 0)
					return 0;
				cmd = atoi(str);
			} while (CheckRange((long) cmd, 0, 3) == 0);

			switch (cmd)
			{
			case 0:
				return 0;
			case 1:
				continue;
			case 2:
				SetCOMBaud();
				continue;
			case 3:
				memcpy(GAddr, DEFAULT_LVCADDR, 4);
				GAddr[4] = '\0';
				term = 1;
				break;
			default:
				break;
			}
		}
	}
	if (memcmp(GAddr, CurrLVCAddr, 4) != 0)
	{
		ClearDBF(MtArchivesDBFName, MtArchivesField, MTARCHFIELDCNT);
		memcpy(GAddr, CurrLVCAddr, 4);
		GAddr[4] = 0x2c;
	}

	return 1;
}

static int CheckUserID(void)
{
	/*char id[10] = "\0";

	 while(1){
	 LcdStart();
	 printf("  LVCPOS V1.03\n");
	 printf("----------------\n");
	 printf("  用户名:%s\n", G_UserID);
	 printf("  密  码:");
	 printf("----------------\n");
	 printf("深圳长城开发科技股份有限公司");
	 if(GetString(id, 6, TYPE_ALPHANUMERIC) == 0) return 0;
	 if (strcmp(id, G_UserID) == 0)
	 return 1;
	 else{
	 printf("\n用户名不正确！");
	 getch();
	 continue;
	 }
	 }*/
	return 1;
}

static int CheckUserPsw(void)
{
	char psw[10] = "\0";
	int x = 0, y = 0;

	while (1)
	{
		LcdStart();
		printf("LVCPOS CS V1.03\n");
		printf("----------------\n");
		printf("  用户名:%s\n", G_UserID);
		printf("\n  密  码:");
		x = wherex();
		y = wherey();
		printf("\n----------------\n");
		printf("深圳长城开发科技\n  股份有限公司");
		gotoxy(x, y);
		if (GetString(psw, 6, TYPE_PASSWORD) == 0)
			return 0;
		if (strcmp(psw, G_PSW) == 0)
			return 1;
		else
		{
			ClearWindow(1, 7, 16, 8);
			gotoxy(1, 8);
			printf("  密码不正确！");
			getch();
			continue;
		}
	}
}

static int LoginSys(void)
{
	//if (CheckUserID() == 0)	return 0;
	if (CheckUserPsw() == 0)
		return 0;

	return 1;
}

static void test(void)
{

}

void notmain(void)
{
	void (*p[])(void) =
	{ LVCParam, Archives, MaintainLVC, ReadData, ManageData, ManageSys };
	const uchar *buf[6];
	char addr[10];

	//test();	return;
	directvideo = 0;
	HideCursor();
	ChineseVerInit();
	ShowTitle();
	KeyCheckTimeout(400);
	if (KeyCheck() != 0)
	{
		KeyWaitFree();
	}

	if (GetINI() == 0)
	{
		SaveINI(G_UserID, G_PSW, "FFFFFFFF", G_COMPort, GBaud);
	}
	if (LoginSys() == 0)
		return;
	if (LVCPosInit() == 0)
		return;
	LastInputInit();

	buf[0] = LVCParamStr;
	buf[1] = ArchivesStr;
	buf[2] = MaintainLVCStr;
	buf[3] = ReadDataStr;
	buf[4] = DataManageStr;
	buf[5] = SystemManageStr;
	while (1)
	{
		InitMenu(buf, p, 6, 0);
		if (YesorNo(3, 0))
		{
			break;
		}
	}
	if (memcmp(GAddr, DEFAULT_LVCADDR, 4) == 0)
		SaveINI(G_UserID, G_PSW, "FFFFFFFF", G_COMPort, GBaud);
	else
	{
		HexToStr(addr, GAddr, 4);
		SaveINI(G_UserID, G_PSW, addr, G_COMPort, GBaud);
	}
}

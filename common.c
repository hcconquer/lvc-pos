 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <stdio.h>
#include <string.h>
#include <dos.h>
#include <mem.h>
#include <conio.h>
#include <math.h>
#include <stdlib.h>
#include <io.h>
#include <ctype.h>
#include "typedef.h"
#include "common.h"
#include "input.h"
#include "lcdout.h"
#include "strdef.h"

boolean yes_no(uchar rol, uchar col)
{
	uchar key;
	
	LcdPrintS(rol, col, YesNo);
	LcdPrintS(7, 0, No);
	LcdPrintS(7, 13, Yes);
	while (1) {
		key = KeyScan();
		if (key == '\r' || key == 'y')
			return 0;
		else if (key == 27 || key == 'n')
			return 1;
	}
}

/*uint coder(uchar l)
{
	uint  wd=0x8000,ll=0;
	uchar bt=0x80,i;
	for (i=0;i<8;i++) {
		if (l&bt) { wd>>=1; ll|=wd;}
		else { ll|=wd; wd>>=1;}
		wd>>=1;
		bt>>=1;
	}
	return ll;
}*/

uchar decode(uint ll)
{
	uint  wd=0x0001;
	uchar bt=0x01,i,l=0;
	for (i=0;i<8;i++) {
		if (ll&wd) { 
			l|=bt;  wd<<=1; 
			if (ll&wd) return 0;
		} else {
			 wd<<=1;
			 if ((ll&wd)==0) return 0;
		}
		wd<<=1;
		bt<<=1;
	}
	return l;
}

/*uint crcset(uchar *str,uchar lng)
{
    union	intval
	{
		uint	val;
       	uchar	d[2];
	};
    union	intval	crcs;
    int i,j;
    uchar recd,ch;
    crcs.val=0;
    for (i=0;i<lng;i++)
	{  ch=str[i];
	   for (j=0;j<8;j++) {
		   recd=0;
	       if (ch>=128)	recd=0x80;
	       ch*=2;
	       recd=recd ^ crcs.d[0];
           if (recd>=128)  {
			   recd=0x80;
		       crcs.d[0]=crcs.d[0] ^ 0x08;
		       crcs.d[1]=crcs.d[1] ^ 0x10;
		   }
           else  recd=0;
	       crcs.d[0]*=2;
	       if (crcs.d[1]>=128) crcs.d[0]++;
	       crcs.d[1]*=2;
           if (recd==128)  crcs.d[1]++;
	   }
	}
 return(crcs.val);
}*/

uchar AscToValue(uchar ascii)
{
	switch (ascii) {
		case '0':	return 0;
		case '1':	return 1;
		case '2':	return 2;
		case '3':	return 3;
		case '4':	return 4;
		case '5':	return 5;
		case '6':	return 6;
		case '7':	return 7;
		case '8':	return 8;
		case '9':	return 9;
		case '.':	return 0x0A;
		case 'a':	return 0x0a;
		case 'A':	return 0x0a;
		case 'b':	return 0x0b;
		case 'B':	return 0x0b;
		case 'f':	return 0x0f;
		case 'F':	return 0x0f;
		default: return 0xff;
	}
}

uchar ValueToAsc(uchar value)
{
	switch(value){
		case 0x00:return '0';
		case 0x01:return '1';
		case 0x02:return '2';
		case 0x03:return '3';	
		case 0x04:return '4';
		case 0x05:return '5';
		case 0x06:return '6';
		case 0x07:return '7';
		case 0x08:return '8';
		case 0x09:return '9';
		case 0x0a:return 'a';
		case 0x0b:return 'b';
		default:return 0;
	}
}

void BcdToAscii(uchar *ascii, uchar *bcd, uchar n)
{
	uchar i;
	uchar value;
	
	for (i = 0; i < 2 * n; i++) {
		if (i%2)
			value = bcd[i/2] & 0x0f;
		else 
			value = bcd[i/2] >> 4;		
		switch (value) {
			case 0:		ascii[i] = '0'; break;
			case 1:		ascii[i] = '1'; break;
			case 2:		ascii[i] = '2'; break;
			case 3:		ascii[i] = '3'; break;
			case 4:		ascii[i] = '4'; break;
			case 5:		ascii[i] = '5'; break;
			case 6:		ascii[i] = '6'; break;
			case 7:		ascii[i] = '7'; break;
			case 8:		ascii[i] = '8'; break;
			case 9:		ascii[i] = '9'; break;
			case 0x0A:	ascii[i] = '.'; break;
			default:	break;
		}
	}
}


uchar BcdToChar(uchar value)
{
	return (value >> 4) * 10 + (value & 0x0f);
}

uchar CharToBcd(uchar value)
{
	return (value % 10) + ((value / 10) << 4);
}

ulong ChangeUnion(ulong InData)
{
	union longunit change;
	uchar temp;
	
	change.four = InData;
	temp = change.d[0]; change.d[0] = change.d[3]; change.d[3] = temp;
	temp = change.d[1]; change.d[1] = change.d[2]; change.d[2] = temp;
	return change.four;
}

ulong BcdToLong(uchar *source,uchar str_len)
{
	uchar i;
	ulong temp = 0;
	ulong pow = 1;
	
	for (i = 0; i < str_len; i++) {
		temp += BcdToChar(source[str_len - i - 1]) * pow;
		pow = pow * 100;
	}
	return temp;
}

void LongToBcd(ulong source,uchar *target)
{
	uchar i, value;
	
	for (i = 0; i < 4; i ++) {
		value = source % 100;
		target[4 - i - 1] = (value % 10) + ((value / 10) << 4);
		source = source / 100;
	}	
}

void strcopy(uchar *str_source, uchar *str_target, uchar str_len, boolean case_flag)
{
	uchar i;
	for (i = 0; i < str_len; i ++) {
		if (case_flag == 0) {
			str_target[i] = str_source[i];
		}
		else {
			str_target[str_len - i - 1] = str_source[i];
		}
	}
}

void RtcReadClock(uchar *time_date)
{
	struct dosdate_t curdate;
	struct time curtime;
	
	_dos_getdate(&curdate);
	gettime(&curtime);
	time_date[0] = CharToBcd((curdate.year / 100) % 100);
	time_date[1] = CharToBcd(curdate.year % 100);	// year
	time_date[2] = CharToBcd(curdate.month);		// month
	time_date[3] = CharToBcd(curdate.day);		// day
	time_date[4] = CharToBcd(curtime.ti_hour);		// hour
	time_date[5] = CharToBcd(curtime.ti_min);		// minute
	time_date[6] = CharToBcd(curtime.ti_sec);		// second
	time_date[7] = CharToBcd(curdate.dayofweek);	// week
}

int GetChoice(char *notice, int min, int max)
{    
    int val, x, y;
	char str[6] = "\0";
	
	x=wherex();
	y=wherey();
	
	do{
		strcpy(str, "3");
		gotoxy(x+1, y);
		if(notice != NULL)
			printf("%s\n", notice);
		if(GetString(str, 1, TYPE_NUMBER) == 0) continue;
		val = atoi(str);
		if ((val >= min) && (val <= max))
			break;
		else{
			ClearWindow(1, 8, 16, 8);
			gotoxy(1, 8);
			printf("  输入不合法！");
			getch();
			ClearWindow(1, 8, 16, 8);
			continue;
		}
	}while(1);
	
	return val;
}

void TestBuf(uchar *buf, int len)
{
	int i;
	
	//printf("packlen=%d\n", len);
	for(i = 0; i<len; i++)
	{
		if(i%5 == 0)
			printf("\n");
		printf("%02x ", buf[i]);
	}
	getch();
}

void PrintBuf(uchar *buf, int len, int order, int space)
{
	int i;
	uchar *ptr = NULL;
	
	switch(order){
		case ORDER:
			ptr = buf;
			for(i=0;i<len;i++){
				printf("%02x", ptr[i]);
				if(space == SPACE)
					printf(" ");
			}
			break;
		case NOTORDER:
			ptr = buf + len - 1;
			for(i=0;i<len;i++){
				printf("%02x", *ptr);
				ptr--;
				if(space == SPACE)
					printf(" ");
			}
			break;
		default:
			break;
	}
}

void PrintBufChar(uchar *buf, int len, int order, int space)
{
	int i;
	uchar *ptr = NULL;
	
	switch(order){
		case ORDER:
			ptr = buf;
			for(i=0;i<len;i++){
				printf("%c", ptr[i]);
				if(space == SPACE)
					printf(" ");
			}
			break;
		case NOTORDER:
			ptr = buf + len - 1;
			for(i=0;i<len;i++){
				printf("%c", *ptr);
				ptr--;
				if(space == SPACE)
					printf(" ");
			}
			break;
		default:
			break;
	}
}

void ScanIP(uchar *ip, uint len, uchar *StoreIp)
{
	uchar *pIP = ip, *pSIP = StoreIp;
	uint IpTemp = 0;
	uint i = 0;
	
	while(i <= len)
	{
		if((*pIP == '.') || (i == len))
		{
			if(i == len)
			{
				*pSIP = IpTemp;
				break;
			}
			*pSIP++ = IpTemp;
			IpTemp = 0;
			pIP++;
			i++;
		}
		IpTemp = IpTemp * 10 + AscToValue(*pIP);
		pIP++;
		i++;
	}
}

void ScanDataTo2Hex(long data, uchar *StoreData, int IsLowToHigh)
{
	uchar *ptr = StoreData;
	
	switch(IsLowToHigh){
		case LowToHigh:
			*ptr++ = data & 0xff;
			*ptr++ = (data >> 8) & 0xff;
			break;
		case HighToLow:
			*ptr++ = (data >> 8) & 0xff;
			*ptr++ = data & 0xff;
			break;
		default:
			break;
	}
}

long Scan2HexToData(uchar *Src, int IsLowToHigh)
{
	uchar *ptr = Src;

	if(IsLowToHigh == 1){
		return (*ptr) + (((*(ptr + 1)) << 8) & 0xff00);
	}
	else{
		return *(ptr+1) + (((*ptr) << 8) & 0xff00);
	}
}

void ScanInputStr(uchar *DstStr, uchar *SrcStr, int len)
{
	int i, j;
	uchar *ptr = DstStr;
	
	for(i=0; i < len; i++)
	{
		if(SrcStr[i] != '\0')
			*ptr++ = SrcStr[i];
		else
		{
			for(j=0; j<len-i; j++)
				*ptr++ = 0x00;
			break;
		}
	}
}

int LenToInt(uchar *buf)
{
	return ((buf[0] + (buf[1] << 8)) >> 2) & 0x3f;
}

//写文件后,在不用关闭文件的情况下,将缓冲区数据刷新到文件
void flush(FILE *stream)
{
  int duphandle;

  /* flush the stream's internal buffer */
  fflush(stream);

  /* make a duplicate file handle */
  duphandle = dup(fileno(stream));

  /* close the duplicate handle to flush the DOS buffer */
  close(duphandle);
}

void LongToRec(long cnt, uchar *rec)
{	
	char ch;
	int tmp=1, i;
	
	for(i = 3;i>=0; i--){
		ch = ValueToAsc((cnt / tmp) % 10);
		tmp = tmp * 10;
		if(ch == '0')
			rec[i] = 0x20;
		else
			rec[i] = ch;
	}
}

void DateToRec(uchar *DateRec, uchar *date, uchar ExistDay)
{
	uchar *ptr = DateRec;
	
	memcpy(ptr, "\x32\x30", 2); //copy "20" to the first two byte of year, such as 20 08
	ptr += 2;
	switch(ExistDay){
		case 'Y':
			BcdToAscii(ptr, date+2, 1);
			ptr += 2;
			BcdToAscii(ptr, date+1, 1);
			ptr += 2;
			BcdToAscii(ptr, date, 1);
			break;
		case 'N':
			BcdToAscii(ptr, date+1, 1);
			ptr += 2;
			BcdToAscii(ptr, date, 1);
			ptr += 2;
			*ptr++ = '0';
			*ptr++ = '1';
			break;
		default:
			break;
	}
	
}

int is_leap_year(int year)
{
    return ((year % 4) == 0 && (year % 100) != 0) || (year % 400) == 0;
}

void NextDay(uchar *day, uchar *month, uchar *year)
{
    static const uchar month_day[] = {31,28,31,30,31,30,31,31,30,31,30,31};
    uchar max_day;

    max_day = month_day[*month - 1];
    if (*month == 2 && is_leap_year(*year + 2000))
        max_day ++;
    if (*day == max_day) {
        *day = 1;
        if (*month == 12) {
            *month = 1;
            *year = *year + 1;
        }
        else {
            *month = *month + 1;
        }
    }
    else {
        *day = *day + 1;
    }
}

void NextMonth(uchar *month, uchar *year)
{
    if (*month == 12) {
        *month = 1;
        *year = *year + 1;
    }
    else {
        *month = *month + 1;
    }
}


unsigned GetString(char *str, uchar len, int type)
{
  int i, x, y,start;
  char LastKey,OldKey=NULL;
  char psw[30]="\0";
  
  i=0;
  x=wherex();
  y=wherey();
  str[len]=NULL;//Cannot delete 20061010 str中可以预先写入值
  if (type == TYPE_PASSWORD)
  	cprintf(psw);
  else
  	cprintf(str);
  //memset(str,NULL,len);
  i=wherex()-x;
  start=0;
  while (1)
  {
    LastKey=getch();
    if (LastKey=='\r')      //Return
    {
      str[i]=0x0;
      cprintf("\r\n");
      return 1;
    }
    else if (LastKey==0x1B) //27 Esc
    {
	 	return 0;
	}
	else if ((LastKey=='\b')||((LastKey=='K')&&(OldKey==NULL)))//<-
    {
	 	start=-1;
	 	if (i>0)
	 		i--;
	 	str[i]=NULL;
	 	if (type == TYPE_PASSWORD)
	 		psw[i] = NULL;
	 }
	 else if ((LastKey!=NULL)&&(i<=len))
	 {
    	switch(type)
    	{
    		case TYPE_NUMBER://'0'..'9'
    		{
	 			if (((start==0)||(i<len))&&isdigit(LastKey))/*&&(LastKey>='0')&&(LastKey<='9')*/
	 			{
	 				if (start==0) i=0;
	 				start=-1;
	 				str[i]=LastKey;
	 				i++;
					str[i]=NULL;
				}
				break;
			}
			case TYPE_FLOAT://'0'..'9' '.'
			{
	 			if (((start==0)||(i<len))&&(isdigit(LastKey)||LastKey=='.'))
	 			{
	 				if (start==0) i=0;
	 				start=-1;
	 				str[i]=LastKey;
	 				i++;
					str[i]=NULL;
				}
				break;
			}
			case TYPE_HEX://'0'..'9' 'a'..'f' 'A'..'F'
			{
    			if (islower(LastKey))
    				LastKey = LastKey-32;//a->A
	 			if (((start==0)||(i<len))&&(isdigit(LastKey)||((LastKey>='A')&&(LastKey<='F'))))
	 			{
	 				if (start==0) i=0;
	 				start=-1;
	 				str[i]=LastKey;
	 				i++;
					str[i]=NULL;
				}
				break;
			}
			case TYPE_ALPHANUMERIC://'0'..'9' 'a'..'z' 'A'..'Z'
			{
    			/*if (islower(LastKey))
    				LastKey = LastKey-32;*/   //a->A
	 			if (((start==0)||(i<len))&&(isalnum(LastKey)))
	 			{
	 				if (start==0) i=0;
	 				start=-1;
	 				str[i]=LastKey;
	 				i++;
					str[i]=NULL;
				}
				break;
			}
			case TYPE_PASSWORD:	//'0'..'9' 'a'..'z' 'A'..'Z'
			{
	 			if (((start==0)||(i<len))&&(isalnum(LastKey)))
	 			{
	 				if (start==0) i=0;
	 				start=-1;
	 				str[i]=LastKey;
	 				psw[i]='*';
	 				i++;
					str[i]=NULL;
					psw[i]=NULL;
				}
				break;
			}
		}
    }
	gotoxy(x, y);
	clreol();
	if (type == TYPE_PASSWORD)
		cprintf(psw);
	else
		cprintf(str);
	OldKey=LastKey;
  }
}

int StrToHex(uchar *hex, uchar *str, int len)
{
	int i;
	
	for(i=0;i<len;i++){
		hex[i] = ((str[2*i] - '0') << 4) + (str[2*i+1] - '0');
	}
	
	return 1;
}

int HexToStr(uchar *str, uchar *hex, int len)
{
	int i;
	
	for(i=0;i<len;i++){
		str[2*i] = ((hex[i] >> 4) & 0x0f) + '0';
		str[2*i+1] = (hex[i] & 0x0f) + '0';
	}
	str[2*len] = '\0';
	
	return 1;
}

int LongToStr(char *str, long val)
{
	long s;
	char StrTmp[10];
	int i = 0, j = 0;
	
	s = val;
	if (s == 0){
		strcpy(str, "0");
		return 1;
	}
	if (s < 0)
		return 0;
	while(s != 0){
		StrTmp[i++] = ValueToAsc(s%10);
		s = s / 10;  
	}
	for (i = i - 1;i >=0; i--)
		str[j++] = StrTmp[i];
	str[j] = '\0';
	
	return 1;	
}
 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-22
 *	Some Code were supported by HongLi
 */
 
#include <stdio.h>
#include <alloc.h>
#include <mem.h>
#include <conio.h>
#include <io.h>
#include <dos.h>
#include <string.h>
#include "typedef.h"
#include "common.h"
#include "dbf.h"
#include "lcdout.h"
#include "lvc_pos.h"

struct FIELD_RECORD MtDayFroRecField[DAYFIELDCNT]=//定义字段描述区
{	//字段名		类型 数据地址 占位		长度	小数	保留
	{"LVCAddr",		'C',   0,     0,		8,		0,		""},
 	{"MtIdx",		'I',   0,     0,		4,  	0,		""},
 	{"Date",		'D',   0,     0,		8,  	0,		""},
 	{"TotalEngy",	'N',   0,     0,		9,		2,		""},
 	{"FeiLv1",		'N',   0,     0, 		9,		2,		""},
 	{"FeiLv2",		'N',   0,     0, 		9,		2,		""},
 	{"FeiLv3",		'N',   0,     0, 		9,		2,		""},
 	{"FeiLv4",		'N',   0,     0, 		9,		2,		""}
};//以上结构中实际上(数据地址+占位符)应为偏移地址4字节,不填入数据,程序自动计算


struct FIELD_RECORD MtMonFroRecField[MONFIELDCNT]=//定义字段描述区
{	//字段名		类型 数据地址	占位	长度	小数	保留
	{"LVCAddr",		'C',   0,		0,		8,		0,		""},
 	{"MtIdx",		'I',   0,		0,		4,  	0,		""},
 	{"Date",		'D',   0,		0,		8,  	0,		""},
 	{"TotalEngy",	'N',   0,		0,		9,		2,		""}
};//以上结构中实际上(数据地址+占位符)应为偏移地址4字节,不填入数据,程序自动计算


struct FIELD_RECORD MtArchivesField[MTARCHFIELDCNT]=//定义字段描述区
{//	 字段名			类型	数据地址 占位	长度	小数	保留
	{"MtIdx",		'C',		0,     0,	2,		0,		""},
 	{"MtNum",		'C',		0,     0,	6,		0,		""},
 	{"MeasPro",		'C',		0,     0,	1,		0,		""},
 	{"ConnWay",		'C',		0,     0, 	1,		0,		""},
 	{"FeiLvNum",	'C',		0,     0, 	1,		0,		""},
 	{"MtType",		'C',		0,     0, 	1,		0,		""},
 	{"LineNum",		'C',		0,     0, 	2,		0,		""},
 	{"MtBoxNum",	'C',		0,     0, 	2,		0,		""},
 	{"CollNum",		'C',		0,     0, 	6,		0,		""}
};//以上结构中实际上(数据地址+占位符)应为偏移地址4字节,不填入数据,程序自动计算

/*struct FIELD_RECORD MPArchField[MPARCHFIELDCNT]=
{
	{"EqpIdx",		'C',		0,     0,	1,		0,		""},
 	{"MeasNum",		'C',		0,     0,	1,		0,		""},
 	{"Baud",		'C',		0,     0,	1,		0,		""},
 	{"ComPro",		'C',		0,     0, 	1,		0,		""},
 	{"ComAddr",		'C',		0,     0, 	6,		0,		""},
 	{"ComPwd",		'C',		0,     0, 	6,		0,		""},
 	{"Count",		'C',		0,     0, 	1,		0,		""}
};*/

/************************************************************************************
**		function:		d_open
**		purpose:		routine to open a dbaseiii file for access by the other routines
**						in dbf.lib.
**		usage:		d = (struct DBF *)malloc(sizeof(struct DBF));
**						strcpy(d->filename,"filename.dbf");
**						d_open(d);
**						... access file with other routines ...
**						d_close(d);
**						free(d);
**		returns:		0			 if successful with structure filled in
**						NO_FILE	 if unable to find file
**						OUT_OF_MEM if not enough memory
**						BAD_FORMAT if not dBASE file
************************************************************************************/
int d_open(struct DBF *d)
{
	int i;
	int n;

	d->status = not_open;							 /* in case can not open       */
	if((d->file_ptr = fopen(d->filename,"r+b")) == NULL)
		return(NO_FILE);

	rewind(d->file_ptr);

	fread((void *)&d->dbf_version,(unsigned)1,(unsigned)12,d->file_ptr);	/* read prolog */

	if (d->dbf_version != DB3FILE					/* check for dbiii file marker */
		&& d->dbf_version != DB3WITHMEMO
		|| d->update_mo == 0)
	{
		fclose(d->file_ptr);
		return(BAD_FORMAT);
	}

	d->current_record = 0L;
	//d->num_fields = ((d->header_length - (FIELD_REC_LEN+1)) / HEADER_PROLOG);
	d->num_fields = ((d->header_length - (HEADER_PROLOG+1)) / FIELD_REC_LEN);//Modify by HongLi

	if((d->fields_ptr = (struct FIELD_RECORD *)malloc((unsigned)(d->num_fields * FIELD_REC_LEN)))==NULL)
		return(OUT_OF_MEM);

	/* position at field descriptions */
	fseek(d->file_ptr,(long)HEADER_PROLOG,0);

	/* read into field description array */
	fread((void *)d->fields_ptr,sizeof *d->fields_ptr,(unsigned)d->num_fields,d->file_ptr); //读出所有字段信息

	if((d->record_ptr = (char *)malloc(d->record_length))==NULL)//分配一条记录的空间
		return(OUT_OF_MEM);

	/* initialize pointers to fields in record. *///填充字段描述表中的字段数据指针(记录中该字段的偏移量) 字段描述表的12-15字节描述,个别表可能未描述,手工计算
	for(i=0,n=1;i<d->num_fields;i++)		/* n is offset from start of rec */ 
	{ 											/* @ n=0 is the deleted record flag */
		d->fields_ptr[i].field_data_address = d->record_ptr + n;
		n += d->fields_ptr[i].len;
	}

	d->status = not_updated;						 /* open successfull */
	return(0);
}


/***************************************************************
*  db_create(filename)  creates a new  DBASE file  header for  *
* output.  A pointer to the DBASE file description block is    *
* returned if there is sufficient memory.  If not, the ERROR   *
* value is returned.                                           *
*                                                              *
* HongLi                                            (c) 2007   *
***************************************************************/
int d_create(struct DBF *d, char *filename, struct FIELD_RECORD *field, int count)
{  
	int i, n, fldlen=0, FIELDCNT;
	struct date dd;
	char EOFb=0x1A, EOHb=0x0D, NULLb[20];//文件头结束符/文件结束符/空字符数组
	struct FIELD_RECORD *field_descriptor_array;

	field_descriptor_array = field;
	FIELDCNT = count;
	
	/*if(stricmp(d->filename, MtDayFroDBFName) == 0){
		field_descriptor_array = MtDayFroRecField;
		FIELDCNT = DAYFIELDCNT;
	}
	else if(stricmp(d->filename, MtMonFroDBFName) == 0){
		field_descriptor_array = MtMonFroRecField;
		FIELDCNT = MONFIELDCNT;
	}
	else if(stricmp(d->filename, MtMonFroDBFName) == 0){
	}
	else
		return ERROR;*/
		
	getdate(&dd);
	/*----- initialize the file description block -----*/
	d->status = not_open;							 /* in case can not open       */
	if((d->file_ptr = fopen(d->filename,"w+b")) == NULL)//写方式打开文件
		return(ERROR);

	//1.DBF文件头32字节(必须)
	d->dbf_version = 3;					/* version character */   //00
	d->update_yr = (char)(dd.da_year%100);	/* date of last update - year(-2000) */  //01
	d->update_mo = dd.da_mon;				/* date of last update - month */   //02
	d->update_day = dd.da_day;				/* date of last update - day	*/    //03
	d->records = 0L;			          /* number of records in dbf */  //04-07
	for(i=0; i<FIELDCNT; i++)
		fldlen = fldlen + field_descriptor_array[i].len;//一条记录中所有字段长度之和(不包括删除标记)
	fldlen = fldlen+1;//删除标记
	d->header_length = HEADER_PROLOG + FIELD_REC_LEN*FIELDCNT + 1;		/* length of header structure */ //08-09
	d->record_length = fldlen;		/* length of a record */     //10-11
	if (fwrite(&(d->dbf_version), 12, 1, d->file_ptr) != 1)//写入前12个字节
	{
		cprintf("Unable to write header1 to %s", filename);
		return ERROR;
	}
	memset(NULLb, NULL, sizeof(NULLb));
	if (fwrite(NULLb, sizeof(NULLb), 1, d->file_ptr) != 1)//写入后20个字节
	//if (fwrite(&NULLb, sizeof(char), 20, d->file_ptr) != 20)//写入后20个字节
	{
		cprintf("Unable to write header2 to %s", filename);
		return ERROR;
	}
	//2.字段描述区(必须)
	for(i=0; i<FIELDCNT; i++)
	{
		if (fwrite(&field_descriptor_array[i], FIELD_REC_LEN, 1, d->file_ptr) != 1)//写入一个字段到文件
		{
			cprintf("Unable to write field to %s", filename);
    	return ERROR;
  	}
	}
	//3.文件头结束符0x0D
	//if (fwrite(&EOHb, sizeof(char), 1, d->file_ptr) != 1)
	if (fputc(EOHb, d->file_ptr) == EOF)//写入一个字符,错误返回EOF,正确返回写入字符
	{
		cprintf("Unable to write EOH to %s", filename);
		return ERROR;
	}
	//4.文件结束符0x1A
	//if (fwrite(&EOFb, sizeof(char), 1, d->file_ptr) != 1)
	if (fputc(EOFb, d->file_ptr) == EOF)//写入一个字符
	{
		cprintf("Unable to write EOF to %s", filename);
		return ERROR;
  }
  flush(d->file_ptr);//刷新缓冲到文件
  
  ///////////////////文件写完之后,将新文件的信息填充到DBF文件结构体的其他部分
  d->current_record = 0L;
	d->num_fields = FIELDCNT;
	if((d->fields_ptr = (struct FIELD_RECORD *)malloc((unsigned)(d->num_fields * FIELD_REC_LEN)))==NULL)
		return(OUT_OF_MEM);

	/* position at field descriptions */
	fseek(d->file_ptr,(long)HEADER_PROLOG,0);

	/* read into field description array */
	fread((void *)d->fields_ptr,sizeof *d->fields_ptr,(unsigned)d->num_fields,d->file_ptr); //读出所有字段信息

	if((d->record_ptr = (char *)malloc(d->record_length))==NULL)//分配一条记录的空间
		return(OUT_OF_MEM);

	/* initialize pointers to fields in record. *///填充字段描述表中的字段数据指针(记录中该字段的偏移量) 字段描述表的12-15字节描述,个别表可能未描述,手工计算
	for(i=0,n=1;i<d->num_fields;i++)		/* n is offset from start of rec */ 
	{ 											/* @ n=0 is the deleted record flag */
		d->fields_ptr[i].field_data_address = d->record_ptr + n;
		n += d->fields_ptr[i].len;
	}

	d->status = not_updated;						 /* open successfull */
	
	d_close(d);
  return 0;
}


/* 
**		function:	d_addrec
**		purpose:	routine to append a record to a open dbaseiii file.
**		usage:	d = (struct DBF *)malloc(sizeof(struct DBF));
**					strcpy(d->filename,"filename.dbf");
**					d_open(d);
**					... put desired data into memory at location pointed to by
**							d->record_ptr ...
**					d_addrec(d);
**					d_close(d);
**					free(d);
*/
int d_addrec(struct DBF *d)
{
	/*LcdStart();
	printf("headLen=%d\nrecords=%d\nrecLen=%d", d->header_length, d->records, d->record_length);
	getch();*/
	fseek(d->file_ptr,((long)d->header_length + ((d->records) * (d->record_length))),0);
	fwrite(d->record_ptr,d->record_length,1,d->file_ptr);
	d->current_record = ++(d->records);
	d->status = updated;
	return(0);
}

/********************************************************************************* 
**		function:	d_getfld
**		purpose:	routine to fill a buffer with field data from a dbiii file
**					opened with d_open and accessed with d_getrec.	the data from the
**					record is copied into the buffer and terminated with '\0'
**		usage:	d = (struct DBF *)malloc(sizeof(struct DBF));
**					strcpy(d->filename,"filename.dbf");
**					d_open(d);
**					d_getrec(d,(long)recordno);
**					d_getfld(d,fieldno,buffer);
**					... use field data ...
**					d_close(d);
**					free(d);
**		returns:	field type  if successful
**					null if not
*********************************************************************************/ 

char d_getfld(struct DBF *d,int f,char *buff)//Get field by index(from 1)
{
	struct FIELD_RECORD *fp;
	if(f > 0 && f <= d->num_fields)
	{
		fp = d->fields_ptr + (f - 1);
		memcpy(buff,fp->field_data_address,fp->len);
		//printf("fplen=%d\n", fp->len);
		buff[fp->len] = '\0';
		return(fp->typ);
	}
	else
	{
		buff[0]='\0';
		return('\0');
	}
}

char d_getfldByName(struct DBF *d,char *name,char *buff)//Get field by name
{
	int f;
	struct FIELD_RECORD *fp;	
	
	for(f=0; f<d->num_fields; f++)//根据字段名返回字段信息
	{
		fp = d->fields_ptr + f;
		if (stricmp(fp->name, name)==0)//找到名称相同的字段,忽略大小写
		{
			memcpy(buff,fp->field_data_address,fp->len);
			buff[fp->len] = '\0';
			return(fp->typ);
		}
	}
	if(f>=d->num_fields)//未找到此字段
	{
		buff[0]='\0';
		return('\0');
	}
	return fp->typ;
}


/* 
**		function:	d_getrec
**		purpose:	routine to get a record from a dbiii file and place in memory at
**					location pointed to by DBF.record_ptr.
**		ussage:	d = (struct DBF *)malloc(sizeof(struct DBF));
**					strcpy(d->filename,"filename.dbf");
**					d_open(d);
**					d_getrec(d,(long)record);
**					... access record as desired ...
**					d_close(d);
**					free(d);
**		returns:	0					if successfull
**					RECNO_TOO_BIG	if record is not in database
*/ 

int d_getrec(struct DBF *d,unsigned long int r)
{
	if(r > d->records)
		return(RECNO_TOO_BIG);
	if (r > 0L)
	{
		fseek(d->file_ptr,((long)d->header_length + ((r - 1L) * d->record_length)),0);
		fread(d->record_ptr,d->record_length,1,d->file_ptr);
		d->current_record = r;
		return(0);
	}
	return(RECNO_TOO_BIG);
}


/* 
**		function:	d_putfld
**		purpose:	routine to fill a field with data from buffer.
**		ussage:	d = (struct DBF *)malloc(sizeof(struct DBF));
**					strcpy(d->filename,"filename.dbf");
**					d_open(d);
**					d_getrec(d,(long)recordno);
**					d_getfld(d,fieldno,buffer);
**					... modify field data ...
**					d_putfld(d,fieldno,buffer);
**					d_putrec(d,(long)recordno);
**					d_close(d);
**					free(d);
**		notes:	this routine places the data into a record in memory.	d_addrec or d_putrec must be called to write
**					the data to the file on disk.
**		returns:	length of the field if successful. 0 if not.
*/ 
int d_putfld(struct DBF *d,int f,char *buff)
{
	struct FIELD_RECORD *fp;

	if(f > 0 && f <= d->num_fields)
	{
		fp = d->fields_ptr + (f - 1);
		memcpy(fp->field_data_address,buff,fp->len);
		return(fp->len);
	}
	else
	{
		return(0);
	}
}

/* 
**		function:	d_putrec
**		purpose:	routine to update a record in a dbiii file.
**		ussage:	d = (struct DBF *)malloc(sizeof(struct DBF));
**					strcpy(d->filename,"filename.dbf");
**					d_open(d);
**					d_getrec(d,(long)recordno);
**					... modify record ...
**					d_putrec(d,(long)recordno);
**					d_close(d);
**					free(d);
**		notes:	the data for the record is contained in memory at the location pointed to by DBF.record_ptr.
**		returns:	0					if successfull
**					RECNO_TOO_BIG	if record is not in database
*/ 
int d_putrec(struct DBF *d,unsigned long int r)
{
	if(r > d->records)
		return(RECNO_TOO_BIG);
	if(r > 0L)
	{
		fseek(d->file_ptr,((long)d->header_length + ((r - 1) * d->record_length)),0);
		fwrite(d->record_ptr,d->record_length,1,d->file_ptr);
		d->status = updated;
	}
	d->current_record = r;
	return(0);
}

//add by Song Hao
int d_delrec(struct DBF *d, unsigned long int r)
{
	uchar del=0x2a;
	
	if(r > d->records)
		return(RECNO_TOO_BIG);
	if(r > 0L)
	{
		fseek(d->file_ptr,((long)d->header_length + ((r - 1) * d->record_length)),0);
		fwrite(&del,1,1,d->file_ptr);
		d->status = updated;
	}
	d->current_record = r;
	return(0);
}

/*************************************************************************
* db_seek positions the read/write pointer to the ith record in the data *
* base.  It also works with an offset so that the pointer can be moved   *
* back or forward n records from the current position, the beginning  or *
* end of the file.  If db_seek is sucessful it returns the current posi- *
* tion in the file; otherwise, it returns a -1.  The offset codes are as *
* follows:                                                               *
*                                                                        *
*          OFFSET                             MEANING                    *
*          ------                   ------------------------------       *
*        SEEK_SET	0                  offset from the beginning of the  *
*                                    data base. zero or + offset only    *
*                                                                        *
*        SEEK_CUR 1                  offset from the current position    *
*                                    in the database. zero, + or - offst *
*                                    allowed                             *
*                                                                        *
*        SEEK_END 2                  offset from the end of the database *
*                                    zero or - offset only               *
*                                                                        *
*************************************************************************/
int d_seek(struct DBF *d,int rcrds,int origin)
{
	long offset;

	/*----- calculate the character position offset for the "records" position -----*/
	switch(origin)
	{
		case 0://移动到从文件头开始的第recds条记录
			if((rcrds<1)||(rcrds>d->records))//1->0
				return(NO_RECORD);
			else
			{
				offset = (long) (d->header_length + (rcrds-1) * d->record_length) ;  //DATA_START
			 	break;
			}

		case 1://以当前记录为基准,前后移动rcrds条记录
			offset =  (long) (rcrds * (d->record_length));
			if((offset > 0) && (d->current_record + rcrds > d->records ))//当前记录+向后移动记录条数>数据库中记录总数
				return(ERROR);          /* tried to get past end of file      */
			else if((offset < 0) && ((long)(d->current_record) + rcrds<0))//当前记录+向前移动记录条数<0
				return(NO_RECORD);     /* tried to get past beginning of file */
			break;

		case 2://从文件尾向前移动rcrds条记录
			if((rcrds>0)||((long)(d->current_record) + rcrds<0))
				return(NO_RECORD);               /* attempt to offset past end-of-file */
			else
			{
				offset = (long)(d->header_length + (d->records + rcrds )*d->record_length);
				origin = 0 ;                   /* reset origin since offset is from beginning of file */
				break;
			}

		default:
	  	return(NO_RECORD);                /* undefined origin value */
	}

	/*----- return the record number -----*/
	if(fseek(d->file_ptr, offset, origin) == 0)//成功则将当前记录位置前后移动
	{
		d->current_record = d->current_record+rcrds;//当前记录
	}
	else//出错则回到操作前的位置
	{
		d_seek(d, d->current_record, 0);
	}
	return(d->current_record);
}

void d_error(int errornum, char *filename)
{
	LcdStart();
	printf("\n文件名:%s", filename);
	printf("\n错误描述:\n");
	switch(errornum)
	{
		case OUT_OF_MEM:
			printf("内存空间不足.\n");
			break;
		case NO_FILE:
			printf("不能打开文件.\n");
			break;
		case BAD_FORMAT:
			printf("文件不是dBASE III格式.\n");
			break;
		case NO_RECORD:
			printf("记录不存在.\n");
			break;
		default:
			printf("未知的错误.\n");
			break;
	}
	WaitForESCKey();
}

/* 
**		function:	d_close
**		purpose:	routine to close a dbaseiii file after access by the other routines
**					in dbf.lib.	updates header and places eof marker at end of file.
**		usage:	d = (struct DBF *)malloc(sizeof(struct DBF));
**					strcpy(d->filename,"filename.dbf");
**					d_open(d);
**					... access file with other routines ...
**					d_close(d);
**					free(d);
**		notes:	ALWAYS close a file that has been opened, otherwise records may be lost.
*/ 		
int d_close(struct DBF *d)
{
	void free(void *block);
	union REGS inregs,outregs;
	if(d->status == updated)
	{
		/* update date data */
		inregs.h.ah=0x2a;
		intdos(&inregs,&outregs);
		d->update_day=outregs.h.dl;
		d->update_mo=outregs.h.dh;
		d->update_yr=outregs.x.cx%100;//outregs.x.cx-1900;

		/* position at start of file */
		rewind(d->file_ptr);
		/* rewrite header */
		fwrite(&d->dbf_version,1,12,d->file_ptr);

		/* position at end of file	 */
		fseek(d->file_ptr,0L,2);
		/* write eof */
		fwrite("\x1a",1,1,d->file_ptr);
	}
	/* free fields array and record */
	free(d->fields_ptr);
	free(d->record_ptr);
	fclose(d->file_ptr);
	return 0;
}




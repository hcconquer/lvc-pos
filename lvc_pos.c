/*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <mem.h>
#include <dos.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <conio.h>
#include "typedef.h"
#include "dbf.h"
#include "lvc_pos.h"
#include "strdef.h"
#include "common.h"
#include "global.h"
#include "lcdout.h"
#include "rs232.h"
#include "util.h"

extern uchar pointer[4];
//ZBCDATA cdat;

uchar CurUserid[5] =
{ "0000" };
uchar CurPasswd[5] =
{ "0000" };
uchar CurAuth = 0;
FILE *fp;

void LastInputInit(void)
{
	memcpy(LastInfo.Userid, "1234", 4);
	LastInfo.CollectorId = 0x0001;
	LastInfo.MeterId = 0x01;
	LastInfo.Value = 0x00000000;
	LastInfo.Decimal = 0x00;
	LastInfo.Type = 0x00;
	LastInfo.PulseConst = 0x0001;
	RtcReadClock(LastInfo.Time_d);
}

void GetLastInput(uchar *buf, long last, uchar *ascii, uchar n)
{
	uchar i, temp;

	if (last >= 0)
	{
		buf += (n / 2 - 1);
		for (i = 0; i < n; i++)
		{
			if (i % 2 == 0)
			{
				*buf = ((last / 10) % 10) << 4;
			}
			else
			{
				*buf |= last % 10;
				last = last / 100;
				buf--;
			}
		}
	}
	else if (ascii != NULL)
	{
		for (i = 0; i < n; i++)
		{
			temp = AscToValue(*ascii++);
			if (i % 2 == 0)
			{
				*buf = temp << 4;
			}
			else
			{
				*buf |= temp;
				buf++;
			}
		}
	}
}

boolean useridcmp(uchar *userid)
{
	USERINFO userinfo;

	if ((fp = fopen("LVCPOS.INI", "rb")) == NULL)
	{
		if ((fp = fopen("LVCPOS.INI", "wb+")) == NULL)
			return False;
		memcpy(userinfo.userid, "1234", 4);
		memcpy(userinfo.passwd, "1234", 4);
		userinfo.auth = READ_PRIV | SETTIME_PRIV | SETMID_PRIV | SETID_PRIV
				| TOPC_PRIV;
		fwrite(&userinfo, sizeof(userinfo), 1, fp);
		fclose(fp);
		return False;
	}
	while (!feof(fp))
	{
		fread((uchar *) (&userinfo), sizeof(userinfo), 1, fp);
		if (memcmp(userid, userinfo.userid, 4) == 0)
		{
			memcpy(CurUserid, userid, 4);
			fclose(fp);
			return True;
		}
	}
	fclose(fp);
	return False;
}

boolean passwdcmp(uchar *passwd)
{
	USERINFO userinfo;

	if ((fp = fopen("LVCPOS.INI", "rb")) == NULL)
		return False;
	while (!feof(fp))
	{
		fread((uchar *) (&userinfo), sizeof(userinfo), 1, fp);
		if (memcmp(passwd, userinfo.passwd, 4) == 0)
		{
			memcpy(CurPasswd, passwd, 4);
			fclose(fp);
			return True;
		}
	}
	fclose(fp);
	return False;
}

uchar check_sum(const uchar *buf, int len)
{
	uchar sum = 0;
	while (len-- > 0)
		sum += *buf++;
	return sum;
}

int pack(uchar *buf, int max_len, const ST_MSG *msg)
{
	uchar sum, exist_pw, exist_ec, exist_tp, aux_size;
	uint len;
	uchar addr[5];
	uchar *ptr = buf;

	exist_pw = msg->aux_flag & 0x01;
	exist_ec = msg->aux_flag & 0x02;
	exist_tp = msg->aux_flag & 0x04;
	if (exist_ec && exist_tp && exist_pw)
		aux_size = 2 + 6 + 2;
	else if (exist_tp && exist_pw)
		aux_size = 6 + 2;
	else if (exist_ec)
		aux_size = 2;
	else if (exist_tp)
		aux_size = 6;
	else if (exist_pw)
		aux_size = 2;
	else
		aux_size = 0;
	if (msg->size + 16 + aux_size <= max_len)
	{
		memcpy(ptr, "\xfe\xfe\xfe\xfe", 4);
		ptr += 4;
		*ptr++ = 0x68;/*报文头*/
		len = ((msg->size + 8 + aux_size) << 2) | 0x01;
		stoc(ptr, len);
		ptr += 2;
		stoc(ptr, len);
		ptr += 2;
		*ptr++ = 0x68;
		*ptr++ = msg->code;/*ctrl code*/

		memcpy(ptr, msg->addr, 5);/*address*/
		ptr += 5;

		*ptr++ = msg->afn;
		*ptr++ = msg->seq;
		memcpy(ptr, msg->data, msg->size);/*链路数据*/
		ptr += msg->size;
		if (exist_ec)
		{
			memcpy(ptr, msg->aux_ec, 2);
			ptr += 2;
		}
		if (exist_tp)
		{
			memcpy(ptr, msg->aux_tp, 6);
			ptr += 6;
		}
		if (exist_pw)
		{
			memcpy(ptr, msg->aux_pw, 2);
			ptr += 2;
		}
		sum = check_sum(buf + 6 + 4, (ptr - buf) - 6 - 4);
		*ptr++ = sum;
		*ptr++ = 0x16;
	}
	return ptr - buf;
}

int unpack(const uchar *buf, int len, ST_MSG *msg)
{
	uchar afn, exist_pw, exist_tp, aux_size, ec_size;
	const uchar *ptr = buf + 6;

	msg->code = *ptr++;
	if ((msg->code & 0x20) == 0)
		ec_size = 0;
	else
		ec_size = 2;

	memcpy(msg->addr, ptr, 5);
	ptr += 5;
	afn = msg->afn = *ptr++;
	msg->seq = *ptr++;
	exist_pw = (afn == 0x01 || afn == 0x04 || afn == 0x05 || afn == 0x0f || afn
			== 0x10 || afn == 0x84 || afn == 0x85);
	exist_tp = msg->seq & 0x80;
	if (exist_pw && exist_tp)
		aux_size = 2 + 6;
	else if (exist_pw)
		aux_size = 2;
	else if (exist_tp)
		aux_size = 6;
	else
		aux_size = 0;
	msg->size = len - 16 - aux_size - ec_size;
	msg->data = (uchar *) ptr;
	ptr += msg->size;
	msg->aux_flag = 0;
	if (exist_pw)
	{
		msg->aux_flag |= 1;
		memcpy(msg->aux_pw, ptr, 2);
		ptr += 2;
	}
	if (exist_tp)
	{
		msg->aux_flag |= 4;
		memcpy(msg->aux_tp, ptr, 6);
		ptr += 6;
		/*if (!check_tp(msg->aux_tp)) {
		 PRINTF("time stamp too old\n");
		 return 0;
		 }*/
	}
	return ptr - buf;
}

uchar GetSEQ(void)
{
	uchar SEQ;

	SEQ = GSEQ;
	GSEQ++;
	if (GSEQ >= 0x0f)
		GSEQ = 0x00;
	return SEQ;
}

void GetFn(int fn, uchar *FnChar)
{
	uchar *ptr = FnChar;

	memcpy(ptr, "\x00\x00", 2);
	ptr += 2;
	*ptr++ = 1 << ((fn - 1) % 8);
	*ptr = (fn - 1) / 8;
}

void GetPnFn(int pn, int fn, uchar *DaDt)
{
	uchar *ptr = DaDt;

	*ptr++ = 1 << ((pn - 1) % 8);
	*ptr++ = ((pn - 1) / 8) + 1;
	*ptr++ = 1 << ((fn - 1) % 8);
	*ptr = (fn - 1) / 8;
}

int GetIntFn(uchar *FnChar)
{
	int di = 0, i = 0, ret = 0;

	di = FnChar[3];
	for (i = 0; i < 8; i++)
	{
		if ((1 << i) == FnChar[2])
			break;
	}
	ret = di * 8 + i + 1;

	return ret;
}

void FillMsgOut(ST_MSG *msg, uchar afn, uchar *data, int MsgLen,
		uchar CtrlCode, int SetQueryAddrFlag, int ExistPw, int ExistTp)
{
	uchar TPV;

	msg->code = DIR_BIT | PRM_BIT | FCB_BIT | FCV_BIT | CtrlCode;
	if (SetQueryAddrFlag == 0)
	{
		memcpy(msg->addr, GAddr, 5);
		TPV = TPV_BIT;
	}
	else
	{
		memcpy(msg->addr, "\xff\xff\xff\xff\x2c", 5);
		TPV = TPV_BIT | 0x80;
	}
	msg->afn = afn;
	msg->seq = TPV | FIR_BIT | FIN_BIT | CON_BIT | GetSEQ();
	msg->size = MsgLen;
	msg->data = data;
	if (ExistPw && ExistTp)
		msg->aux_flag = 0x05;
	else if (ExistPw)
	{
		msg->aux_flag = 0x01;
		memcpy(msg->aux_pw, "\x00\x00", 2);
	}
	else if (ExistTp)
		msg->aux_flag = 0x04;
	else
		msg->aux_flag = 0x00;
}

void PrintCommStat(int flag)
{
	LcdStart();
	switch (flag)
	{
	case COMMFAIL:
		printf("\n通信失败,请重试!\n");
		break;
	case SETFAIL:
		printf("\n设置失败,请重试!\n");
		break;
	case SETSUC:
		printf("\n设置成功!\n");
		break;
	case SAVEFAIL:
		printf("\n保存数据失败,请\n重试!\n");
		break;
	case SAVESUC:
		printf("\n保存数据成功!\n");
		break;
	case RECVPACKERR:
		printf("\n接收数据包格式不\n对，请重试!\n");
		break;
	case EMPTY:
		printf("\n数据域为空!\n");
		break;
	case DELSUC:
		printf("\n删除数据成功!\n");
		break;
	default:
		break;
	}
}

int PrintRdRecProcess(int MtIdx, int Cnt, int stat, int flag)
{
	LcdStart();
	switch (flag)
	{
	case RDFROZEN:
		printf("  抄读冻结电量\n");
		printf("表序号: %d\n", MtIdx);
		printf("当前第%d天(月)\n", Cnt);
		break;
	case RDMTARCH:
		printf("  抄读电表档案\n");
		printf("表序号: %d\n", MtIdx);
		break;
	default:
		break;
	}
	printf("状态:");
	switch (stat)
	{
	case COMMINGSTAT:
		printf("正在通信\n");
		break;
	case DATAEMPSTAT:
		printf("数据为空\n");
		printf("是否继续读后续电\n表或者重试?\n1:否 2:是 3:重试\n请选择:");
		return GetChoice('\0', 1, 3) - 1;
	case COMMFAILSTAT:
		printf("通信失败\n");
		printf("是否继续读后续电\n表或者重试?\n1:否 2:是 3:重试\n请选择:");
		return GetChoice('\0', 1, 3) - 1;
	case SAVESUCSTAT:
		printf("保存数据成功\n");
		break;
	case SAVEFAILSTAT:
		printf("保存失败\n");
		printf("是否继续读后续电\n表或者重试?\n1:否 2:是 3:重试\n请选择:");
		return GetChoice('\0', 1, 3) - 1;
	case PACKERRSTAT:
		printf("接受包错误\n");
		printf("是否继续读后续电\n表或者重试?\n1:否 2:是 3:重试\n请选择:");
		return GetChoice('\0', 1, 3) - 1;
	case FEILVCNTOVER:
		printf("\n费率数超过4,不能\n存入数据库!");
		printf("是否继续读后续电\n表或者重试?\n1:否 2:是 3:重试\n请选择:");
		return GetChoice('\0', 1, 3) - 1;
	case RECNO_TOO_BIG:
		printf("无效的记录数\n");
		printf("是否继续读后续电\n表或者重试?\n1:否 2:是 3:重试\n请选择:");
		return GetChoice('\0', 1, 3) - 1;
	default:
		printf("错误码%d\n", stat);
		printf("是否继续读后续电\n表或者重试?\n1:否 2:是 3:重试\n请选择:");
		return GetChoice('\0', 1, 3) - 1;
	}
	return 0;
}

void PrintEnergyFormat03(uchar *engy)
{
	uchar *ptr = engy + 3;
	uchar unit[5];
	uchar tmp;
	int i, StartFlag = 0;

	if (((*ptr >> 4) & 0x01) == 1) //energy is minus
		printf("-");
	if (((*ptr >> 4) & 0x04) == 1)
		strcpy(unit, "MWh");
	else
		strcpy(unit, "KWh");
	tmp = *ptr & 0x0f;
	if ((tmp > 0x00) && (tmp <= 0x09))
	{
		printf("%d", tmp);
		StartFlag = 1;
	}
	ptr--;
	for (i = 0; i < 3; i++)
	{
		tmp = (*ptr >> 4) & 0x0f;
		if (StartFlag == 1)
			printf("%d", tmp);
		else
		{
			if ((tmp > 0x00) && (tmp <= 0x09))
			{
				printf("%d", tmp);
				StartFlag = 1;
			}
		}
		tmp = *ptr & 0x0f;
		if (StartFlag == 1)
			printf("%d", tmp);
		else
		{
			if ((tmp > 0x00) && (tmp <= 0x09))
			{
				printf("%d", tmp);
				StartFlag = 1;
			}
		}
		ptr--;
	}
}

int EnterNo(char *buf, int len)
{
	int length, i;

	if (GetString(buf, len, TYPE_HEX) == 0)
		return 0;
	length = strlen(buf);
	if (length < len)//表号不够12位,用0补齐12位
	{
		strrev(buf);
		for (i = 0; i < (len - length); i++)
		{
			buf[length + i] = '0';
		}
		strrev(buf);
	}
	return 1; // succeed
}

void PrintMtIdx(uchar *idx)
{
	printf("【电表表序号】\n");
	printf("% ld\n", Scan2HexToData(idx, LowToHigh));
}

void PrintMtNum(uchar *num, int len) //hold 2 lines on screen
{
	int i;

	printf("【电表表号】\n");
	for (i = len - 1; i >= 0; i--)
		printf("%02x", num[i]);

	printf("\n");
}

void PrintMeasPro(uchar *pro) //hold 3 lines on screen
{
	printf("【测量点性质】\n");
	switch (*pro >> 4)
	{
	case 0x00:
		printf("普通485表\n");
		break;
	case 0x01:
		printf("载波通信表\n");
		break;
	case 0x02:
		printf("简易多功能表\n");
		break;
	case 0x03:
		printf("多功能总表\n");
		break;
	case 0x04:
		printf("中继器\n");
		break;
	default:
		break;
	}
	switch (*pro & 0x0f)
	{
	case 0x00:
		printf("预付电量表\n");
		break;
	case 0x01:
		printf("预付费表\n");
		break;
	case 0x02:
		printf("非预付费(电量)表\n");
		break;
	default:
		break;
	}
}

void PrintConnWay(uchar *way) //hold 3 lines on screen
{
	printf("【接线方式】\n");
	if ((*way & 0x02) == 0x00)
		printf("单相表\n");
	else
		printf("三相表\n");
	if ((*way & 0x01) == 0x00)
		printf("直接接线\n");
	else
		printf("经互感器接线\n");
}

void PrintFvNum(uchar *fv) //hold 3 lines on screen
{
	printf("【费率方案】\n");
	switch (*fv >> 4)
	{
	case 0x00:
		printf("单费率电表\n");
		break;
	case 0x01:
		printf("费率方案号1\n");
		break;
	case 0x02:
		printf("费率方案号2\n");
		break;
	case 0x03:
		printf("费率方案号3\n");
		break;
	case 0x04:
		printf("费率方案号4\n");
		break;
	case 0x05:
		printf("费率方案号5\n");
		break;
	case 0x06:
		printf("费率方案号6\n");
		break;
	case 0x07:
		printf("费率方案号7\n");
		break;
	case 0x08:
		printf("费率方案号8\n");
		break;
	default:
		break;
	}
	switch (*fv & 0x0f)
	{
	case 0x00:
		printf("总电量\n");
		break;
	case 0x01:
		break;
	case 0x02:
		printf("总,1,2电量\n");
		break;
	case 0x03:
		printf("总,1,2,3电量\n");
		break;
	case 0x04:
		printf("总,1,2,3,4电量\n");
		break;
	default:
		break;
	}
}

void PrintMtType(uchar *type) //hold 4 or 5 lines on screen
{
	printf("【电表类型】\n");
	switch (*type & 0x03)
	{
	case 0x00:
		printf("相位不确定\n");
		break;
	case 0x01:
		printf("A相;");
		break;
	case 0x02:
		printf("B相;");
		break;
	case 0x03:
		printf("C相;");
		break;
	}
	if ((*type & 0x04) == 0x00)
		printf("不带拉闸\n");
	else
		printf("带拉闸\n");
	if ((*type & 0x08) == 0x00)
		printf("普通用户;");
	else
		printf("重点用户;");
	if ((*type & 0x10) == 0x00)
		printf("不选抄\n");
	else
		printf("选抄\n");
	if ((*type & 0x20) == 0x00)
		printf("允许断电\n");
	else
		printf("不允许断电\n");

}

void PrintLineNum(uchar *line) //hold 2 lines on screen
{
	printf("【线路编号】\n");
	printf("    %02x%02x\n", line[1], line[0]);
}

void PrintBoxNum(uchar *box) //hold 2 lines on screen
{
	printf("【表箱编号】\n");
	printf("  %ld\n", Scan2HexToData(box, LowToHigh));
}

void PrintCollNum(uchar *coll, int len) //hold 2 lines on screen
{
	int i;

	printf("【采集终端编号】\n");
	for (i = len - 1; i >= 0; i--)
	{
		printf("%02x", coll[i]);
	}
	printf("\n");
}

int CheckAndDelRec(struct DBF *db, int idx)
{
	int i, ret;
	uchar buf[25], MtIdx[2];

	ScanDataTo2Hex((long) idx, MtIdx, LowToHigh);
	for (i = 1; i <= db->records; i++)
	{
		//db->record_ptr = buf;
		d_getrec(db, (long) i);
		memcpy(buf, db->record_ptr, db->record_length);
		if (memcmp(buf, "\x20", 1) != 0)
			continue;
		if (memcmp(buf + 1, MtIdx, 2) == 0)
		{
			ret = d_delrec(db, i);
			if (ret != 0)
				return ret;
			break;
		}
	}
	return 0;
}

int GetMtIdxInput(int *idx, char *notice)
{
	int val;
	char str[6] = "\0";

	do
	{
		LcdStart();
		if (notice != NULL)
			printf("%s\n", notice);
		printf("电表序号:");
		if (GetString(str, 4, TYPE_NUMBER) == 0)
			return 0;
		val = atoi(str);
	} while (CheckRange((long) val, 1, 1024) == 0);
	*idx = val;

	return 1;
}

int GetMtNumInput(char *num, char *notice)
{
	char str[13] = "\0";
	int i, len;

	LcdStart();
	if (notice != NULL)
		printf("%s\n", notice);
	printf("电表表号:\n");
	if (GetString(str, 12, TYPE_NUMBER) == 0)
		return 0;
	len = strlen(str);
	for (i = 0; i < (12 - len); i++)
	{
		num[i] = '0';
	}
	memcpy(num + (12 - len), str, len);

	return 1;
}

void PrintDataFormat11(uchar *msg)
{
	int i;

	for (i = 3; i >= 0; i--)
	{
		if (i == 0)
			printf(".");
		printf("%02x", msg[i]);
	}
	printf("KWh");
}

void PrintDataFormat15(uchar *data)
{
	printf("%02x-%02x-%02x  %02x:%02x", data[4], data[3], data[2], data[1],
			data[0]);
}

void PrintDataFormat17(uchar *data)
{
	printf("%02x-%02x %02x:%02x", data[3], data[2], data[1], data[0]);
}

void PrintDataFormat08(uchar *data)
{
	printf("%02x%02x", data[1], data[0]);
}

void PrintDataFormat10(uchar *data)
{
	printf("%02x%02x%02x分钟", data[2], data[1], data[0]);
}

void PrintDataFormat13(uchar *data)
{
	printf("%02x%02x.%02x%02xKWh", data[3], data[2], data[1], data[0]);
}

void PrintDataFormat1(uchar *msg)
{
	printf("日期:%02d-%02d-%02d\n", BcdToChar(msg[5]),
			BcdToChar(msg[4] & 0x1f), BcdToChar(msg[3]));
	printf("时间:%02d:%02d:%02d\n", BcdToChar(msg[2]), BcdToChar(msg[1]),
			BcdToChar(msg[0]));
	PrintWeek((msg[4] >> 5) & 0x07);
}

void PrintWeek(uchar ch)
{
	switch (ch)
	{
	case 1:
		printf("%s\n", Monday);
		break;
	case 2:
		printf("%s\n", Tuesday);
		break;
	case 3:
		printf("%s\n", Wednesday);
		break;
	case 4:
		printf("%s\n", Thursday);
		break;
	case 5:
		printf("%s\n", Friday);
		break;
	case 6:
		printf("%s\n", Saturday);
		break;
	case 7:
		printf("%s\n", Sunday);
		break;
	default:
		printf("星期:无效\n");
	}
}

void TimeToClock(uchar *clock)
{
	struct time time;
	struct dosdate_t date;
	uchar TimeBcd[8];

	_dos_getdate(&date);
	gettime(&time);
	TimeBcd[0] = ((date.year / 1000) << 4) + (date.year / 100) % 10;
	TimeBcd[1] = (((date.year / 10) % 10) << 4) + (date.year % 10);
	TimeBcd[2] = (((date.month / 10) % 10) << 4) + date.month % 10;
	TimeBcd[3] = (((date.day / 10) % 10) << 4) + date.day % 10;
	TimeBcd[4] = (((time.ti_hour / 10) % 10) << 4) + time.ti_hour % 10;
	TimeBcd[5] = (((time.ti_min / 10) % 10) << 4) + time.ti_min % 10;
	TimeBcd[6] = (((time.ti_sec / 10) % 10) << 4) + time.ti_sec % 10;
	TimeBcd[7] = (((date.dayofweek / 10) % 10) << 4) + date.dayofweek % 10;
	BcdToAscii(clock, TimeBcd, 8);
}

int ClearDBF(uchar *filename, struct FIELD_RECORD *record, int count)
{
	int errornum;
	struct DBF db;

	strcpy(db.filename, filename);
	if ((errornum = d_create(&db, db.filename, record, count)) != 0)
	{
		d_error(errornum, db.filename);
		return -1;
	}

	return 0;
}

void GetLocalClock(uchar *clock)
{
	struct time time;
	struct dosdate_t date;
	uchar TimeBcd[8];

	_dos_getdate(&date);
	gettime(&time);
	clock[0] = (((time.ti_sec / 10) % 10) << 4) + time.ti_sec % 10;
	clock[1] = (((time.ti_min / 10) % 10) << 4) + time.ti_min % 10;
	clock[2] = (((time.ti_hour / 10) % 10) << 4) + time.ti_hour % 10;
	clock[3] = (((date.day / 10) % 10) << 4) + date.day % 10;
	if (date.dayofweek == 0)
		date.dayofweek = 7;
	clock[4] = ((date.dayofweek << 5) & 0xe0)
			+ ((((date.month / 10) % 10) << 4) & 0x10) + ((date.month % 10)
			& 0x0f);
	clock[5] = (((date.year / 10) % 10) << 4) + (date.year % 10);
}

int CheckRange(long val, long min, long max)
{
	if ((val >= min) && (val <= max))
		return 1;
	else
	{
		printf("错误: 数据超出范\n围[%ld-%ld].", min, max);
		getch();
		return 0;
	}
}

int CheckStrLen(uchar *str, int min, int max)
{
	int len;

	len = strlen(str);
	if (min == max)
	{
		if (len == min)
			return 1;
		else
		{
			printf("错误: 字串长度应\n为%d.", min);
			getch();
			return 0;
		}
	}
	else if (min < max)
	{
		if ((len >= min) && (len <= max))
			return 1;
		else
		{
			printf("错误: 字串长度应\n在[%d-%d]之间.", min, max);
			getch();
			return 0;
		}
	}
	else
	{
		printf("错误: 软件出错,\n最小值应小于最大\n值.");
		getch();
		return 0;
	}
}

int CheckChoiceInput(char *cmd, char min, char max)
{
	if ((cmd[0] >= min) && (cmd[0] <= max))
		return 1;
	else
	{
		printf("错误：输入数据超\n出范围.");
		getch();
		return 0;
	}
}

uchar GetDirection(int LfFlg, int RhFlg, int UpFlg, int DnFlg)
{
	uchar ch;

	while (1)
	{
		ch = getch();
		switch (ch)
		{
		case 'K':
			if (LfFlg == 1)
				return ch;
			else
				continue;
		case 'M':
			if (RhFlg == 1)
				return ch;
			else
				continue;
		case 'H':
			if (UpFlg == 1)
				return ch;
			else
				continue;
		case 'P':
			if (DnFlg == 1)
				return ch;
			else
				continue;
		case 27:
			return ch;
		default:
			continue;
		}
	}
}

int GetYesOrNo(void)
{
	uchar ch;

	while (1)
	{
		ch = getch();
		if (ch == 27)
			return 0;
		if (ch == '\r')
			return 1;
	}
}

void SetCOMBaud(void)
{
	char str[3] = "4";
	int val;

	do
	{
		LcdStart();
		printf(" 设置COM%d波特率\n", G_COMPort);
		printf("1:1200\n2:2400\n3:4800\n4:9600\n5:38400\n请选择:");
		if (GetString(str, 1, TYPE_NUMBER) == 0)
			return;
		val = atoi(str);
	} while (CheckRange(val, 1, 5) == 0);
	switch (val)
	{
	case 1:
		GBaud = 1200;
		break;
	case 2:
		GBaud = 2400;
		break;
	case 3:
		GBaud = 3800;
		break;
	case 4:
		GBaud = 9600;
		break;
	case 5:
		GBaud = 38400;
		break;
	}
}

void WaitForESCKey(void)
{
	char ch;

	while (1)
	{
		ch = getch();
		if (ch == 27)
			return;
	}
}

void WaitForEnterKey(void)
{
	char ch;

	while (1)
	{
		ch = getch();
		if (ch == 13)
			return;
	}
}

int SaveINI(char *id, char *psw, char *addr, int com, long baud)
{
	FILE *fp;
	char str[100], BaudStr[10];

	strcpy(str, "UserID=");
	strcat(str, id);
	strcat(str, "\n");

	strcat(str, "PSW=");
	strcat(str, psw);
	strcat(str, "\n");

	strcat(str, "LVCAddr=");
	strcat(str, addr);
	strcat(str, "\n");

	strcat(str, "COM=");
	if (com == 1)
		strcat(str, "COM1");
	else
		strcat(str, "COM2");
	strcat(str, "\n");

	strcat(str, "Baud=");
	LongToStr(BaudStr, baud);
	strcat(str, BaudStr);

	if ((fp = fopen("LVCPOS.ini", "w+")) != NULL)
	{
		fputs(str, fp);
	}
	fclose(fp);

	return 1;
}

int IsFindEnter(char *str)
{
	char *ptr = str;

	while (*ptr != '\0')
	{
		if (*ptr == '\n')
			return 1;
		ptr++;
	}
	return 0;
}

int MsgComm(uchar *BufOut, int BufOutLen, uchar *BufIn, int timeout)
{
	int BufInLen = 0;

	LcdStart();
	LcdPrintS(1, 0, Sending);
	OpenCom(G_COMPort, GBaud);
	Send(BufOut, BufOutLen);
	BufInLen = Receive(BufIn, 0x16, 0x16, 0, timeout);
	/*LcdStart();
	 TestBuf(BufIn, BufInLen);*/
	CloseCom(G_COMPort);

	return BufInLen;
}

int CopyLVCAddr(uchar *DstAddr, uchar *SrcAddr, int len)
{
	DstAddr[0] = SrcAddr[1];
	DstAddr[1] = SrcAddr[0];
	DstAddr[2] = SrcAddr[3];
	DstAddr[3] = SrcAddr[2];

	return 1;
}

int CheckPacketIn(uchar *PacketIn, int PacketInLen)
{
	int len = 0;

	len = ((PacketIn[2] << 8) + PacketIn[1]) >> 2;
	if (PacketInLen != (6 + len + 2))
		return 0;

	return 1;
}

int IsAction(uchar *notice)
{
	LcdStart();
	printf("\n\n%s\n", notice);
	printf("\n\n\n\n否            是");

	return GetYesOrNo();
}

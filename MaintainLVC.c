/*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <stdio.h>
#include <mem.h>
#include <conio.h>
#include <stdlib.h>
#include "typedef.h"
#include "strdef.h"
#include "menu.h"
#include "lcdout.h"
#include "lvc_pos.h"
#include "common.h"
#include "global.h"
#include "rs232.h"
#include "input.h"
#include "md5.h"
#include "util.h"

int PackSize = 512;
int ErrorCount = 0;
int MaxError = 8;

int FillQueryLVCData(int fn, uchar *data, uchar ReadMode)
{
	BYTE *ptr = data;
	BYTE FnChar[4];

	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = ReadMode;
	return ptr - data;
}

/*
 * 填充链路用户数据
 *
 * args:
 * data		链路用户数据
 * fn		功能码Fn
 * fid		文件id
 * fatt		文件属性
 * EndFlag	文件尾标记
 * count	文件块总数
 * off		块偏移
 * len		块长度
 * buf		文件块
 *
 * return	链路用户数据长度
 * */
static int FillUpdateLVCData(BYTE *data, int fn, BYTE fid, BYTE fatt,
		BYTE EndFlag, WORD count, DWORD off, WORD len, BYTE *buf)
{
	BYTE *ptr = data;
	BYTE FnChar[4];
	MD5_CTX md5;
	BYTE hash[16];
	int i;

	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;

	*ptr++ = fid;
	*ptr++ = fatt;
	*ptr++ = EndFlag;
	stoc(ptr, count);
	ptr += 2;
	ltoc(ptr, off);
	ptr += 4;
	stoc(ptr, len);
	ptr += 2;

	if (buf != NULL)
	{
		memcpy(ptr, buf, len);
		ptr += len;
	}

	MD5Iter(&hash, data + 4, len + 11, 128);
	memcpy(ptr, hash, 8);
	ptr += 8;

	return ptr - data;
}

static void PrintSHVersion(uchar *ptr)
{
	int i;

	for (i = 0; i < 8; i++)
		printf("%02d", BcdToChar(*(ptr + i)));
}

static void PrintProtocolVersion(uchar *ptr, uchar ch)
{
	int i;

	switch (ch)
	{
	case 'a':
		for (i = 0; i < 6; i++)
			printf("%02d", BcdToChar(*(ptr + i)));
		break;
	case 'm':
		printf("%02d", BcdToChar(*ptr));
		break;
	case '1':
		printf("%02d", BcdToChar(*(ptr + 1)));
		break;
	case '2':
		printf("%02d", BcdToChar(*(ptr + 2)));
		break;
	default:
		break;
	}
}

static int FillSetLVCClockData(int fn, uchar *data, uchar *clock)
{
	uchar *ptr = data;
	uchar FnChar[4];

	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, clock, 6);
	ptr += 6;

	return ptr - data;
}

static int FillResetMainBoardData(int fn, uchar *data)
{
	uchar *ptr = data;
	uchar FnChar[4];

	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, "\x00\x88\x90\x00\x01\x01", 6);
	ptr += 6;

	return ptr - data;
}

static int FillResetPlcBoardData(int fn, uchar *data)
{
	uchar *ptr = data;
	uchar FnChar[4];

	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, "\x00\x88\x90\x00\x01\x03", 6);
	ptr += 6;

	return ptr - data;
}

static int FillResetWirelessModemData(int fn, uchar *data)
{
	uchar *ptr = data;
	uchar FnChar[4];

	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, "\x00\x88\x90\x00\x01\x02", 6);
	ptr += 6;

	return ptr - data;
}

static int GetReturnMtCnt(uchar *ptr)
{
	return *ptr + ((*(ptr + 1)) << 8);
}

static int FillQueryMtStatWordData(int fn, uchar *data, int idx)
{
	uchar *ptr = data;
	uchar FnChar[4], MtIdx[2];

	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = 0x00;
	*ptr++ = 0x01;
	*ptr++ = 0x00;
	ScanDataTo2Hex((long) idx, MtIdx, LowToHigh);
	memcpy(ptr, MtIdx, 2);
	ptr += 2;

	return ptr - data;
}

static void PrintMtStatWord(uchar *msg)
{
	printf("继电器状态:");
	if (*msg & 0x40)
		printf("断\n");
	else
		printf("合\n");
	printf("有功电能方向:");
	if (*msg & 0x10)
		printf("反\n");
	else
		printf("正\n");
	printf("电池电压:");
	if (*msg & 0x04)
		printf("欠压\n");
	else
		printf("正常\n");
	printf("抄表方式:");
	if (*msg & 0x01)
		printf("手动\n");
	else
		printf("自动\n");
}

static void QueryAutoFlag(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar flag = 1;
	uchar data[5], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen = 0;

	DataLen = FillQueryLVCData(2, data, 0x03);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 2))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("    查询结果\n");
		printf("抄表日完成时间:\n%02d日%02d时%02d分\n", BcdToChar(*(MsgIn.data
				+ 6)), BcdToChar(*(MsgIn.data + 5)), BcdToChar(
				*(MsgIn.data + 4)));
		printf("轮抄结束标志:\n");
		flag = *(MsgIn.data + 7);
		if (flag == 0x00)
			printf("   轮抄结束\n");
		else if (flag == 0x55)
			printf("   正在轮抄...\n");
		else
			printf("  未知的标志\n");
		WaitForESCKey();
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QuerySoftHardVersion(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], addr[4], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	DataLen = FillQueryLVCData(9, data, 0x03);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 9))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("   集中器版本\n");
		printf("软件版本号:\n");
		PrintSHVersion(MsgIn.data + 4);
		printf("\n硬件版本号:\n");
		PrintSHVersion(MsgIn.data + 12);
		WaitForESCKey();
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryProtocolVersion(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], addr[4], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	DataLen = FillQueryLVCData(10, data, 0x03);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 10))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf(" 集中器协议版本\n");
		printf("集中器协议版本号:\n");
		PrintProtocolVersion(MsgIn.data + 4, 'a');
		printf("\n协议主号:");
		PrintProtocolVersion(MsgIn.data + 4, 'm');
		printf("\n协议分号1:");
		PrintProtocolVersion(MsgIn.data + 4, '1');
		printf("\n协议分号2:");
		PrintProtocolVersion(MsgIn.data + 4, '2');
		printf("\n日期:%02d-%02d-%02d\n", BcdToChar(*(MsgIn.data + 9)),
				BcdToChar(*(MsgIn.data + 8)), BcdToChar(*(MsgIn.data + 7)));
		WaitForESCKey();
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryChooseMtCnt(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], addr[4], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	DataLen = FillQueryLVCData(17, data, 0x03);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 17))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("    查询结果\n");
		printf("集中器指定进行抄\n读的表数量:%d\n", GetReturnMtCnt(MsgIn.data
				+ 4));
		WaitForESCKey();
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryFinishRdMtCnt(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], addr[4], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	DataLen = FillQueryLVCData(18, data, 0x03);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 18))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("    查询结果\n");
		printf("集中器成功完成抄\n读的表数量:%d\n", GetReturnMtCnt(MsgIn.data
				+ 4));
		WaitForESCKey();
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryStatMeterCnt(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], addr[4], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	DataLen = FillQueryLVCData(19, data, 0x03);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 19))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("  表计总数统计\n");
		printf("普通用户表:%d\n", GetReturnMtCnt(MsgIn.data + 4));
		printf("简易多功能表:%d\n", GetReturnMtCnt(MsgIn.data + 6));
		printf("重点户电能表:%d\n", *(MsgIn.data + 8));
		printf("载波电能表:%d\n", GetReturnMtCnt(MsgIn.data + 9));
		printf("RS485电表:%d\n", GetReturnMtCnt(MsgIn.data + 11));
		printf("集中器管理的电表\n总数:%d", GetReturnMtCnt(MsgIn.data + 13));
		WaitForESCKey();
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryMtStatWord(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], addr[4], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	int MtIdx;

	if (GetMtIdxInput(&MtIdx, "  电表状态字") == 0)
		return;
	DataLen = FillQueryMtStatWordData(74, data, MtIdx);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_LONG);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 74))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		if ((*(MsgIn.data + 4) != 1) || ((int) Scan2HexToData(MsgIn.data + 5,
				LowToHigh) != MtIdx))
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("   电表状态字\n");
		printf("表序号:%d\n", MtIdx);
		PrintMtStatWord(MsgIn.data + 7);
		WaitForESCKey();
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void LVCStat(void)
{
	void (*p[])(void) =
	{ QuerySoftHardVersion, QueryProtocolVersion, QueryChooseMtCnt,
			QueryFinishRdMtCnt, QueryAutoFlag, QueryStatMeterCnt };
	const uchar *buf[6];

	buf[0] = QuerySoftHardVersionStr;
	buf[1] = QueryProtocolVersionStr;
	buf[2] = QueryChooseMtCntStr;
	buf[3] = QueryFinishRdMtCntStr;
	buf[4] = QueryAutoFlagStr;
	buf[5] = QueryStatMeterCntStr;

	InitMenu(buf, p, 6, 0);
}

static void RdDiagnoseInfo(void)
{
}

static void SetLVCClock(void)
{
	uchar clock[6];
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[127], PacketIn[255];
	uchar CommCtrlWord;
	int RecvCascadeTimeOut, RecvTimeOut, DelayTimeOut, ResponseTimeOut,
			ReSendTimes;
	int PacketOutLen, DataLen, PacketInLen;

	GetLocalClock(clock);
	DataLen = FillSetLVCClockData(31, data, clock);
	FillMsgOut(&MsgOut, 0x05, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
			PrintCommStat(SETFAIL);
		else
			PrintCommStat(SETSUC);
		WaitForESCKey();
		return;
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryLVCRtc(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	DataLen = FillQueryLVCData(1, data, 0x03);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 1))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("   集中器时钟\n");
		printf("日期:%02d-%02d-%02d\n", BcdToChar(*(MsgIn.data + 9)),
				BcdToChar(*(MsgIn.data + 8) & 0x1f), BcdToChar(
						*(MsgIn.data + 7)));
		printf("时间:%02d:%02d:%02d\n", BcdToChar(*(MsgIn.data + 6)),
				BcdToChar(*(MsgIn.data + 5)), BcdToChar(*(MsgIn.data + 4)));
		PrintWeek((*(MsgIn.data + 8) >> 5) & 0x07);
		WaitForESCKey();
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void ResetMainBoard(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	if (IsAction("确定要复位吗?") == 0)
		return;
	DataLen = FillResetMainBoardData(110, data);
	FillMsgOut(&MsgOut, 0x84, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		else
		{
			LcdStart();
			printf("\n\n复位集中器主板成\n功.");
			WaitForESCKey();
		}
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void ResetPlcBoard(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	if (IsAction("确定要复位吗?") == 0)
		return;
	DataLen = FillResetPlcBoardData(110, data);
	FillMsgOut(&MsgOut, 0x84, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		else
		{
			LcdStart();
			printf("\n\n复位集中器载波板\n成功.");
			WaitForESCKey();
		}
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void ResetWirelessModem(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	if (IsAction("确定要复位吗?") == 0)
		return;
	DataLen = FillResetWirelessModemData(110, data);
	FillMsgOut(&MsgOut, 0x84, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if (PacketInLen > 0)
	{
		if (CheckPacketIn(PacketIn, PacketInLen) == 0)
		{
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if ((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		else
		{
			LcdStart();
			printf("\n\n复位集中器无线模\n块成功.");
			WaitForESCKey();
		}
	}
	else
	{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

void ResetLVC(void)
{
	void (*p[])(void) =
	{ ResetMainBoard, ResetPlcBoard, ResetWirelessModem };
	const uchar *buf[3];

	buf[0] = ResetMainBoardStr;
	buf[1] = ResetPlcBoardStr;
	buf[2] = ResetWirelessModemStr;

	InitMenu(buf, p, 3, 0);
}

/*
 * 读取确认/否认报文
 *
 * return	Fn
 * 			-1	该报文不是有效确认/否认报文
 */
BYTE GetConfirmNo(ST_MSG msg)
{
	BYTE *buf = msg.data;
	if (msg.code & 0xFF != 0x80 || msg.afn != 0x00 || msg.seq & 0xFF != 0x60
			|| ctos(buf) != 0x00)
	{
		return -1;
	}
	buf += 2;
	return ctos(buf);
}

/*
 * 更新文件块
 *
 * args:
 * buf		文件块数据
 * len		文件块长度
 * flag
 *
 * */
int UpdataLVCFilePack(uchar *buf, int len, int fid, int fatt, int count,
		int off)
{
	ST_MSG MsgOut, MsgIn;
	BYTE *PacketOut;
	BYTE PacketIn[128];
	BYTE seq;
	BYTE *data;
	int PacketOutLen, DataLen;
	BYTE EndFlag;

	BYTE *ptr;

	data
			= (BYTE*) malloc((4 + 2 + 1 + 2 + 4 + 2 + PackSize + 8)
					* sizeof(BYTE));/*DA+DT,File Info,End Flag,count,off,len,buf,MD5*/
	PacketOut = (BYTE*) malloc((6 + 1 + 5 + 1 + 1 + 4 + 2 + 1 + 2 + 4 + 2
			+ PackSize + 8 + 2 + 1 + 1) * sizeof(BYTE));/*Head,Ctrl Code,Address,AFN,SEQ,DA+DT,File Info,End Flag,count,off,len,buf,MD5,PW,CS,End*/
	EndFlag = buf == NULL ? 1 : 0;
	DataLen = FillUpdateLVCData(data, 1, fid, fatt, EndFlag, count, off, len,
			buf);
	seq = off & 0x0F;
	seq = 0x10;
	if (off == 0)
	{
		seq |= 0x40;
	}
	if (EndFlag == 1)
	{
		seq |= 0x20;
	}
	FillMsgOut(&MsgOut, 0x0f, data, DataLen, 0x01, 0, 1, 0);
	for (; ErrorCount < MaxError;)/*失败重传*/
	{
		PacketInLen = MsgComm(PacketOut, PacketInLen, PacketIn,
				RECVTIMEOUT_SHORT);
		if (PacketInLen > 0)
		{
			if (CheckPacketIn(PacketIn, PacketInLen) == 0)
			{
				PrintCommStat(RECVPACKERR);
				WaitForESCKey();
				return;
			}
			unpack(PacketIn, PacketInLen, &MsgIn);
			if ((MsgIn.afn == 0x00) || (GetIntFn(MsgIn.data) == 1))/*FIXME 补全回复报文检查*/
			{
				LcdStart();
				printf("更新块成功");
				WaitForESCKey();
				break;
			}
			else
			{
				ErrorCount++;
				LcdStart();
				printf("更新块失败\n重传");
				WaitForESCKey();
			}
			return;
		}
		else
		{
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
		}
	}
	return 1;
}

void UpdateLVC(void)
{
	FILE *fp;
	BYTE *buf;
	BYTE fid, fatt;
	WORD count, len;
	DWORD off;

	if (IsAction("确定要升级吗?") == 0)
	{
		return;
	}
	if ((fp = fopen("cscon.dwl", "r")) == NULL)
	{
		LcdStart();
		printf("文件不存在\n升级失败");
		WaitForESCKey();
		return;
	}
	fid = 0x00;
	fatt = 0x00;
	off = 0x00;
	count = fsize(fp) / PackSize + 1;
	LcdStart();
	printf("开始升级\n");
	printf("共 %d 包\n", count);
	buf = (BYTE*) malloc(PackSize * sizeof(BYTE));
	while ((len = fread(buf, sizeof(BYTE), PackSize, fp)) != 0)
	{
		UpdateLVCFilePack(buf, len, fid, fatt, count - 1, off++);
	}
	UpdataLVCFilePack(NULL, 0, fid, fatt, count - 1, off++);/*升级文件结束报文*/
	fclose(fp);
	LcdStart();
	printf("集中器升级成功");
	WaitForESCKey();
}

void MaintainLVC(void)
{
	void (*p[])(void) =
	{ LVCStat, RdDiagnoseInfo, SetLVCClock, QueryLVCRtc, ResetLVC, UpdateLVC };
	const uchar *buf[6];

	buf[0] = LVCStatStr;
	buf[1] = RdDiagnoseInfoStr;
	buf[2] = SetLVCClockStr;
	buf[3] = QueryLVCRtcStr;
	buf[4] = ResetLVCStr;
	buf[5] = "6.升级集中器程序";/*FIXME*/

	InitMenu(buf, p, 6, 0);
}

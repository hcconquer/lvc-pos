 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <conio.h>
#include <stdio.h>
#include "input.h"
#include "lcdout.h"
#include "htlcd.H"

void LcdStart(void)
{
	clrscr();
	window(1, 1, SCREENX + 1, SCREENY + 1);
}

void ClearWindow(int StartX, int StartY, int EndX, int EndY)
{
	window(StartX, StartY, EndX, EndY);
	clrscr();
	window(1, 1, SCREENX + 1, SCREENY + 1);
}

void LcdPrintC(uchar line,uchar colum, uchar ascii)
{
	uchar SingleChar[2] = {" "};
	
	SingleChar[0] = ascii;
	gotoxy(colum + 1, line + 1);
	cprintf(SingleChar);
}
void LcdTypeMem(uchar isRow,uchar isCol, uchar isMem)
{
	uchar AsciiMem[3];
	
	#ifndef NUMPOSITION;
	#define NUMPOSITION 0x30
	#endif	//end ifndef NUMPOSITION
	
	AsciiMem[0] = (isMem >> 4) + NUMPOSITION;
	AsciiMem[1] = (isMem & 0x0f)  + NUMPOSITION;
	if (AsciiMem[0] == ':') { // ':' = 0x0A + NUMPOSITION
		AsciiMem[0] = '.';
	}
	if (AsciiMem[1] == ':') {
		AsciiMem[1] = '.';
	}
	AsciiMem[2] = 0;
	LcdPrintS(isRow, isCol, AsciiMem);
}

void LcdTypeNum(uchar isRow,uchar isCol, uchar isNum)
{
	uchar AsciiNum[4];
	
	AsciiNum[0] = isNum % 10 + NUMPOSITION;
	if (isNum > 9) {
		AsciiNum[1] = AsciiNum[0];
		AsciiNum[0] = (isNum % 100) / 10 + NUMPOSITION;
		if (isNum > 99) {
			AsciiNum[2] = AsciiNum[1];
			AsciiNum[1] = AsciiNum[0];
			AsciiNum[0] = isNum / 100 + NUMPOSITION;
			AsciiNum[3] = 0;
		}
		else {
			AsciiNum[2] = 0;
		}
	}
	else {
		AsciiNum[1] = 0;
	}
	LcdPrintS(isRow, isCol, AsciiNum);
}

void LcdPrintS(uchar isRow,uchar isCol,const uchar *isStr)
{
	gotoxy(isCol + 1, isRow + 1);
	cprintf(isStr);
}

void LcdShowChar(uchar row, uchar col, uchar n, const uchar graph[][16])
{
	uchar start[10] = "Mt\x00\x00$", end [10] = "C\x01Lt\x00\x00$";
	uchar i, j, k, tmp;
	
	for (k = 0; k < n; k++) {
		start[3] = row * 16; end[5] = row * 16;
		for (j = 0; j < 16; j++) {
			start[2] = (col + k) * 8;
			end[4] = (col + k) * 8;
			for (i = 0; i < 8; i++) {
				if (graph[k][j] & (0x80 >> i)) {
					end[1] = 0x01;
				}
				else {
					end[1] = 0x00;
				}
				Draw(start);
				Draw(end);
				start[2]++;
				end[4]++;
			}
			start[3]++;
			end[5]++;
		}
	}
	RefreshLCD();
}

void ShowReverse(uchar StartLine, uchar StartCol, uchar EndLine, uchar EndCol) // MIN of line and column is 1.
{
	uchar luX, luY, rlX, rlY;
	
	luY = (StartLine - 1) * 16;
	luX = (StartCol - 1) * 8;
	rlY = EndLine * 16 - 1;
	rlX = EndCol * 8 - 1;
	ReverseArea(luX, luY, rlX, rlY);
}
 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-22
 */
 
 
/************************************************************************************ 
**		file:		dbf.h
**		purpose:	header file defining structures and error codes for dbase access
**					routines.
**		notes:	this file must be included in any programs which use the dbase
**					access routines included in dbf.lib.  do not use -a option with
**					this header file.
************************************************************************************/
#ifndef _DBF_H_
#define _DBF_H_
#include <stdio.h>//FILE

#define		DB2FILE			2
#define		DB3FILE			3
#define		DB3WITHMEMO		0x83
#define		FIELD_REC_LEN	32		/*	length of field description record */
#define		HEADER_PROLOG	32		/*	length of header without field desc and terminator */
#define		MAX_HEADER		4129	/*	maximum length of dBase III header	*/
#define		MAX_RECORD		4000	/*	dBase III record limit */
#define		MAX_FIELDS		128	/*	dBase III field limit */
#define		MAX_MEMO_BYTES	512	/*	dBase III memo field record size */
#define   BUFFERLEN     255

#define 	MAXPATH			80

#define true 1
#define false 0

/* error codes */
#define ERROR -1
#define   NO_RECORD     -1  /* have not found the specified record */
#define		OUT_OF_MEM		101		/* insufficient memory error */
#define		NO_FILE			102		/*	file not found error */
#define		BAD_FORMAT		103		/*	file not dBASE III file */
#define		RECNO_TOO_BIG	104	/*	requested record too big */

typedef unsigned char UCHAR;

struct FIELD_RECORD					/* This structure is filled in memory */
{												      /* with a fread.	do not change. */
		char name[11];					/* name of field in asciz */ //0-10
		char typ;						 	  /* type of field...char,numeric etc. */ //11
		char *field_data_address; 		/* offset of field in record */   //12-15
		//#if defined(__TINY__) || defined(__SMALL__) || defined (__MEDIUM__)
		int space_holder;					/* field_data_address must be 32 bits */ //仅为了占位,与前面的指针数组一起占32位
		//#endif
		UCHAR	len;						 	/* length of field */  //16
		UCHAR	dec;						 	/* decimals in field */  //17
		UCHAR	reserved_bytes[14];		/* reserved by dbase */  //18-31
};

struct DBF
{
		char filename[MAXPATH];					/* dos filename */
		FILE *file_ptr;						/* c file pointer */
		unsigned long int current_record;/* current record in memory */
		enum								 		/* status of file */
		{
			not_open=0,
			not_updated,
			updated
		} status;
		UCHAR	num_fields;				 		/* number of fields */

		/* the following 7 variables are filled with a fread, do not change order or size */
		UCHAR	dbf_version;					/* version character */   //00
		UCHAR	update_yr;						/* date of last update - year(-1900) */  //01
		UCHAR	update_mo;						/* date of last update - month */   //02
		UCHAR	update_day;				 		/* date of last update - day	*/    //03
		unsigned long int records;			/* number of records in dbf */  //04-07
		unsigned int	header_length;		/* length of header structure */ //08-09
		unsigned int	record_length;		/* length of a record */     //10-11
		/*															 */
		struct FIELD_RECORD *fields_ptr;	/* pointer to field array */
		char *record_ptr;					 	/* pointer to current record struct */
};

extern int d_open(struct DBF *d);
//打开数据库,并将数据库信息填入结构体中

int d_create(struct DBF *d, char *filename, struct FIELD_RECORD *field, int count);
//创建名为filename的数据库,并将数据库信息填入结构体中

//extern int d_cpystr(struct DBF *s,struct DBF *d);
//拷贝数据库结构

//extern int d_blank(struct DBF *d);
//将当前记录置为空白

extern int d_addrec(struct DBF *d);
//将当前记录添加到数据库中

extern char d_getfld(struct DBF *d,int f,char *buff);
//取序号为f的数据库字段

extern char d_getfldByName(struct DBF *d,char *name,char *buff);
//Get field by name取名称为name的数据库字段

extern int d_getrec(struct DBF *d,unsigned long int r);
//取记录号为r的数据库记录

extern int d_putfld(struct DBF *d,int f,char *buff);
//用buff中的内容修改字段号为f的字段

extern int d_putrec(struct DBF *d,unsigned long int r);
//用当前内存记录中的数据修改记录号为r的记录

int d_delrec(struct DBF *d, unsigned long int r);

extern int d_seek(struct DBF *d,int rcrds,int origin);
//查找记录号为rcrds的记录

extern void d_error(int errornum, char *filename);
//给出错误诊断,错误号为errornum

extern int d_close(struct DBF *d);
//关闭当前数据库

#endif /* _DBF_H_ */

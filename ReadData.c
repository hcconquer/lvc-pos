 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */
 
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <mem.h>
#include <string.h>
#include "typedef.h"
#include "strdef.h"
#include "menu.h"
#include "func.h"
#include "global.h"
#include "lvc_pos.h"
#include "common.h"
#include "rs232.h"
#include "lcdout.h"
#include "dbf.h"

static void PrintMtStatWord(uchar *msg)
{
	printf("继电器状态:");
	if(*msg & 0x40)
		printf("断\n");
	else
		printf("合\n");
	printf("有功电能方向:");
	if(*msg & 0x10)
		printf("反\n");
	else
		printf("正\n");
	printf("电池电压:");
	if(*msg & 0x04)
		printf("欠压\n");
	else
		printf("正常\n");
	printf("抄表方式:");
	if(*msg & 0x01)
		printf("手动\n");
	else
		printf("自动\n");
}

static void PrintMtStatWordBS8(uchar *data)
{
	printf(" 电表运行状态字\n");
	if ( ((*data & 0x80) != 0) || ((*data & 0x08) != 0) ){
		printf("\n无效数据！\n");
		printf("\n\n\n\n   按确定继续...");
		return;
	}
	printf("无功电能方向:");
	if (*data & 0x20)
		printf("反\n");
	else
		printf("正\n");
	printf("有功电能方向:");
	if (*data & 0x10)
		printf("反\n");
	else
		printf("正\n");
	printf("电池电压:");
	if (*data & 0x04)
		printf("正常\n");
	else
		printf("欠压\n");
	printf("最大需量积算方式\n");
	if (*data & 0x02)
		printf(" 滑差\n");
	else
		printf(" 区间\n");
	printf("抄表:");
	if (*data & 0x01)
		printf("手动\n");
	else
		printf("自动\n");
}

static void PrintNetwordStatWordBS8(uchar *data)
{
	printf("   电网状态字\n");
	if ( ((*data & 0x80) != 0) || ((*data & 0x08) != 0) ){
		printf("\n无效数据！\n");
		printf("\n\n\n\n   按确定继续...");
		return;
	}
	printf("C相过压:");
	if (*data & 0x40)
		printf("过压\n");
	else
		printf("正常\n");
	printf("B相过压:");
	if (*data & 0x20)
		printf("过压\n");
	else
		printf("正常\n");
	printf("A相过压:");
	if (*data & 0x10)
		printf("过压\n");
	else
		printf("正常\n");
	printf("C相断电:");
	if (*data & 0x04)
		printf("断电\n");
	else
		printf("正常\n");
	printf("B相断电:");
	if (*data & 0x02)
		printf("断电\n");
	else
		printf("正常\n");
	printf("A相断电:");
	if (*data & 0x01)
		printf("断电\n");
	else
		printf("正常\n");
}

static int FillQueryMtStatWordData(int fn, uchar *data, int idx)
{
	uchar *ptr = data;
	uchar FnChar[4], MtIdx[2];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = 0x00;
	*ptr++ = 0x01;
	*ptr++ = 0x00;
	ScanDataTo2Hex((long)idx, MtIdx, LowToHigh);
	memcpy(ptr, MtIdx, 2);
	ptr += 2;
	
	return ptr - data;
}

static int FillRdMPCurrData(int pn, int fn, uchar *data)
{
	uchar *ptr = data;
	uchar DaDt[4];
	
	GetPnFn(pn, fn, DaDt);
	memcpy(ptr, DaDt, 4);
	ptr += 4;
	
	return ptr - data;
}

static int FillQueryMtCurrData(int fn, uchar *data, uchar *idx)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = 0x00;
	*ptr++ = 0x01;
	*ptr++ = 0x00;
	memcpy(ptr, idx, 2);
	ptr += 2;
	
	return ptr - data;
}

static int FillFrozenData(int fn, int MtIdx, int Cnt, uchar day, uchar mon, uchar year, uchar *data)
{
	uchar *ptr = data;
	uchar FnChar[4];
	uchar idx[2];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = 0x01;
	ScanDataTo2Hex((long)MtIdx, idx, LowToHigh);
	*ptr++ = idx[0];
	*ptr++ = idx[1];
	*ptr++ = Cnt;
	if(fn == 1)
		*ptr++ = CharToBcd(day);
	*ptr++ = CharToBcd(mon);
	*ptr++ = CharToBcd(year);
	
	return ptr - data;
}

static int GetMtFroCntInput(int *cnt)
{
	char str[6] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("  读取冻结数据\n");
		printf("连续表数:");
		if(GetString(str, 4, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 1024) == 0);
	*cnt = val;
	
	return 1;
}

static int GetFroDateInput(uchar *day, uchar *mon, uchar *year, int ExitDay)
{
	char DayStr[4] = "\0", MonStr[4] = "\0", YearStr[4] = "\0";
	int DayVal, MonVal, YearVal;
	
	while(1){
		LcdStart();
		printf(" 读取日冻结数据\n");
		printf("起始日期输入:\n");
		printf("年(01-99):");
		if(GetString(YearStr, 2, TYPE_NUMBER) == 0) return 0;
		YearVal = atoi(YearStr);
		if(CheckRange((long)YearVal, 1, 99) == 0)
			continue;
		printf("月(1-12):");
		if(GetString(MonStr, 2, TYPE_NUMBER) == 0) return 0;
		MonVal = atoi(MonStr);
		if(CheckRange((long)MonVal, 1, 12) == 0)
			continue;
		if(ExitDay == 0)
			break;
		printf("日(1-31):");
		if(GetString(DayStr, 2, TYPE_NUMBER) == 0) return 0;
		DayVal = atoi(DayStr);
		if(CheckRange((long)DayVal, 1, 31) == 0)
			continue;
		else
			break;	
	}
	*day = DayVal;
	*mon = MonVal;
	*year = YearVal;
	
	return 1;
}

static int GetFroDayCnt(int *cnt)
{
	char str[4] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("  读取冻结数据\n");
		printf("连续天数:");
		if(GetString(str, 2, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 31) == 0);
	*cnt = val;
	
	return 1;
}

static int GetFroMonCnt(int *cnt)
{
	char str[4] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("  读取冻结数据\n");
		printf("连续月数:");
		if(GetString(str, 2, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 12) == 0);
	*cnt = val;
	
	return 1;
}

static int GetDayFrozenInput(int *MtIdx, int *MtCnt, uchar *day, uchar *mon, uchar *year, int *DayCnt)
{
	if(GetMtIdxInput(MtIdx, " 读取日冻结数据") == 0) return 0;
	if(GetMtFroCntInput(MtCnt) == 0) return 0;
	if(GetFroDateInput(day, mon, year, 1) == 0) return 0;
	if(GetFroDayCnt(DayCnt) == 0) return 0;
	
	return 1;
}

static int GetMonFrozenInput(int *MtIdx, int *MtCnt, int *MonCnt, uchar *mon, uchar *year)
{	
	uchar day;
	
	if(GetMtIdxInput(MtIdx, " 读取月冻结数据") == 0) return 0;
	if(GetMtFroCntInput(MtCnt) == 0) return 0;
	if(GetFroDateInput(&day, mon, year, 0) == 0) return 0;
	if(GetFroMonCnt(MonCnt) == 0) return 0;
			
	return 1;
}

static int GetMPIdxInput(int *idx, char *notice)
{
	char str[4] = "\0";
	int val;
	
	do{
		LcdStart();
		if(notice != NULL)
			printf("%s\n", notice);
		printf("测量点号:");
		if(GetString(str, 2, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 64) == 0);
	*idx = val;
	
	return 1;
}

static int FillMtDayFrozenEngyRecord(uchar *Rec, uchar RecStartFlag, uchar *addr, 
		uchar *MtIdx, uchar *date, uchar *engy, uchar *fv1, uchar *fv2, uchar *fv3, uchar *fv4)
{
	uchar *ptr = Rec;
	
	*ptr++ = RecStartFlag;
	memcpy(ptr, addr, 8);
	ptr += 8;
	memcpy(ptr, MtIdx, 4);
	ptr += 4;
	memcpy(ptr, date, 8);
	ptr += 8;
	memcpy(ptr, engy, 9);
	ptr += 9;
	memcpy(ptr, fv1, 9);
	ptr += 9;
	memcpy(ptr, fv2, 9);
	ptr += 9;
	memcpy(ptr, fv3, 9);
	ptr += 9;
	memcpy(ptr, fv4, 9);
	ptr += 9;
	
	return ptr - Rec;
}

static int FillMtMonFrozenEngyRecord(uchar *Rec, uchar RecStartFlag, uchar *addr,
		 	uchar *MtIdx, uchar *date, uchar *engy)
{
	uchar *ptr = Rec;
	
	*ptr++ = RecStartFlag;
	memcpy(ptr, addr, 8);
	ptr += 8;
	memcpy(ptr, MtIdx, 4);
	ptr += 4;
	memcpy(ptr, date, 8);
	ptr += 8;
	memcpy(ptr, engy, 9);
	ptr += 9;
		
	return ptr - Rec;
}

static void LVCAddrToRecord(uchar *DstAddr, uchar *SrcAddr)
{
	int i;
	
	for(i=0; i<4;i++){
		DstAddr[2*i] = ValueToAsc((SrcAddr[i] >> 4) & 0x0f);
		DstAddr[2*i + 1] = ValueToAsc(SrcAddr[i] & 0x0f);
	}
}

static void EngyToRec(uchar *EngyRec, uchar *engy)
{
	int i;
	uchar *ptr = EngyRec + 8;
	
	if(memcmp(engy, "\xee\xee\xee", 3) == 0)
		memcpy(EngyRec, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
	else{
		for(i=0;i<4;i++)
		{
			if(i == 1){
				*ptr = '.';
				ptr--;
			}
			*ptr = ValueToAsc(engy[i] & 0x0f);
			ptr--;
			*ptr = ValueToAsc((engy[i] >> 4) & 0x0f);
			ptr--;
		}
	}
}

static void FvToRec(uchar *fv1, uchar *fv2, uchar *fv3, uchar *fv4, int FeiLvCnt, uchar *SrcFv)
{
	uchar *ptr = SrcFv;
	
	switch(FeiLvCnt){
		case 0:
			memcpy(fv1, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			memcpy(fv2, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			memcpy(fv3, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			memcpy(fv4, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			break;
		case 1:
			EngyToRec(fv1, ptr);
			memcpy(fv2, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			memcpy(fv3, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			memcpy(fv4, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			break;
		case 2:
			EngyToRec(fv1, ptr);
			ptr += 7;
			EngyToRec(fv2, ptr);
			memcpy(fv3, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			memcpy(fv4, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			break;
		case 3:
			EngyToRec(fv1, ptr);
			ptr += 7;
			EngyToRec(fv2, ptr);
			ptr += 7;
			EngyToRec(fv3, ptr);
			memcpy(fv4, "\x20\x20\x20\x20\x20\x20\x20\x20\x20", 9);
			break;
		case 4:
			EngyToRec(fv1, ptr);
			ptr += 7;
			EngyToRec(fv2, ptr);
			ptr += 7;
			EngyToRec(fv3, ptr);
			ptr += 7;
			EngyToRec(fv4, ptr);
			break;
		default:
			break;
	}
}

static int SaveMtDayFrozenEnergy(struct DBF *db, uchar *msg, uchar *addr)
{
	long MtCnt=0, MtIdx=0;
	int MtCntLoop, MtDayCntLoop, MtDayCnt=0, FeiLvCnt=0;
	uchar *ptr = msg;
	uchar LVCAddr[8], MtIdxArray[4], TotalEnergy[9], date[8], Fv1[9], Fv2[9], Fv3[9], Fv4[9];
	uchar record[DayFroRecLen];
	int ret;
	
	LVCAddrToRecord(LVCAddr, addr);
	MtCnt = Scan2HexToData(ptr, LowToHigh);
	ptr += 2;
	for(MtCntLoop = 0;MtCntLoop < MtCnt;MtCntLoop++)
	{
		MtIdx = Scan2HexToData(ptr, LowToHigh);
		ptr += 2;
		LongToRec(MtIdx, MtIdxArray);
		MtDayCnt = *ptr;
		ptr++;
		FeiLvCnt = *ptr;
		ptr++;
		if(FeiLvCnt > 4)
			return FEILVCNTOVER;
		for(MtDayCntLoop = 0;MtDayCntLoop < MtDayCnt;MtDayCntLoop++)
		{
			//printf("\nMtCnt=%d,MtDayCnt=%d\nmtno=%d,dayno=%d\n", MtCnt, MtDayCnt, MtCntLoop, MtDayCntLoop);
			EngyToRec(TotalEnergy, ptr);
			ptr += 4;
			DateToRec(date, ptr, 'Y');
			ptr +=3;
			FvToRec(Fv1, Fv2, Fv3, Fv4, FeiLvCnt, ptr);
			ptr += FeiLvCnt * 7;
			FillMtDayFrozenEngyRecord(record, 0x20, LVCAddr, MtIdxArray, date, TotalEnergy, Fv1, Fv2, Fv3, Fv4);		
			//db->record_ptr = record;
			memcpy(db->record_ptr, record, db->record_length);
			d_addrec(db);
		}
		MtDayCnt = 0;
	}
	return SAVESUC;
}

static int SaveMtMonFrozenEnergy(struct DBF *db, uchar *msg, uchar *addr)
{
	long MtCnt=0, MtIdx=0;
	int MtMonCnt;
	uchar *ptr = msg;
	uchar LVCAddr[8], MtIdxArray[4], TotalEnergy[9], date[8];
	uchar record[MonFroRecLen];
	int ret;
	
	LVCAddrToRecord(LVCAddr, addr);
	MtCnt = Scan2HexToData(ptr, LowToHigh);
	ptr += 2;
	if(MtCnt != 1)
		return PACKERRSTAT;
	ptr += 2;  //cellecting year and month
	MtIdx = Scan2HexToData(ptr, LowToHigh);
	ptr += 2;		
	LongToRec(MtIdx, MtIdxArray);
	MtMonCnt = *ptr;
	ptr++;
	if(MtMonCnt != 1)
		return PACKERRSTAT;
	EngyToRec(TotalEnergy, ptr);
	ptr += 4;
	DateToRec(date, ptr, 'N');
	FillMtMonFrozenEngyRecord(record, 0x20, LVCAddr, MtIdxArray, date, TotalEnergy);		
	//db->record_ptr = record;
	memcpy(db->record_ptr, record, db->record_length);
	d_addrec(db);
	
	return SAVESUC;
}

static void PrintMPCurrEngyMuster(uchar *data)
{
	uchar *ptr = data;
	int FvCnt = 0;
	
	LcdStart();
	printf("  当前电量集合\n");	
	printf("终端抄表时间:\n");
	PrintDataFormat15(data);
	ptr += 5;
	FvCnt = *ptr++;
	printf("费率数:%d",FvCnt);
	///////////////////need to add....
	
}

static void PrintMPCurrPlusEngy(uchar *data)
{
	int FvCnt = 0, i;
	uchar *ptr = data;
	
	LcdStart();
	FvCnt = *ptr++;
	printf("当日正向有功总电\n能:");
	PrintDataFormat13(ptr);
	printf("\n");
	ptr += 4;
	for (i=1;i<=FvCnt;i++){
		if (i < 3){
			printf("当日正无功电能费\n率%d:", i);
			PrintDataFormat13(ptr);
			printf("\n");
			ptr += 4;
		}
		else if (i == 3){
			printf("当日正无功电能费\n率%d:", i);
			PrintDataFormat13(ptr);
			ptr += 4;
			WaitForEnterKey();
			LcdStart();
		}
		else{			
			printf("当日正无功电能费\n率%d:", i);
			PrintDataFormat13(ptr);			
			ptr += 4;
			if ((i - 4 + 1) % 4 == 0){
				WaitForEnterKey();
				LcdStart();
			}
			else
				printf("\n");
		}
	}	
}

static void PrintMPClockStatWord(uchar *data)
{
	uchar *ptr = data;
	LcdStart();
	printf("  时钟及状态字\n");
	printf("终端抄表时间:\n");
	PrintDataFormat15(ptr);
	ptr += 5;
	printf("\n");
	printf("电能表日历时钟:\n");
	PrintDataFormat1(ptr);
	printf("   按确定继续...");
	WaitForEnterKey();
	ptr += 6;
	
	LcdStart();
	PrintMtStatWordBS8(ptr);
	printf("   按确定继续...");
	WaitForEnterKey();
	ptr += 1;
	
	LcdStart();
	PrintNetwordStatWordBS8(ptr);
	printf("   按确定继续...");
	ptr += 1;	
	WaitForEnterKey();
	
	LcdStart();
	printf("最近一次编程时间\n");
	PrintDataFormat17(ptr);
	printf("\n");
	ptr += 4;
	printf("最近最大需量清零\n时间:");
	PrintDataFormat17(ptr);
	printf("\n");
	ptr += 4;
	printf("编程次数:");
	PrintDataFormat08(ptr);
	ptr += 2;
	printf("\n");
	printf("最大需量清零次数\n");
	PrintDataFormat08(ptr);
	printf("\n");
	printf("   按确定继续...");
	WaitForEnterKey();
	ptr += 2;
	LcdStart();
	printf("电池工作时间:\n");
	PrintDataFormat10(ptr);
}

static int CheckRecvPacketAFN0cFn41(ST_MSG *msg, uchar *FnPn)
{
	if (msg->afn != 0x0c) return 0;
	if (memcmp(msg->data, FnPn, FNPN_LEN) != 0) return 0;
	if (msg->size == FNPN_LEN) return 1;
	if ( (4 + 4*(*(msg->data + 4))) != (msg->size - FNPN_LEN - 1) ) return 0;   //1 for Fv cnt byte
}

static int CheckRecvPacketAFN0cFn27(ST_MSG *msg, uchar *FnPn)
{
	if (msg->afn != 0x0c) return 0;
	if (memcmp(msg->data, FnPn, FNPN_LEN) != 0) return 0;
	if (msg->size == FNPN_LEN) return 1;
	if ((msg->size - 4) != 28) return 0;
}

static void MtDayFrozenEnergy(void)
{	
	ST_MSG MsgOut, MsgIn;
	uchar data[15], day, mon, year, StartDay, StartMon, StartYear, PacketOut[255], PacketIn[255];
	int MtCnt, MtIdx, DayCnt, PacketOutLen, PacketInLen, DataLen;
	int i,j, ret, SaveRet;
	struct DBF db;
	
	if(GetDayFrozenInput(&MtIdx, &MtCnt, &day, &mon, &year, &DayCnt) == 0) return;
	StartDay = day;
	StartMon = mon;
	StartYear = year;
	strcpy(db.filename, MtDayFroDBFName);
	if(d_open(&db) != 0){
		LcdStart();
		printf("\n\n打开记录文件失败\n请重试!");
		WaitForESCKey();
		return;
	}
	for(i=0;i<MtCnt;i++)
	{
		for(j=1;j<=DayCnt;j++)
		{
			DataLen = FillFrozenData(1, MtIdx + i, 1, day, mon, year, data);
			//TestBuf(data, DataLen);
			FillMsgOut(&MsgOut, 0x8d, data, DataLen, 0x01, 0, 0, 0);
			PacketOutLen = pack(PacketOut, 255, &MsgOut);
			PrintRdRecProcess(MtIdx + i, j, COMMINGSTAT, RDFROZEN);
			OpenCom(G_COMPort, GBaud);
			Send(PacketOut, PacketOutLen);
			PacketInLen = Receive(PacketIn, 0x16, 0x16, 0, RECVTIMEOUT_SHORT);
			CloseCom(G_COMPort);
			if(PacketInLen > 0){
				unpack(PacketIn, PacketInLen, &MsgIn);
				if((MsgIn.afn != 0x8d)  || (GetIntFn(MsgIn.data) != 1)){
					ret = PrintRdRecProcess(MtIdx + i, j, COMMFAILSTAT, RDFROZEN);
					if(ret == 0){
						d_close(&db);return;
					}
					else if(ret == 1)
						continue;
					else{
						j--;continue;
					}
				}
				else{ 
					if(MsgIn.size == 6){
						if(memcmp(MsgIn.data + 4, "\x00\x00", 2) == 0)
							ret = PrintRdRecProcess(MtIdx + i, j, DATAEMPSTAT, RDFROZEN);
						else
							ret =PrintRdRecProcess(MtIdx + i, j, PACKERRSTAT, RDFROZEN);
						if(ret == 0){
							d_close(&db);return;
						}
						else if(ret == 1)
							continue;
						else{
							j--;continue;
						}
					}
					else{
						SaveRet = SaveMtDayFrozenEnergy(&db, MsgIn.data + 4, MsgIn.addr);
						if(SaveRet == SAVESUC)
							PrintRdRecProcess(MtIdx + i, j, SAVESUCSTAT, RDFROZEN);
						else{
							ret = PrintRdRecProcess(MtIdx + i, j, SaveRet, RDFROZEN);
							if(ret == 0){
								d_close(&db);return;
							}
							else if(ret == 1)
								continue;
							else{
								j--;continue;
							}
						}
					}
				}
			}
			else{
				ret = PrintRdRecProcess(MtIdx + i, j, COMMFAILSTAT, RDFROZEN);
				if(ret == 0){
					d_close(&db);return;
				}
				else if(ret == 1)
					continue;
				else{
					j--;continue;
				}
			}
			NextDay(&day, &mon, &year);
		}
		day = StartDay;
		mon = StartMon;
		year = StartYear;
	}	
	d_close(&db);
}

static void MtMonFrozenEnergy(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], mon, year, StartMon, StartYear, PacketOut[255], PacketIn[255];
	int MtCnt, MtIdx, MonCnt, PacketOutLen, PacketInLen, DataLen;
	int i,j, ret, SaveRet;
	struct DBF db;
	
	if(GetMonFrozenInput(&MtIdx, &MtCnt, &MonCnt, &mon, &year) == 0) return;
	StartMon = mon;
	StartYear = year;
	strcpy(db.filename, MtMonFroDBFName);
	if(d_open(&db) != 0){
		LcdStart();
		printf("\n\n打开记录文件失败\n请重试!");
		WaitForESCKey();
		return;
	}
	for(i=0;i<MtCnt;i++)
	{
		for(j=1;j<=MonCnt;j++)
		{
			DataLen = FillFrozenData(3, MtIdx + i, 1, 0, mon, year, data);
			FillMsgOut(&MsgOut, 0x8d, data, DataLen, 0x01, 0, 0, 0);
			PacketOutLen = pack(PacketOut, 255, &MsgOut);
			//TestBuf(PacketOut, PacketOutLen);
			PrintRdRecProcess(MtIdx + i, j, COMMINGSTAT, RDFROZEN);
			OpenCom(G_COMPort, GBaud);
			Send(PacketOut, PacketOutLen);
			PacketInLen = Receive(PacketIn, 0x16, 0x16, 0, RECVTIMEOUT_SHORT);
			CloseCom(G_COMPort);
			if(PacketInLen > 0){
				unpack(PacketIn, PacketInLen, &MsgIn);
				if((MsgIn.afn != 0x8d)  || (GetIntFn(MsgIn.data) != 3)){
					ret = PrintRdRecProcess(MtIdx + i, j, COMMFAILSTAT, RDFROZEN);
					if(ret == 0){
						d_close(&db);return;
					}
					else if(ret == 1)
						continue;
					else{
						j--;continue;
					}
				}
				else{ 
					if(MsgIn.size == 6){
						if(memcmp(MsgIn.data + 4, "\x00\x00", 2) == 0)
							ret = PrintRdRecProcess(MtIdx + i, j, DATAEMPSTAT, RDFROZEN);
						else
							ret =PrintRdRecProcess(MtIdx + i, j, PACKERRSTAT, RDFROZEN);
						if(ret == 0){
							d_close(&db);return;
						}
						else if(ret == 1)
							continue;
						else{
							j--;continue;
						}
					}
					else if(MsgIn.size == (4+13)){
						SaveMtMonFrozenEnergy(&db, MsgIn.data + 4, MsgIn.addr);
						PrintRdRecProcess(MtIdx + i, j, SAVESUCSTAT, RDFROZEN);
					}
					else if(MsgIn.size == 11){
						ret = PrintRdRecProcess(MtIdx + i, j, DATAEMPSTAT, RDFROZEN);
						if(ret == 0){
							d_close(&db);return;
						}
						else if(ret == 1)
							continue;
						else{
							j--;continue;
						}
					}
					else{
						ret = PrintRdRecProcess(MtIdx + i, j, PACKERRSTAT, RDFROZEN);
						if(ret == 0){
							d_close(&db);return;
						}
						else if(ret == 1)
							continue;
						else{
							j--;continue;
						}
					}
				}
			}
			else{
				ret = PrintRdRecProcess(MtIdx + i, j, COMMFAILSTAT, RDFROZEN);
				if(ret == 0){
					d_close(&db);return;
				}
				else if(ret == 1)
					continue;
				else{
					j--;continue;
				}
			}
			NextMonth(&mon, &year);
		}
		mon = StartMon;
		year = StartYear;
	}	
	d_close(&db);
}

void MPCurrEngyMuster(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], idx[2], addr[4], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	int MPIdx;
			
	if(GetMPIdxInput(&MPIdx, "  当前电量集合") == 0) return;
	DataLen = FillRdMPCurrData(MPIdx, 33, data);
	FillMsgOut(&MsgOut, 0x0c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if(MsgIn.afn != 0x0c){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		else if (MsgIn.size == FNPN_LEN){
			PrintCommStat(EMPTY);
			WaitForESCKey();return;
		}
		else{
			PrintMPCurrEngyMuster(MsgIn.data + FNPN_LEN);
			WaitForESCKey();return;
		}
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

void MPCurrPlusEngy(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], idx[2], addr[4], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	int MPIdx;
	
	if(GetMPIdxInput(&MPIdx, " 当日正有功电量") == 0) return;
	DataLen = FillRdMPCurrData(MPIdx, 41, data);
	FillMsgOut(&MsgOut, 0x0c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_LONG);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		//if (CheckRecvPacketAFN0cFn41(&MsgIn, data) == 0) {
		if (MsgIn.afn != 0x0c) {
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		if (MsgIn.size == FNPN_LEN){
			PrintCommStat(EMPTY);
			WaitForESCKey();
			return;	
		}			
		PrintMPCurrPlusEngy(MsgIn.data + FNPN_LEN);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

void MPClockStatWord(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], idx[2], addr[4], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	int MPIdx;
	
	if(GetMPIdxInput(&MPIdx, "  时钟及状态字") == 0) return;
	DataLen = FillRdMPCurrData(MPIdx, 27, data);
	FillMsgOut(&MsgOut, 0x0c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_LONG);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		/*if (CheckRecvPacketAFN0cFn27(&MsgIn, data) == 0) {
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}*/
		if (MsgIn.size == FNPN_LEN){
			PrintCommStat(EMPTY);
			WaitForESCKey();
			return;	
		}			
		PrintMPClockStatWord(MsgIn.data + FNPN_LEN);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

void MtCurrEngy(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], idx[2], addr[4], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	int MtIdx;
	
	if(GetMtIdxInput(&MtIdx, "  当前有功电量") == 0) return;
	ScanDataTo2Hex((long)MtIdx, idx, LowToHigh);
	DataLen = FillQueryMtCurrData(57, data, idx);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_LONG);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 57)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		if((*(MsgIn.data+4) != 1) || ((int)Scan2HexToData(MsgIn.data+5, LowToHigh) != MtIdx)){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();return;
		}
		LcdStart();
		printf("  当前有功电量\n");	
		printf("表序号:%d\n", MtIdx);
		printf("当前正向有功总电\n能:");
		PrintDataFormat11(MsgIn.data + 7);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

void MtClock(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], idx[2], addr[4], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	int MtIdx;
	
	if(GetMtIdxInput(&MtIdx, "  电表日期时间") == 0) return;
	ScanDataTo2Hex((long)MtIdx, idx, LowToHigh);
	DataLen = FillQueryMtCurrData(67, data, idx);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_LONG);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 67)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		if((*(MsgIn.data+4) != 1) || ((int)Scan2HexToData(MsgIn.data+5, LowToHigh) != MtIdx)){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();return;
		}
		LcdStart();
		printf("    电表时钟\n");	
		PrintDataFormat1(MsgIn.data + 7);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

void MtStatWord(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], addr[4], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	int MtIdx;
	
	if(GetMtIdxInput(&MtIdx, "  电表日期时间") == 0) return;
	DataLen = FillQueryMtStatWordData(74, data, MtIdx);
	FillMsgOut(&MsgOut, 0x8c, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_LONG);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x8c) || (GetIntFn(MsgIn.data) != 74)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		if((*(MsgIn.data+4) != 1) || ((int)Scan2HexToData(MsgIn.data+5, LowToHigh) != MtIdx)){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();return;
		}
		LcdStart();
		printf("   电表状态字\n");	
		printf("表序号:%d\n", MtIdx);
		PrintMtStatWord(MsgIn.data + 7);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

void MeasPointData(void)
{
	/*void (*p[])(void) = {MPCurrEngyMuster, MPCurrPlusEngy, MPClockStatWord};
	const uchar *buf[3];
    
    buf[0] = MPCurrEngyMusterStr;
    buf[1] = MPCurrPlusEngyStr;
	buf[2] = MPClockStatWordStr;
	
	mymenu(buf, p, 3, 0);*/
	
	void (*p[])(void) = {MPCurrPlusEngy, MPClockStatWord};
	const uchar *buf[2];
    
    buf[0] = MPCurrPlusEngyStr;
    buf[1] = MPClockStatWordStr;
	
	InitMenu(buf, p, 2, 0);
}

void CurrMtData(void)
{
	void (*p[])(void) = {MtCurrEngy, MtClock, MtStatWord};
	const uchar *buf[3];
    
    buf[0] = MtCurrEngyStr;
    buf[1] = MtClockStr;
	buf[2] = MtStatWordStr;
	
	InitMenu(buf, p, 3, 0);
}

void ReadData(void)
{
	void (*p[])(void) = {MtDayFrozenEnergy, MtMonFrozenEnergy, MeasPointData, CurrMtData};
	const uchar *buf[4];
    
    buf[0] = MtDayFrozenEnergyStr;
    buf[1] = MtMonFrozenEnergyStr;
	buf[2] = MeasPointDataStr;
	buf[3] = CurrMtDataStr;
	
	InitMenu(buf, p, 4, 0);
}
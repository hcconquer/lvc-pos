 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <stdio.h>
#include <conio.h>
#include <mem.h>
#include <string.h>
#include "typedef.h"
#include "func.h"
#include "lvc_pos.h"
#include "lcdout.h"
#include "dbf.h"
#include "menu.h"
#include "strdef.h"
#include "common.h"

extern struct FIELD_RECORD MtDayFroRecField[DAYFIELDCNT];
extern struct FIELD_RECORD MtMonFroRecField[MONFIELDCNT];
extern struct FIELD_RECORD MtArchivesField[MTARCHFIELDCNT];
//extern struct FIELD_RECORD MPArchField[MPARCHFIELDCNT];

static void ViewDayFroData(void)
{
	struct DBF db;
	int i, quit=0, IsFind = 0;
	uchar dir;
	uchar buf[70];
	
	strcpy(db.filename, MtDayFroDBFName); 
	if(d_open(&db) != 0){
		LcdStart();
		printf("\n\n打开记录文件失败\n请重试!");
		WaitForESCKey();
		return;
	}
	
	for(i=1;i<=db.records;i++)
	{
		d_getrec(&db, (long)i);
		memcpy(buf, db.record_ptr, db.record_length);
		if(memcmp(buf, "\x20", 1) != 0)
			continue;
		IsFind = 1;	
		LcdStart();
		printf("集中器通信地址:\n");
		PrintBufChar(buf+1, 8, ORDER, NOSPAC);	
		printf("\n表序号:");
		PrintBufChar(buf+9, 4, ORDER, NOSPAC);
		printf("\n日期:");
		PrintBufChar(buf+13, 8, ORDER, NOSPAC);
		printf("\n正向有功总电能:\n");
		PrintBufChar(buf+21, 9, ORDER, NOSPAC);
		if(i == 1)
			dir = GetDirection(0, 1, 0, 1);
		else if(i == db. records)
			dir = GetDirection(1, 0, 0, 1);
		else
			dir = GetDirection(1, 1, 0, 1);
		switch(dir){
			case 'K':i = i - 2;continue;
			case 'M':continue;
			case 'P':break;
			case 27:quit=1;break;
			default:continue;
		}
		if(quit == 1)
			break;
		LcdStart();
		printf("费率1正向有功总\n电能:");
		PrintBufChar(buf+30, 9, ORDER, NOSPAC);
		printf("\n费率1正向有功总\n电能:");
		PrintBufChar(buf+39, 9, ORDER, NOSPAC);
		printf("\n费率1正向有功总\n电能:");
		PrintBufChar(buf+48, 9, ORDER, NOSPAC);
		printf("\n费率1正向有功总\n电能:");
		PrintBufChar(buf+57, 9, ORDER, NOSPAC);
		if(i == 1)
			dir = GetDirection(0, 1, 1, 0);
		else if(i == db. records)
			dir = GetDirection(1, 0, 1, 0);
		else
			dir = GetDirection(1, 1, 1, 0);
		switch(dir){
			case 'K':i = i - 2;continue;
			case 'M':continue;
			case 'H':i--;continue;
			case 27:quit=1;break;
			default:continue;
		}
		if(quit == 1)
			break;
	}
	if(IsFind == 0){
		LcdStart();
		printf("\n没有可查看的数据\n内容.");
		WaitForESCKey();
	}
	d_close(&db);
}

static void ClearDayFroData(void)
{
	LcdStart();
	printf(" 清除日冻结数据\n");
	printf("\n 确定要清除吗?\n");
	printf("\n\n\n\n否            是");
	if (GetYesOrNo() == 0)
		return;
	if(ClearDBF(MtDayFroDBFName, MtDayFroRecField, DAYFIELDCNT) == 0){
		LcdStart();
		printf("\n清除数据成功!\n");
		WaitForESCKey();
	}
}

static void ViewMonFroData(void)
{
	struct DBF db;
	int i, quit=0, IsFind = 0;
	uchar dir;
	uchar buf[70];
	
	strcpy(db.filename, MtMonFroDBFName); 
	if(d_open(&db) != 0){
		LcdStart();
		printf("\n\n打开记录文件失败\n请重试!");
		WaitForESCKey();
		return;
	}
	
	for(i=1;i<=db.records;i++)
	{
		d_getrec(&db, (long)i);
		memcpy(buf, db.record_ptr, db.record_length);
		if(memcmp(buf, "\x20", 1) != 0)
			continue;
		IsFind = 1;	
		LcdStart();
		printf("集中器通信地址:\n");
		PrintBufChar(buf+1, 8, ORDER, NOSPAC);	
		printf("\n表序号:");
		PrintBufChar(buf+9, 4, ORDER, NOSPAC);
		printf("\n年月:");
		PrintBufChar(buf+13, 6, ORDER, NOSPAC);
		printf("\n正向有功总电能:\n");
		PrintBufChar(buf+21, 9, ORDER, NOSPAC);
		if(i == 1)
			dir = GetDirection(0, 1, 0, 1);
		else if(i == db. records)
			dir = GetDirection(1, 0, 1, 0);
		else
			dir = GetDirection(1, 1, 1, 1);
		switch(dir){
			case 'K':i = i - 2;continue;
			case 'M':continue;
			case 'P':continue;
			case 'H':i = i - 2;continue;
			case 27:quit=1;break;
			default:continue;
		}
		if(quit == 1)
			break;
	}
	if(IsFind == 0){
		LcdStart();
		printf("\n没有可查看的数据\n内容.");
		WaitForESCKey();
	}
	d_close(&db);
}

static void ClearMonFroData(void)
{
	LcdStart();
	printf(" 清除月冻结数据\n");
	printf("\n 确定要清除吗?\n");
	printf("\n\n\n\n否            是");
	if (GetYesOrNo() == 0)
		return;
	if(ClearDBF(MtMonFroDBFName, MtMonFroRecField, MONFIELDCNT) == 0){
		LcdStart();
		printf("\n清除数据成功!\n");
		WaitForESCKey();
	}
}

static void ClearMtArch(void)
{
	LcdStart();
	printf("  清除电表档案\n");
	printf("\n 确定要清除吗?\n");
	printf("\n\n\n\n否            是");
	if (GetYesOrNo() == 0)
		return;
	if(ClearDBF(MtArchivesDBFName, MtArchivesField, MTARCHFIELDCNT) == 0){
		LcdStart();
		printf("\n清除数据成功!\n");
		WaitForESCKey();
	}
}

/*static void ClearMPArch(void)
{
	if(ClearDBF(MeasPoArchDBFName, MPArchField, MPARCHFIELDCNT) == 0){
		LcdStart();
		printf("\n清除数据成功!\n");
		getch();
	}
}*/

void ManageData(void)
{
	void (*p[])(void) = {ViewDayFroData, ClearDayFroData, ViewMonFroData, ClearMonFroData, ClearMtArch};
	const uchar *buf[5];

	buf[0] = ViewDayFroDataStr;
	buf[1] = ClearDayFroDataStr;
	buf[2] = ViewMonFroDataStr;
	buf[3] = ClearMonFroDataStr;
	buf[4] = ClearMtArchStr;
	
	InitMenu(buf, p, 5, 0);
}
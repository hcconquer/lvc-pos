 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */
#include "typedef.h"
#include "dbf.h"
 
void LastInputInit(void);
void GetLastInput(uchar *buf, long last, uchar *ascii, uchar n);
boolean useridcmp(uchar *userid);
boolean passwdcmp(uchar *passwd);
uchar check_sum(const uchar *buf, int len);
int pack(uchar *buf, int max_len, const ST_MSG *msg);
int unpack(const uchar *buf, int len, ST_MSG *msg);
uchar GetSEQ(void);
void GetFn(int fn, uchar *FnChar);
int GetIntFn(uchar *FnChar);
void GetPnFn(int pn, int fn, uchar *DaDt);
void FillMsgOut(ST_MSG *msg, uchar afn, uchar *data, int MsgLen, uchar CtrlCode, int SetQueryAddrFlag, int ExistPw, int ExistTp);
void PrintCommStat(int flag);
int PrintRdRecProcess(int MtIdx, int Cnt, int stat, int flag);
void PrintEnergy(uchar *engy);
int EnterNo(char *buf,int len);
void PrintMtIdx(uchar *idx);
void PrintMtNum(uchar *num, int len);
void PrintMeasPro(uchar *pro);
void PrintConnWay(uchar *way);
void PrintFvNum(uchar *fv);
void PrintMtType(uchar *type);
void PrintLineNum(uchar *line);
void PrintBoxNum(uchar *box);
void PrintCollNum(uchar *coll, int len);
int CheckAndDelRec(struct DBF *d, int idx);
int GetMtIdxInput(int *idx, char *notice);
int GetMtNumInput(char *num, char *notice);
void PrintDataFormat11(uchar *msg);
void PrintDataFormat1(uchar *msg);
void PrintDataFormat15(uchar *data);
void PrintDataFormat13(uchar *data);
void PrintDataFormat17(uchar *data);
void PrintDataFormat08(uchar *ptr);
void PrintDataFormat10(uchar *data);
void PrintWeek(uchar ch);
void TimeToClock(uchar *clock);
int ClearDBF(uchar *filename, struct FIELD_RECORD *record, int count);
void GetLocalClock(uchar *clock);
int CheckRange(long val, long min, long max);
int CheckStrLen(uchar *str, int min, int max);
int CheckChoiceInput(char *cmd, char min, char max);
uchar GetDirection(int LfFlg, int RhFlg, int UpFlg, int DnFlg);
int GetYesOrNo(void);
void SetCOMBaud(void);
void WaitForESCKey(void);
void WaitForEnterKey(void);
int SaveINI(char *id, char *psw, char *addr, int com, long baud);
int IsFindEnter(char *str);
int MsgComm(uchar *BufOut, int BufOutLen, uchar *BufIn, int timeout);
int CopyLVCAddr(uchar *DstAddr, uchar *SrcAddr, int len);
int CheckPacketIn(uchar *PacketIn, int PacketInLen);
int IsAction(uchar *notice);
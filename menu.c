 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <string.h>
#include <stdarg.h>
#include "menu.h"
#include "lcdout.h"
#include "input.h"
#include "common.h"
#include "strdef.h"
#include "lvc_pos.h"

void InitMenu(const uchar *item[], void (*p[])(void), uchar FuncNum, uchar flag)
{
	uchar i, key, KeyValue, CurLine = 1, confirm = 0;
	
	while (1) {
		LcdStart();
		for (i = 0; i < FuncNum; i++) {
			LcdPrintS(i, 0, item[i]);
		}
		ShowReverse(CurLine, 1, CurLine, strlen(item[CurLine - 1]));
		do {
			key = KeyScan();
			switch (key) {
				case '\r':
					confirm = 1;
					break;
				case 'K':
				case 'H':
					if (CurLine > 1) {
						ShowReverse(CurLine, 1, CurLine, strlen(item[CurLine - 1]));
						CurLine--;
						ShowReverse(CurLine, 1, CurLine, strlen(item[CurLine - 1]));
					}
					break;
				case 'P':
				case 'M':
					if (CurLine < FuncNum) {
						ShowReverse(CurLine, 1, CurLine, strlen(item[CurLine - 1]));
						CurLine++;
						ShowReverse(CurLine, 1, CurLine, strlen(item[CurLine - 1]));
					}
					break;
				case 27:
					return;
				default:
					KeyValue = AscToValue(key);
					break;
			}
			if (confirm == 1) {
				KeyValue = CurLine;
				break;
			}
		} while (KeyValue < 1 || KeyValue > FuncNum);
		p[KeyValue - 1]();
		confirm = 0; KeyValue = 0;
		if (flag == 1)
			return;
	}
}
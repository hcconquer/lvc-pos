#ifndef _RS232_H
#define _RS232_H

void ShowProgress(unsigned char x, unsigned char y);
int OpenCom(unsigned ComNo, unsigned long BaudVal);
int CloseCom(unsigned ComNo);
int Receive(unsigned char *inbuf, unsigned char end, unsigned char end2,int act, int timeout);
int Send(unsigned char *outbuf, unsigned length);
boolean commget(unsigned char *chr, unsigned char sec);
void CommPutC(unsigned char c);
unsigned char CommGetC(void);
//int RecvPacket(uchar *inbuf);

#endif // end ifndef _RS232_H
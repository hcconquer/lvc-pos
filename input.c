 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <dos.h>
#include <conio.h>
#include <mem.h>
#include "htxlcd.h"
#include "input.h"
#include "lcdout.h"
#include "common.h"

void KeyCheckTimeout(uint TenMsec)
{
	struct time lasttime, curtime;
	uchar lastsecond;
	uint cnt = 0;
	
	gettime(&lasttime);
	lastsecond = lasttime.ti_sec;
	do {
		if (kbhit()) {
			getch();
			return;
		}
		gettime(&curtime);
		if (curtime.ti_sec < lastsecond) {
			cnt += 60;
		}
		lastsecond = curtime.ti_sec;
	} while ((((curtime.ti_sec + cnt) * 100 + curtime.ti_hund) - (lasttime.ti_sec * 100 + lasttime.ti_hund)) < TenMsec);
}

uchar KeyCheck(void)
{
	return kbhit();
}

void KeyWait(void)
{
	while (kbhit() == 0);
	getch();
	return;
}

void KeyWaitFree(void)
{
	while (kbhit()) {
		getch();
	}
	return;
}
uchar KeyScan(void)
{
	uchar key;
	while (1) {
		key = getch();
		if (key != 0)
			return key;
	}
}

static void ClearInput(uchar Row, uchar Col, uchar len)
{
	uchar i;
	
	for (i = 0; i < len; i++) {
		LcdPrintS(Row, Col + i, " ");
	}
}

/*
 *	
 */
uchar LcdInput(uchar *buf0, uchar rol_no, uchar col_no, uchar length, uchar flag, boolean isAutoSkip)
{
	uchar key, i = 0, count = 0, temp, *buf = buf0;
	uchar inlen, first = 0;

	
	ShowCursor();
	memcpy(buf, DEFAULT_LVCADDR, 4);
	temp = ((length > 16 - col_no) ? 16 - col_no : length);
	if (flag == 0 && buf0[0] != 0xff) {
		for (i = 0; i < length; i += 2) {
			LcdTypeMem(rol_no, col_no + i,*(buf0 + i / 2));
			buf ++;
			count += 2;
		}
		gotoxy(col_no + 1 + length, rol_no + 1);
	}
	else {
		gotoxy(col_no + 1, rol_no + 1);
	}
	do
	{
		key = KeyScan();
		switch(key)
		{
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '.':
				if (first == 0) {
					ClearInput(rol_no, col_no, length);
					buf = buf0;
					for (i = 0; i < length / 2 + length % 2; i++) {
						buf[i] = 0;
					}
					count = 0;
				}
				if(count == temp)
					break;
				if (flag != 0) {
					LcdPrintC(rol_no, col_no+count, '*');
				}
				else {
					if (key == '.')
						LcdPrintC(rol_no, col_no+count, '.');
					else
						LcdPrintC(rol_no, col_no+count, key);
				}
				key = AscToValue(key);
				if((count%2)==0) {
					*buf = key;
					*buf <<= 4;
				}
				else {
					*buf |= key;
					buf++;
				}
				count++;
				break;
			case 'H':
				return 'H';
			case 'P':
				return 'P';
			case 'K':
			case 'M':			
			case '\b':
				if(count != 0) {
					count--;
					LcdPrintS(rol_no, col_no + count, " ");	// clear current character
					LcdPrintS(rol_no, col_no + count, "");	// move cursor back
					if((count % 2) == 1) 
					{
						buf--;
						*buf &= 0xf0;
					}
					else {
						*buf &= 0x0f;
					}
				}
				break;
			case 27:
				*buf0=EXIT_VALUE;
				HideCursor();
				return count;
			default: break;
		}
		first = 1;
	}while(key!='\r' && (count < length || isAutoSkip == 0));
	inlen=count;
	buf=buf0;
	if(count==0) {
		HideCursor();
		return 0;
	}
	if((count%2)==1)
	{
		for(i=(count-1)/2;i>0;i--)
		{
			buf[i]>>=4;			// Arrange odd array
			temp=buf[i-1]<<4;
			buf[i]|=temp;
		}
		buf[0]>>=4; 
	}
	buf=buf0;
	temp=(length/2)+length%2;
	if(count%2==1) count++;
	if(count/2<temp)
	{
		for(i=count/2;i>0;i--) { // Move data to high byte
			buf[i - 1 + temp - count / 2] = buf[i-1];
		}
		for(i=0;i<temp-count/2;i++)		// Clear low byte
		{
			buf[i]=0;
		}
	}
	HideCursor();
	return inlen;
}
 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#ifndef _COMMON_H
#define _COMMON_H

#include <stdio.h>
#include "typedef.h"

//输入数据类型,用于GetString函数中,用于替换scanf
//#define TYPE_NUMBER        0//整数
//#define TYPE_FLOAT         1//浮点数
//#define TYPE_HEX           2//十六进制数
//#define TYPE_ALPHANUMERIC  3//字母或数字

//////////////以下部分为函数声明
extern void flush(FILE *stream);
//写文件后,在不用关闭文件的情况下,将缓冲区数据刷新到文件

extern unsigned char Choice(void);
// 菜单选择处理

boolean chkpass(void);
void mydelay(int msecond);
boolean yes_no(uchar rol, uchar col);
//uint coder(uchar l);
uchar decode(uint ll);
//uint crcset(uchar *str,uchar lng);
uchar AscToValue(uchar ascii);
uchar ValueToAsc(uchar value);
void BcdToAscii(uchar *ascii, uchar *bcd, uchar n);
uchar CharToBcd(uchar value);
uchar BcdToChar(uchar value);
ulong ChangeUnion(ulong InData);
ulong BcdToLong(uchar *source,uchar str_len);
void LongToBcd(ulong source,uchar *target);
void strcopy(uchar *str_source, uchar *str_target, uchar str_len, boolean case_flag);
void RtcReadClock(uchar *time_date);
int GetChoice(char *notice, int min, int max);
void TestBuf(uchar *buf, int len);
void PrintBuf(uchar *buf, int len, int order, int space);
void PrintBufChar(uchar *buf, int len, int order, int space);
void ScanIP(uchar *ip, uint len, uchar *StoreIp);
void ScanDataTo2Hex(long data, uchar *StoreData, int IsLowToHigh);
long Scan2HexToData(uchar *Src, int IsLowToHigh);
void ScanInputStr(uchar *DstStr, uchar *SrcStr, int len);
void LongToRec(long cnt, uchar *rec);
void DateToRec(uchar *DateRec, uchar *date, uchar ExistDay);
int LenToInt(uchar *buf);
void flush(FILE *stream);
int is_leap_year(int year);
void NextDay(uchar *year, uchar *month, uchar *day);
void NextMonth(uchar *month, uchar *year);
unsigned GetString(char *str, uchar len, int type);
int StrToHex(uchar *hex, uchar *str, int len);
int HexToStr(uchar *str, uchar *hex, int len);
int LongToStr(char *str, long val);


#endif	//end ifndef _COMMON_H
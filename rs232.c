/*
 *  Function for low level communication.
 */

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <dos.h>
#include "typedef.h"
#include "commplus.h"
#include "infrared.h"
#include "common.h"
#include "strdef.h"
#include "rs232.h"

extern int SENDDELAY;
int ShowWait=1;


/* flash for receive data */
void ShowProgress(unsigned char x, unsigned char y)
{
  static int Progress;
  char ProgressArray[3][2] =
  {
    "/", "|", "\\"
  };
  gotoxy(x, y);
  cprintf(ProgressArray[Progress]);
  Progress++ ;
  if (Progress > 2)
  	Progress = 0;
}

int OpenCom(unsigned ComNo, unsigned long BaudVal)
{
	if (ComNo==COM2)
	{
		IRPowerCtrl(1);
		SetIRMFreq(38400);
		SetIROutput(2);
		SetCommMode(mod_8N1, ComNo-1, BaudVal);	// W7|S1|EVEN
	}
	else
	{
		SetCommMode(mod_8E1, ComNo-1, BaudVal);	
	}
	EnableRx(0);
	return 1;
}

int CloseCom(unsigned ComNo)
{
	DisableRx();
	if (ComNo==COM2)
	{
		SetIRMFreq(0);
		IRPowerCtrl(0);
	}
	return 0;
}

/*
 * HHU receive data to buf. If it is error, buf is empty, else reture length of received data.
 * If the time is over RECEIVETIMEOUT, return. If act = 0, when received end or end2 and length is right,
 * return at onece, else waiting to receive a character again then return.
 */

int Receive(unsigned char *inbuf, unsigned char end, unsigned char end2,int act, int timeout)
{
	unsigned char c,state;
	int x,y,i,len,total1=0,total2=0,tt1,tt2;
	struct time t1, t2;
	unsigned char DataLen = 200 - 1, StartReceive = 0;
	union intval temp;

	//memset(inbuf,NULL,BUFFERLEN);// clear 0
	
	/*if (ShowWait)
	cprintf("\r\n");
	cprintf(Waiting);
	{
		x = wherex();
		y = wherey();
	}*/
	
	gettime(&t1);
	tt1=t1.ti_hund;
	total1 = 0;
	len=0;
	for(;;)
	{
		if (ExistAuxBGot(&c,&state) != 0)
		{
			if (c == 0x68 && StartReceive == 0) {
				StartReceive = 1;
			}
			if (StartReceive == 0)
				continue;
			inbuf[len++] = c;
			if (len == 3) {
				/*temp.d[0]=inbuf[1];
				temp.d[1]=inbuf[2];
				DataLen = decode(temp.val);*/
				DataLen = LenToInt(inbuf + 1);
			}
			if (((c == end) || (c == end2)) && (len >= (DataLen + 8)))
			{
				if (act == 0) break;
				
				gettime(&t2);
				tt2=t2.ti_hund;
				total2 = 0;						
				for (;;)
				{
					if (ExistAuxBGot(&c,&state) != 0)
					{
						inbuf[len++] = c;
						break;
					}
					gettime(&t2);
					if(t2.ti_hund<tt2)
						total2 = 100 + t2.ti_hund - tt2 + total2;
					else
						total2 = t2.ti_hund - tt2 + total2;
					tt2 = t2.ti_hund;
					if ((total2*10) >= timeout) break;									
				}
				break;
			}
			/*if (ShowWait)
				ShowProgress(x, y);*/
		}
		gettime(&t1);
		if(t1.ti_hund<tt1)
			total1 = 100 + t1.ti_hund - tt1 + total1;
		else
			total1 = t1.ti_hund - tt1 + total1;

		tt1 = t1.ti_hund;
		if ((total1*10) >= timeout)
			break;
	}
	
	/*if (ShowWait)
	{
		gotoxy(1, y);
		clreol();
	}*/
	return len;
}

/*
 * Send data of outbuf
 */
int Send(unsigned char *outbuf, unsigned length)
{
	int i;
	
	#ifdef debug
		cprintf("S:");
		for(i=0;i<length;i++)
			cprintf("%02x",((unsigned char *)outbuf)[i]);
		if (i!=0)
			cprintf("\r\n");
	#endif
	
	for(i=0;i<length;i++)
	{
		PutAuxByte(outbuf[i]);
//		mydelay(SENDDELAY);
	}
	return True;
}

boolean commget(unsigned char *chr, unsigned char sec)
{
	struct time lasttime, curtime;
	unsigned char c, state;
	
	gettime(&lasttime);
	while (1) {
		if (ExistAuxBGot(chr,&state) != 0)
			return 1;
		gettime(&curtime);
		if (curtime.ti_sec >= lasttime.ti_sec) {
			if ((curtime.ti_sec - lasttime.ti_sec) > sec)
				return 0;
		}
		else {
			if ((60 + curtime.ti_sec - lasttime.ti_sec) > sec)
				return 0;
		}
		if (kbhit() && (getch() == 27))
			return 0;
	}
}

void CommPutC(unsigned char c)
{
	Send(&c, 1);
}

unsigned char CommGetC(void)
{
	unsigned char c, state;
	
	while (!ExistAuxBGot(&c, &state));
	return c;
}
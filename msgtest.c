/*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <stdio.h>
#include <mem.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include "typedef.h"
#include "strdef.h"
#include "menu.h"
#include "lcdout.h"
#include "lvc_pos.h"
#include "common.h"
#include "global.h"
#include "rs232.h"
#include "input.h"
#include "md5.h"
#include "util.h"

int PackSize = 512;

static int FillUpdateLVCData(BYTE *data, int fn, BYTE fid, BYTE fatt,
		BYTE EndFlag, WORD count, DWORD off, WORD len, BYTE *buf)
{
	BYTE *ptr = data;
	BYTE FnChar[4];
	MD5_CTX md5;
	BYTE hash[16];
	int i;

	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;

	*ptr++ = fid;
	*ptr++ = fatt;
	*ptr++ = EndFlag;
	stoc(ptr, count);
	ptr += 2;
	ltoc(ptr, off);
	ptr += 4;
	stoc(ptr, len);
	ptr += 2;

	if (buf != NULL)
	{
		memcpy(ptr, buf, len);
		ptr += len;
	}

	MD5Iter(&hash, data + 4, len + 11, 128);
	memcpy(ptr, hash, 8);
	ptr += 8;

	return ptr - data;
}

int UpdataLVCFilePack(BYTE *buf, WORD len, BYTE fid, BYTE fatt, WORD count,
		DWORD off)
{
	ST_MSG MsgOut;
	BYTE *PacketOut;
	BYTE seq;
	BYTE *data;
	int PacketOutLen, DataLen;
	BYTE EndFlag;

	BYTE *ptr;

	data
			= (BYTE*) malloc((4 + 2 + 1 + 2 + 4 + 2 + PackSize + 8)
					* sizeof(BYTE));/*DA+DT,File Info,End Flag,count,off,len,buf,MD5*/
	PacketOut = (BYTE*) malloc((6 + 1 + 5 + 1 + 1 + 4 + 2 + 1 + 2 + 4 + 2
			+ PackSize + 8 + 2 + 1 + 1) * sizeof(BYTE));/*Head,Ctrl Code,Address,AFN,SEQ,DA+DT,File Info,End Flag,count,off,len,buf,MD5,PW,CS,End*/
	EndFlag = buf == NULL ? 1 : 0;
	DataLen = FillUpdateLVCData(data, 1, fid, fatt, EndFlag, count, off, len,
			buf);
	seq = off & 0x0F;/*截取最低4bit作为PSEQ*/
	seq = 0x10;/*本报文必须确认*/
	if (off == 0)/*首桢*/
	{
		seq |= 0x40;
	}
	if (EndFlag == 1)
	{
		seq |= 0x20;
	}
	FillMsgOut(&MsgOut, 0x0f, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 1024, &MsgOut);
	printf("\nThe %d packet:\n", off + 1);
	printf("Size = %d\n", PacketOutLen - 4);
	PRINTB(PacketOut + 4, PacketOutLen - 4);
	printf("\n");
	free(data);
	free(PacketOut);
	return 1;
}

void UpdateLVC(void)
{
	FILE *fp;
	BYTE *buf;
	BYTE fid, fatt;
	WORD count, len;
	DWORD off;

	if ((fp = fopen("cscon.dwl", "rb")) == NULL)
	{
		printf("File not found\nFailue");
		return;
	}
	buf = (BYTE*) malloc(PackSize * sizeof(BYTE));
	fid = 0x00;
	fatt = 0x00;
	off = 0x00;
	len = PackSize;
	count = fsize(fp) / PackSize + 1;
	printf("Total %d pacaket:\n", count);
	while ((len = fread(buf, sizeof(BYTE), PackSize, fp)) != 0)
	{
		UpdataLVCFilePack(buf, len, fid, fatt, count - 1, off++);
		printf("len = %d and pos = %ld\n", len, ftell(fp));
	}
	UpdataLVCFilePack(NULL, 0, fid, fatt, count - 1, off++);
	free(buf);
	fclose(fp);
	printf("success\n");
}

void MaintainLVC(void)
{
}

void main(void)
{
	UpdateLVC();
	/*WaitForESCKey();*/
}

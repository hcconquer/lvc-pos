 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-19
 */
#include <stdio.h>
#include <mem.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include "typedef.h"
#include "strdef.h"
#include "func.h"
#include "lvc_pos.h"
#include "menu.h"
#include "lcdout.h"
#include "common.h"
#include "global.h"
#include "rs232.h"

static int FillQueryMtData(int fn, uchar *data, uchar ReadMode, int cnt, uchar *MtIdStr)
{
	uchar *ptr = data;
	uchar FnChar[4];
	uchar CntStr[2];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = ReadMode;
	ScanDataTo2Hex((long)cnt, CntStr, LowToHigh);
	memcpy(ptr, CntStr, 2);
	ptr += 2;
	memcpy(ptr, MtIdStr, 2);
	ptr += 2;
	return ptr - data;	
}

static void TransformToMtNum(uchar *Dst, uchar *Src)
{
	int i;
	uchar *ptr = Dst + 5;
	
	for(i=0;i<6;i++){
		*ptr = ((AscToValue(Src[2 * i]) << 4) & 0xf0) + AscToValue(Src[2*i+1]);
		ptr--;
	}
}

static int GetMeasureProInput(uchar *pro)
{
	char str[2] = "2";
	int cmd1, cmd2;
	
	do{
		LcdStart();
		printf("   测量点属性\n");
		printf("1.普通485表\n2.载波通信表\n3.简易多功能表\n4.多功能总表\n5.中继器\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd1 = atoi(str);
	}while(CheckRange(cmd1, 1, 5) == 0);
	
	strcpy(str, "3");
	do{
		LcdStart();
		printf("   测量点属性\n");
		printf("1.预付费电量表\n2.预付费表\n3.非预付费表\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd2 = atoi(str);
	}while(CheckRange(cmd2, 1, 3) == 0);
	cmd1 -= 1;
	cmd2 -= 1;
	*pro = ((cmd1 << 4) & 0x70) + (cmd2 & 0x03);
	
	return 1;
}

static int GetConnWayInput(uchar *way)
{
	int cmd1, cmd2;
	char str[2] = "2";
	
	do{
		LcdStart();
		printf("    接线方式\n");
		printf("1.单相表\n2.三相表\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd1 = atoi(str);
	}while(CheckRange(cmd1, 1, 2) == 0);
	
	strcpy(str, "1");
	do{
		LcdStart();
		printf("    接线方式\n");
		printf("1.直接接线\n2.经互感器接线\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd2 = atoi(str);
	}while(CheckRange(cmd2, 1, 2) == 0);
	cmd1 -= 1;
	cmd2 -= 1;
	*way = ((cmd1 << 1) & 0x02) + cmd2;
	
	return 1;
}

static int GetFeiLvNumInput(uchar *num)
{
	int cmd1, cmd2;
	char str[2] = "0";
	
	do{
		LcdStart();
		printf("    费率方案\n");
		printf("0:单费率方案\n1-8:分别对应费率\n方案号1-8\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd1 = atoi(str);
	}while(CheckRange(cmd1, 0, 8) == 0);
	
	strcpy(str, "0");
	do{
		LcdStart();
		printf("    费率方案\n");
		printf("0:总电量\n1:总,1,2电量\n2:总,1,2,3电量\n3:总,1,2,3,4电量\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd2 = atoi(str);
	}while(CheckRange(cmd2, 0, 3) == 0);
	if(cmd2 != 0)
		cmd2 += 1;
	*num = ((cmd1 << 4) & 0xf0) + cmd2;
	
	return 1;
}

static int GetMtTypePhase(int *phase)
{
	char str[3] = "1";
	int val;
	
	do{
		LcdStart();
		printf("    电表类型\n");
		printf("0:不确定\n1: A相\n2: B相\n3: C相\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange(val, 0, 3) == 0);
	*phase = val;
	
	return 1;
}

static int GetMtTypeGate(int *gate)
{
	char str[3] = "0";
	int val;
	
	do{
		LcdStart();
		printf("    电表类型\n");
		printf("0:不带拉闸\n1:带拉闸\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 0, 1) == 0);
	*gate = val;
	
	return 1;
}

static int GetMtTypeCus(int *cus)
{
	char str[3] = "0";
	int val;
	
	do{
		LcdStart();
		printf("    电表类型\n");
		printf("0:普通用户\n1:重点用户\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange(val, 0, 1) == 0);
	*cus = val;
	
	return 1;
}

static int GetMtTypeChoiceRd(int *Choice)
{
	char str[3] = "1";
	int val;
	
	do{
		LcdStart();
		printf("    电表类型\n");
		printf("0:不选抄\n1:选抄\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange(val, 0, 1) == 0);
	*Choice = val;
	
	return 1;
}

static int GetMtTypeCutOff(int *cut)
{
	char str[3] = "1";
	int val;
	
	do{
		LcdStart();
		printf("    电表类型\n");
		printf("0:允许断电\n1:不允许断电\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange(val, 0, 1) == 0);
	*cut = val;
	
	return 1;
}

static int GetMtTypeInput(uchar *type)
{
	int phase, gate, cus, choice, cut;
	
	if(GetMtTypePhase(&phase) == 0) return 0;
	if(GetMtTypeGate(&gate) == 0) return 0;
	if(GetMtTypeCus(&cus) == 0) return 0;
	if(GetMtTypeChoiceRd(&choice) == 0) return 0;	
	if(GetMtTypeCutOff(&cut) == 0) return 0;
	*type = ((cut<<5)&0x20) + ((choice<<4)& 0x10) + ((cus << 3)&0x08) + ((gate << 2)&0x04)+ phase;
	
	return 1;
}

static int GetAddMtArchInput(uchar *MtIdx, uchar *MtNum, uchar *MeasurePro, uchar *ConnWay, uchar *FeiLvNum, uchar *MtType)
{
	int idx;
	char num[13];
	
	if(GetMtIdxInput(&idx, "  设置电表档案") == 0) return 0;
	ScanDataTo2Hex((long)idx, MtIdx, LowToHigh);
	if(GetMtNumInput(num, "  设置电表档案") == 0) return 0;
	TransformToMtNum(MtNum, num);	
	if(GetMeasureProInput(MeasurePro) == 0) return 0;
	if(GetConnWayInput(ConnWay) == 0) return 0;
	if(GetFeiLvNumInput(FeiLvNum) == 0) return 0;
	if(GetMtTypeInput(MtType) == 0) return 0;
	
	return 1;
}

static int FillAddMtArch(int fn, uchar *data, uchar *idx, uchar *num, uchar *pro, uchar *way, uchar *FVNum, uchar *type)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, "\x01\x00", 2);
	ptr += 2;
	memcpy(ptr, idx, 2);
	ptr += 2;
	memcpy(ptr, num, 6);
	ptr += 6;
	*ptr++ = *pro;
	*ptr++ = *way;
	*ptr++ = *FVNum;
	*ptr++ = *type;
	memcpy(ptr, "\x00\x00", 2);
	ptr += 2;
	memcpy(ptr, "\x01\x00", 2);
	ptr += 2;
	memcpy(ptr, "\xff\xff\xff\xff\xff\xff", 6);
	ptr += 6;
	
	return ptr - data;
}

static int FillRdMtArchData(int fn, uchar *data, int cnt, int idx)
{
	uchar *ptr = data;
	uchar FnChar[4], TmpCnt[2], TmpIdx[2];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	ScanDataTo2Hex((long)cnt, TmpCnt, LowToHigh);
	ScanDataTo2Hex((long)idx, TmpIdx, LowToHigh);
	memcpy(ptr, TmpCnt, 2);
	ptr += 2;
	memcpy(ptr, TmpIdx, 2);
	ptr += 2;
	
	return ptr - data;
}

static int GetRdMtArchCntInput(int *cnt)
{
	int val;
	char str[6] = "\0";
	
	do{
		LcdStart();
		printf("  读取电表档案\n");
		printf("连续表数:");
		if(GetString(str, 4, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 1024) == 0);
	*cnt = val;
	
	return 1;
}

static int SaveMtArchRec(struct DBF *db, uchar *msg)
{
	uchar record[MtArchRecLen], MtIdx[2], buf[25];
	uchar *ptr = record;
	int i, ret, IsFind = 0;
	
	*ptr++ = 0x20;
	memcpy(ptr, msg, MtArchRecLen - 1);
	memcpy(MtIdx, msg, 2);
	for(i=1;i<=db->records;i++)
	{
		d_getrec(db, (long)i);
		memcpy(buf, db->record_ptr, db->record_length);
		if(memcmp(buf, "\x20", 1) != 0)
			continue;
		if(memcmp(buf+1, MtIdx, 2) == 0){
			IsFind = 1;
			memcpy(db->record_ptr, record, db->record_length);
			ret = d_putrec(db, (long)i);
			if(ret != 0)
				return ret;
			break;
		}
	}
	if(IsFind == 0){
		memcpy(db->record_ptr, record, db->record_length);
		d_addrec(db);
	}
			
	return SAVESUCSTAT;
}

static int FillDelMtArch(int fn, uchar *data, uchar *idx)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = 0x01;
	*ptr++ = 0x00;
	memcpy(ptr, idx, 2);
	ptr += 2;
	memcpy(ptr, "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff", 20);
	ptr += 20;
	
	return ptr - data;
}

static int FillQueryMtCurrData(int fn, uchar *data, uchar *idx)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = 0x00;
	*ptr++ = 0x01;
	*ptr++ = 0x00;
	memcpy(ptr, idx, 2);
	ptr += 2;
	
	return ptr - data;
}

static int GetClientIdxInput(uchar *idx)
{
	char str[4] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("   测量点档案\n");
		printf("电能表/交流采样\n装置序号1-64:");
		if(GetString(str, 2, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 64) == 0);
	*idx = val;
	
	return 1;
}

static int GetMPNumInput(uchar *num)
{
	char str[4] = "\0";
	int val;
	
	do{
		LcdStart();
		printf(" 配置测量点档案\n");
		printf("所属测量点号输入\n(1-64):");
		if(GetString(str, 2, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 64) == 0);
	*num = val;
	
	return 1;
}

static int GetBaudPortInput(uchar *baud)
{
	char str[4] = "\0";
	int val, cmd;
	uchar port;
	
	do{
		LcdStart();
		printf(" 配置测量点档案\n");
		printf("电能表/交流采样\n装置接入终端的通\n信端口号1-31:");
		if(GetString(str, 2, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 64) == 0);
	port = val;
	
	strcpy(str, "6");
	do{
		LcdStart();
		printf("0:默认的波特率\n");
		printf("1:600  2.1200\n");
		printf("3:2400\n");
		printf("4:4800\n");
		printf("5:7200\n");
		printf("6:9600\n");
		printf("7:19200\n");
		printf("选择波特率:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd = atoi(str);
	}while(CheckRange((long)cmd, 0, 7) == 0);
	*baud = port & 0x0f;
	*baud |= ((cmd << 5) & 0xe0);
	
	return 1;
}

static void TransformToCommParam(uchar *Dst, uchar *Src, int IsLowToHigh)
{
	int i;
	uchar *ptr;
	
	switch(IsLowToHigh){
		case LowToHigh:
			ptr = Dst + 5;
			for(i=0;i<6;i++){
				*ptr = ((AscToValue(Src[2 * i]) << 4) & 0xf0) + AscToValue(Src[2*i+1]);
				ptr--;
			}
			break;
		case HighToLow:
			ptr = Dst;
			for(i=0;i<6;i++){
				*ptr = ((AscToValue(Src[2 * i]) << 4) & 0xf0) + AscToValue(Src[2*i+1]);
				ptr++;
			}
			break;
		default:
			break;
	}
}

static int GetConfigMPComProInput(uchar *pro)
{
	uchar str[3] = "1";
	int val;
	
	do{
		LcdStart();
		printf(" 配置测量点档案\n");
		printf("通信规约类型:\n");
		printf("0:无效\n");
		printf("1:DL/T645规约\n");
		printf("2:交流采样装置\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 0, 2) == 0);
	*pro = val;
	
	return 1;
}

static int GetConfigMPPwdInput(uchar *pwd)
{
	uchar TmpPwd[13] = "\0";
	
	if(EnterNo(TmpPwd, 12) == 0)
		return 0;
	else{
		TransformToCommParam(pwd, TmpPwd, HighToLow);
	}
	
	return 1;
}

static int GetMPCountDecimal(int *dec)
{
	char str[3] = "2";
	int val;
	
	do{
		LcdStart();
		printf(" 配置测量点档案\n");
		printf("电表有功电能示值\n的小数位数1-4:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 4) == 0);
	*dec = val;
	return 1;
}

static int GetMPCountInt(int *intval)
{
	char str[3] = "\0";
	int val;
	
	do{
		LcdStart();
		printf(" 配置测量点档案\n");
		printf("电表有功电能示值\n整数位数4-7:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 4, 7) == 0);
	*intval = val;
	return 1;
}

static int GetMPCountFv(int *fv)
{
	char str[3] = "\0";
	int val;
	
	do{
		LcdStart();
		printf(" 配置测量点档案\n");
		printf("测量点的电能费率\n个数1-14:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 1, 14) == 0);
	*fv = val;
	return 1;
}

static int GetConfigMPCountInput(uchar *count)
{
	int cmd1, cmd2, cmd3;
	int dec, intval, fv;
	uchar value;
	
	if(GetMPCountDecimal(&dec) == 0) return 0;
	value = (dec-1) & 0x03;
	if(GetMPCountInt(&intval) == 0) return 0;
	value |= ((intval-4) & 0x0c);
	if(GetMPCountFv(&fv) == 0) return 0;
	value |= ((fv << 4) & 0xf0);
	*count = value;
	
	return 1;
}

static int GetConfigMPAddrInput(uchar *addr)
{	
	uchar TmpAddr[13] = "\0";
		
	if(EnterNo(TmpAddr, 12) == 0) 
		return 0;
	else{
		TransformToCommParam(addr, TmpAddr, LowToHigh);
	}
	
	return 1;
}

static int FillRdMeasPointArchData(int fn, uchar *data)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	
	return ptr - data;
}

static int FillSetRdMtPeriodData(int fn, uchar *data, uchar min)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = min;
	
	return ptr - data;
}

/*static int SaveMeasPointArch(struct DBF *db, uchar *msg)
{
	int i, IsFind = 0, ret = -1;
	uchar flag=0, EqipIdx, buf;
	uchar record[MeasPointRecLen];
	
	EqipIdx = msg[0];
	record[0] = 0x20;
	memcpy(record+1, msg, MeasPointRecLen - 1);
	for(i=1;i<=db->records;i++){
		d_getrec(db, (long)i);
		flag = *(db->record_ptr);
		buf = *(db->record_ptr + 1);
		//printf("flg=%02x,buf=%02x\n", flag, buf);
		//getch();
		if(flag != 0x20)
			continue;
		if(buf == EqipIdx){
			IsFind = 1;
			if(memcmp(msg+1, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 16) == 0)
				ret = d_delrec(db, (long)i);
			else{
				memcpy(db->record_ptr, record, db->record_length);
				ret = d_putrec(db, (long)i);
			}
			if(ret != 0)
				return ret;
			break;
		}		
	}
	//printf("IsFind=%d\n", IsFind);
	//getch();
	if((IsFind == 0) && (memcmp(msg+1, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 16) != 0)){
		memcpy(db->record_ptr, record, db->record_length);
		d_addrec(db);
	}
			
	return 0;
}*/

static int FillConfigMPData(int fn, uchar *data, uchar idx, uchar num, uchar baud, 
			uchar pro, uchar *addr, uchar *pwd, uchar cnt)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = 0x01;
	*ptr++ = idx;
	*ptr++ = num;
	*ptr++ = baud;
	*ptr++ = pro;
	memcpy(ptr, addr, 6);
	ptr += 6;
	memcpy(ptr, pwd, 6);
	ptr += 6;
	*ptr++ = cnt;
	
	return ptr - data;
}

static int FillQueryRdMtPeriodData(int fn, uchar *data)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	
	return ptr - data;
}

static int CheckAndDelMPArchRec(struct DBF *db, uchar idx)
{
	int i, ret;
	uchar buf, flag;
	
	for(i=1;i<=db->records;i++){
		d_getrec(db, (long)i);
		flag = *(db->record_ptr);
		buf = *(db->record_ptr + 1);
		if(flag != 0x20)
			continue;
		if(buf == idx){
			printf("equal!\n");
			getch();
			ret = d_delrec(db, (long)i);
			if(ret != 0)
				return ret;
			break;
		}
	}
	return 0;
}

static void PrintMeasPointArch(uchar *rec, int RecCnt)
{
	int cmd, i, quit=0;
	const int MPArchLen = 17;
	uchar *ptr = rec, dir;
	char *baud[8]={"默认", "600", "1200", "2400", "4800", "7200", "9600", "19200"};
	char *pro[3]={"无效", "DL/T645规约", "交流采样装置"};
	
	for(i=1;i<=RecCnt;i++){
		ptr += (i-1) * MPArchLen;
		LcdStart();
		printf("   测量点档案\n");
		printf("电能表/交流采样\n装置序号:%d\n", ptr[0]);
		printf("所属测量点号:%d\n", ptr[1]);
		printf("电能表/交流采样\n装置接入终端的通\n信端口号:%d\n", (ptr[2] & 0x0f));
		cmd = ((ptr[2]) >> 5) & 0x07;
		if((cmd >=0) && (cmd <=7))
			printf("波特率:%s", baud[cmd]);
		else
			printf("波特率:错误数据");
		getch();LcdStart();
		printf("通信规约类型:\n");
		cmd = ptr[3];
		if((cmd >= 0) && (cmd <= 3))
			printf("  %s\n", pro[cmd]);
		else
			printf("  错误数据\n");
		printf("通信地址:\n");
		PrintBuf(ptr+4, 6, NOTORDER, NOSPAC);
		printf("\n");
		printf("通信密码:\n");
		PrintBuf(ptr+10, 6, ORDER, NOSPAC);
		printf("\n");
		getch();LcdStart();
		printf("电表有功电能示值\n的小数位数:%d\n", (ptr[16]&0x03)+1);
		printf("电表有功电能示值\n整数位数:%d\n", ((ptr[16] >> 2)&0x03) + 4);
		printf("测量点的电能费率\n个数:%d\n", (ptr[16] >> 4) & 0x0f);
		if(i == 1)
			dir = GetDirection(0, 1, 0, 1);
		else if(i == RecCnt)
			dir = GetDirection(1, 0, 1, 0);
		else
			dir = GetDirection(1, 1, 1, 1);
		switch(dir){
			case 'K':
			case 'H':i = i - 2;continue;
			case 'M':
			case 'P':continue;
			case 27:quit=1;break;
			default:continue;
		}
		if(quit == 1)
			break;
	}
}

static void ConfigMeasPointArch(void)
{
	uchar ClientIdx, MPNum, Baud, CommPro, CommAddr[6], CommPwd[6], count;
	ST_MSG MsgOut, MsgIn;
	uchar data[25], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	
	if(GetClientIdxInput(&ClientIdx) == 0) return;
	if(GetMPNumInput(&MPNum) == 0) return;
	if(GetBaudPortInput(&Baud) == 0) return;
	if(GetConfigMPComProInput(&CommPro) == 0) return;
	LcdStart();
	printf("   测量点档案\n");
	printf("通信地址:\n");
	if(GetConfigMPAddrInput(CommAddr) == 0) return;
	printf("通信密码:\n");
	if(GetConfigMPPwdInput(CommPwd) == 0) return;
	LcdStart();
	if(GetConfigMPCountInput(&count) == 0) return;
	
	DataLen = FillConfigMPData(10, data, ClientIdx, MPNum, Baud, CommPro, CommAddr, CommPwd, count);
	FillMsgOut(&MsgOut, 0x04, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
			PrintCommStat(COMMFAIL);
		else
			PrintCommStat(SETSUC);
		WaitForESCKey();
		return;
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void RdMeasPointArch(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[127], PacketIn[1255];
	int PacketOutLen, DataLen, PacketInLen;
	int ArchCnt=0;
	uchar *ptr = NULL;
	const int MPArchLen = 17;
	
	DataLen = FillRdMeasPointArchData(10, data);
	FillMsgOut(&MsgOut, 0x0a, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x0a) || (GetIntFn(MsgIn.data) != 10)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();return;
		}
		ArchCnt = *(MsgIn.data+4);
		if(ArchCnt == 0){
			PrintCommStat(EMPTY);
			WaitForESCKey();
			return;
		}
		else if(MsgIn.size != (MPArchLen*ArchCnt+4+1)){ //4 for fn, 1 for client count
			PrintCommStat(RECVPACKERR);
			WaitForESCKey();
			return;
		}
		else{
			ptr = MsgIn.data + 5;
			PrintMeasPointArch(ptr, ArchCnt);
		}
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

/*static void QueryMeasPointArch(void)
{
	uchar ClientIdx, record[MeasPointRecLen - 1];
	uchar buf, flag;
	struct DBF db;
	int IsFind = 0, i;
	
	LcdStart();
	printf("电能表交流采样装\n置序号1-64:");
	if(GetConfigMPIntInput(&ClientIdx, 1, 64) < 0) return;
	strcpy(db.filename, MeasPoArchDBFName);
	if(d_open(&db) != 0){
		LcdStart();
		printf("\n\n打开记录文件失败\n请重试!");
		getch();
		return;
	}
	for(i=1;i<=db.records;i++){
		d_getrec(&db, (long)i);
		flag = *(db.record_ptr);
		buf = *(db.record_ptr + 1);
		if(flag != 0x20)
			continue;
		if(buf == ClientIdx){
			IsFind = 1;
			memcpy(record, db.record_ptr + 1, db.record_length - 1);
			PrintMeasPointArch(record);
			break;
		}		
	}
	if(IsFind == 0){
		LcdStart();
		printf("\n本地数据库中不存\n在电能表/交流采\n样装置序号为%d\n的记录,请尝试先\n从集中器中读取.", ClientIdx);
		getch();
	}
	d_close(&db);
}*/

static void DelMeasPointArch(void)
{
	uchar ClientIdx, MPNum, Baud, CommPro, CommAddr[6], CommPwd[6], count;
	ST_MSG MsgOut, MsgIn;
	uchar data[25], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	int ret;
	
	if(GetClientIdxInput(&ClientIdx) == 0) return;
	if (IsAction("确定要删除吗?") == 0) return;
	MPNum = 0x00; Baud = 0x00; CommPro = 0x00; 
	memcpy(CommAddr, "\x00\x00\x00\x00\x00\x00", 6);
	memcpy(CommPwd, "\x00\x00\x00\x00\x00\x00", 6);
	count = 0x00;
	
	DataLen = FillConfigMPData(10, data, ClientIdx, MPNum, Baud, CommPro, CommAddr, CommPwd, count);
	FillMsgOut(&MsgOut, 0x04, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();return;
		}
		else{
			LcdStart();
			PrintCommStat(DELSUC);
			WaitForESCKey();
		}
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

/*static void QueryClientRdMtPeriod(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	
	DataLen = FillQueryRdMtPeriodData(24, data);
	FillMsgOut(&MsgOut, 0x0a, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	//TestBuf(PacketOut, PacketOutLen);
	LcdStart();
	LcdPrintS(1,0,Sending);
	OpenCom(G_COMPort, GBaud);
	Send(PacketOut, PacketOutLen);
	PacketInLen = Receive(PacketIn, 0x16, 0x16, 0);
	//PacketInLen = RecvPacket(PacketIn);
	CloseCom(G_COMPort);
	//printf("lenflg=%d\n", PacketInLen);
	//TestBuf(PacketIn, PacketInLen);
	if(PacketInLen > 0){
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x0a) || (MsgIn.size != 5) || (GetIntFn(MsgIn.data) != 24)){
			PrintCommStat(COMMFAIL);
			getch();
			return;
		}
		LcdStart();
		printf("\n终端抄表间隔时间\n为:%d(分钟)\n", *(MsgIn.data + 4));
		getch();
	}
	else{
		PrintCommStat(COMMFAIL);
		getch();
	}
}

static void SetClientRdMtPeriod(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[8], min, PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	
	LcdStart();
	printf("终端抄表间隔时间\n(分钟):");
	if(GetConfigMPIntInput(&min, 1, 60) < 0)
		return;
	DataLen = FillSetRdMtPeriodData(24, data, min);
	FillMsgOut(&MsgOut, 0x04, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	//TestBuf(PacketOut, PacketOutLen);
	LcdStart();
	LcdPrintS(1,0,Sending);
	OpenCom(G_COMPort, GBaud);
	Send(PacketOut, PacketOutLen);
	PacketInLen = Receive(PacketIn, 0x16, 0x16, 0);
	//PacketInLen = RecvPacket(PacketIn);
	CloseCom(G_COMPort);
	//printf("lenflg=%d\n", PacketInLen);
	//TestBuf(PacketIn, PacketInLen);
	if(PacketInLen > 0){
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (MsgIn.size != 4) || (GetIntFn(MsgIn.data) != 1)){
			PrintCommStat(COMMFAIL);
			getch();
			return;
		}
		LcdStart();
		PrintCommStat(SETSUC);
		getch();
	}
	else{
		PrintCommStat(COMMFAIL);
		getch();
	}
}*/

static void AddMtArch(void)
{
	uchar MtIdx[2], MtNum[6], MeasurePro, ConnWay, FeiLvNum, MtType;
	ST_MSG MsgOut, MsgIn;
	uchar data[30], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
		
	if(GetAddMtArchInput(MtIdx, MtNum, &MeasurePro, &ConnWay, &FeiLvNum, &MtType) == 0) return;		
	DataLen = FillAddMtArch(1, data, MtIdx, MtNum, &MeasurePro, &ConnWay, &FeiLvNum, &MtType);
	//TestBuf(data, DataLen);
	FillMsgOut(&MsgOut, 0x84, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		PrintCommStat(SETSUC);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void DelMtArch(void)
{
	uchar MtIdx[2];
	ST_MSG MsgOut, MsgIn;
	uchar data[30], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	struct DBF db;
	int idx, ret;
	
	if(GetMtIdxInput(&idx, "  删除电表档案") == 0) return;
	if (IsAction("确定要删除吗?") == 0) return;
	ScanDataTo2Hex((long)idx, MtIdx, LowToHigh);	
	DataLen = FillDelMtArch(1, data, MtIdx);
	FillMsgOut(&MsgOut, 0x84, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		PrintCommStat(SETSUC);
		strcpy(db.filename, MtArchivesDBFName);
		if(d_open(&db) != 0){
			LcdStart();
			printf("\n同步本地数据库时\n打开记录文件失败\n请在数据管理中手\n动删除表序号为\n%ld的记录!", idx);
			WaitForESCKey();
			return;
		}
		ret = CheckAndDelRec(&db, (int)idx);
		if(ret != 0){
			LcdStart();
			printf("\n同步本地数据库出\n错,请在数据管理\n中手动删除表序号\n为%ld的记录!", idx);
		}
		d_close(&db);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void RdMtArch(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[255], PacketIn[255];
	int MtCnt, MtIdx, PacketOutLen, PacketInLen, DataLen;
	int i,j, ret, SaveRet, DelRet, PackRet;
	struct DBF db;
	
	if(GetMtIdxInput(&MtIdx, "  读取电表档案") == 0) return;	
	if(GetRdMtArchCntInput(&MtCnt) == 0) return;
	strcpy(db.filename, MtArchivesDBFName);
	if(d_open(&db) != 0){
		LcdStart();
		printf("\n\n打开记录文件失败\n请重试!");
		WaitForESCKey();
		return;
	}
	for(i=0;i<MtCnt;i++)
	{
		DataLen = FillRdMtArchData(1, data, 1, MtIdx + i);
		FillMsgOut(&MsgOut, 0x8a, data, DataLen, 0x01, 0, 0, 0);
		PacketOutLen = pack(PacketOut, 127, &MsgOut);
		PrintRdRecProcess(MtIdx + i, 0, COMMINGSTAT, RDMTARCH);
		OpenCom(G_COMPort, GBaud);
		Send(PacketOut, PacketOutLen);
		PacketInLen = Receive(PacketIn, 0x16, 0x16, 0, RECVTIMEOUT_SHORT);
		CloseCom(G_COMPort);
		//TestBuf(PacketIn, PacketInLen);
		if(PacketInLen > 0){
			if (CheckPacketIn(PacketIn, PacketInLen) == 0){
				PackRet = PrintRdRecProcess(MtIdx + i, 0, PACKERRSTAT, RDMTARCH);
				if(PackRet == 0){
					d_close(&db);return;
				}
				else if(ret == 1)
					continue;
				else{
					i--;continue;
				}
			}
			unpack(PacketIn, PacketInLen, &MsgIn);
			if((MsgIn.afn != 0x8a) ||(GetIntFn(MsgIn.data) != 1) || (MsgIn.size == 6)){
				if((MsgIn.size == 6) && (Scan2HexToData(MsgIn.data+4, LowToHigh) == 0)){
					ret = PrintRdRecProcess(MtIdx + i, 0, DATAEMPSTAT, RDMTARCH);
					DelRet = CheckAndDelRec(&db, MtIdx + i);
					if(DelRet != 0){
						LcdStart();
						printf("\n同步本地数据库出\n错,请在数据管理\n中手动删除表序号\n为%d的记录!", MtIdx + i);
						WaitForESCKey();
					}
					if(ret == 0){
						d_close(&db);return;
					}
					else if(ret == 1)
						continue;
					else{
						i--;continue;
					}
				}
				else{
					ret = PrintRdRecProcess(MtIdx + i, 0, COMMFAILSTAT, RDMTARCH);
					if(ret == 0){
						d_close(&db);return;
					}
					else if(ret == 1)
						continue;
					else{
						i--;continue;
					}
				}
			}
			else{ 
				SaveRet = SaveMtArchRec(&db, MsgIn.data + 6);// 4 bytes for fn, 2 bytes for meter count
				if(SaveRet == SAVESUCSTAT)
					PrintRdRecProcess(MtIdx + i, 0, SAVESUCSTAT, RDMTARCH);
				else{
					ret = PrintRdRecProcess(MtIdx + i, 0, SaveRet, RDMTARCH);
					if(ret == 0){
						d_close(&db);return;
					}
					else if(ret == 1)
						continue;
					else{
						i--;continue;
					}
				}
			}
		}
		else{
			ret = PrintRdRecProcess(MtIdx + i, 0, COMMFAILSTAT, RDMTARCH);
			if(ret == 0){
				d_close(&db);return;
			}
			else if(ret == 1)
				continue;
			else{
				i--;continue;
			}
		}
	}	
	d_close(&db);
}

static void QueryMtArchByIdx(void)
{
	int idx, i, IsFind = 0;
	struct DBF db;
	uchar buf[25], MtIdx[2], MtNum[7], MeasurePro[2], ConnWay[2], FeiLvNum[2], MtType[2], LineNum[3], BoxNum[3], CollNum[7];
	
	if(GetMtIdxInput(&idx, "  查询电表档案") == 0) return;
	ScanDataTo2Hex((long)idx, MtIdx, LowToHigh);
	strcpy(db.filename, MtArchivesDBFName); 
	if(d_open(&db) != 0){
		LcdStart();
		printf("\n\n打开记录文件失败\n请重试!");
		WaitForESCKey();
		return;
	}
	for(i=1;i<=db.records;i++)
	{
		d_getrec(&db, (long)i);
		memcpy(buf, db.record_ptr, db.record_length);
		if(memcmp(buf, "\x20", 1) != 0)
			continue;		
		d_getfld(&db, 1, buf);
		if(memcmp(buf, MtIdx, 2) == 0){
			IsFind = 1;
			d_getfld(&db, 2, MtNum);
			d_getfld(&db, 3, MeasurePro);
			d_getfld(&db, 4, ConnWay);
			d_getfld(&db, 5, FeiLvNum);
			d_getfld(&db, 6, MtType);
			d_getfld(&db, 7, LineNum);
			d_getfld(&db, 8, BoxNum);
			d_getfld(&db, 9, CollNum);
			break;
		}
	}
	if(IsFind == 1){
		LcdStart();
		printf("【表序号】\n %d\n", idx);
		PrintMtNum(MtNum, 6);
		PrintMeasPro(MeasurePro);
		if (GetYesOrNo() == 0){
			d_close(&db);return;
		}
		LcdStart();
		PrintConnWay(ConnWay);
		PrintFvNum(FeiLvNum);
		if (GetYesOrNo() == 0){
			d_close(&db);return;
		}
		LcdStart();
		PrintMtType(MtType);
		PrintLineNum(LineNum);
		if (GetYesOrNo() == 0){
			d_close(&db);return;
		}
		LcdStart();
		PrintBoxNum(BoxNum);
		PrintCollNum(CollNum, 6);
		WaitForESCKey();
	}
	else{
		LcdStart();
		printf("\n未找到表序号为\n  %d\n的电表档案!请尝\n试从集中器中读取.", idx);
		WaitForESCKey();
	}
	d_close(&db);
}

static void QueryMtArchByNum(void)
{
	int idx, i, IsFind = 0;
	struct DBF db;
	uchar buf[25], TmpMtNum[13], MtIdx[3], MtNum[7], MeasurePro[2], ConnWay[2], FeiLvNum[2], MtType[2], LineNum[3], BoxNum[3], CollNum[7];
	
	if(GetMtNumInput(TmpMtNum, "  查询电表档案") == 0) return;
	TransformToMtNum(MtNum, TmpMtNum);
	
	strcpy(db.filename, MtArchivesDBFName); 
	if(d_open(&db) != 0){
		LcdStart();
		printf("\n\n打开记录文件失败\n请重试!");
		WaitForESCKey();
		return;
	}
	for(i=1;i<=db.records;i++)
	{
		d_getrec(&db, (long)i);
		memcpy(buf, db.record_ptr, db.record_length);
		if(memcmp(buf, "\x20", 1) != 0)
			continue;
		d_getfld(&db, 2, buf);
		if(memcmp(buf, MtNum, 6) == 0){
			IsFind = 1;
			d_getfld(&db, 1, MtIdx);
			d_getfld(&db, 3, MeasurePro);
			d_getfld(&db, 4, ConnWay);
			d_getfld(&db, 5, FeiLvNum);
			d_getfld(&db, 6, MtType);
			d_getfld(&db, 7, LineNum);
			d_getfld(&db, 8, BoxNum);
			d_getfld(&db, 9, CollNum);
			break;
		}
	}
	if(IsFind == 1){
		LcdStart();
		PrintMtIdx(MtIdx);
		PrintMtNum(MtNum, 6);
		PrintMeasPro(MeasurePro);
		if (GetYesOrNo() == 0){
			d_close(&db);return;
		}
		LcdStart();
		PrintConnWay(ConnWay);
		PrintFvNum(FeiLvNum);
		if (GetYesOrNo() == 0){
			d_close(&db);return;
		}
		LcdStart();
		PrintMtType(MtType);
		PrintLineNum(LineNum);
		if (GetYesOrNo() == 0){
			d_close(&db);return;
		}
		LcdStart();
		PrintBoxNum(BoxNum);
		PrintCollNum(CollNum, 6);
		WaitForESCKey();
	}
	else{
		LcdStart();
		printf("\n未找到\n");
		PrintMtNum(MtNum, 6);
		printf("的电表档案!请尝\n试从集中器中读取.");
		WaitForESCKey();
	}
	d_close(&db);
}

static void MtArchives(void)
{
	void (*p[])(void) = {AddMtArch, RdMtArch, DelMtArch, QueryMtArchByIdx, QueryMtArchByNum};
	const uchar *buf[5];

	buf[0] = AddMtArchStr;
	buf[1] = RdMtArchStr;
	buf[2] = DelMtArchStr;
	buf[3] = QueryMtArchByIdxStr;
	buf[4] = QueryMtArchByNumStr;

	InitMenu(buf, p, 5, 0);
}

static void ClientArchives(void)
{
	void (*p[])(void) = {ConfigMeasPointArch, RdMeasPointArch, DelMeasPointArch};
	const uchar *buf[3];
	
	buf[0] = ConfigMeasPointArchStr;
	buf[1] = RdMeasPointArchStr;
	buf[2] = DelMeasPointArchStr;

	InitMenu(buf, p, 3, 0);
}

void Archives(void)
{
	void (*p[])(void) = {MtArchives, ClientArchives};
	const uchar *buf[2];
	
	buf[0] = MtArchivesStr;
	buf[1] = ClientArchivesStr;

	InitMenu(buf, p, 2, 0);
}
 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

/*
 * Define all character string
 */

#include "strdef.h"
#include "menu.h"

/***********************************************************************/
static const uchar *ChineseYes = " 是";
static const uchar *ChineseNo = "否";
static const uchar *ChineseSureExit = "真要退出吗(y/n)?";
static const uchar *ChineseUserid = " 用户名:";
static const uchar *ChinesePasswd = "   密码:";
static const uchar *ChineseUserIdErr = " 用户名不存在!";
static const uchar *ChinesePasswdErr = "    密码错误!";
static const uchar *ChineseKaiFa = "深科技";

static const uchar *ChinesePosName = "集中器POS机";


static const uchar *ChineseLVCParamStr = "1.集中器参数";
static const uchar *ChineseArchivesStr = "2.电表测量点档案";
static const uchar *ChineseMaintainLVCStr = "3.集中器维护";
static const uchar *ChineseReadDataStr = "4.数据召测";
static const uchar *ChineseDataManageStr = "5.数据管理";
static const uchar *ChineseSystemManageStr = "6.系统管理";

static const uchar *ChineseLVCStatStr = "1.运行状态信息";
static const uchar *ChineseRdDiagnoseInfoStr = "2.读取自诊断信息";
static const uchar *ChineseSetLVCClockStr = "3.设置集中器时钟";
static const uchar *ChineseQueryLVCRtcStr = "4.读取集中器时钟";
static const uchar *ChineseResetLVCStr = "5.集中器复位";

static const uchar *ChineseResetMainBoardStr = "1.复位集中器主板";
static const uchar *ChineseResetPlcBoardStr = "2.复位载波板";
static const uchar *ChineseResetWirelessModemStr = "3.复位无线模块";

static const uchar *ChineseQuerySoftHardVersionStr = "1.软硬件版本号";
static const uchar *ChineseQueryProtocolVersionStr = "2.抄表协议版本号";
static const uchar *ChineseQueryChooseMtCntStr = "3.指定选读表数量";
static const uchar *ChineseQueryFinishRdMtCntStr = "4.成功抄收表数量";
static const uchar *ChineseQueryAutoFlagStr = "5.轮抄时间和标志";
static const uchar *ChineseQueryStatMeterCntStr = "6.表计总数统计";

static const uchar *ChineseMtDayFrozenEnergyStr = "1.集中器日冻结";
static const uchar *ChineseMtMonFrozenEnergyStr = "2.集中器月冻结";
static const uchar *ChineseMeasPointDataStr = "3.测量点数据";
static const uchar *ChineseCurrMtDataStr = "4.居民电表数据";

static const uchar *ChineseMPCurrEngyMusterStr = "1.当前电量集合";
static const uchar *ChineseMPCurrPlusEngyStr = "1.当日正有功电量";
static const uchar *ChineseMPClockStatWordStr = "2.时钟及状态字";

static const uchar *ChineseMtCurrEngyStr = "1.当前有功电量";
static const uchar *ChineseMtClockStr = "2.电表日期时间";
static const uchar *ChineseMtStatWordStr = "3.电表状态字";

static const uchar *ChineseAddMtArchStr = "1.设置电表档案";
static const uchar *ChineseDelMtArchStr = "3.删除电表档案";
static const uchar *ChineseRdMtArchStr = "2.读取电表档案";
static const uchar *ChineseQueryMtArchByIdxStr = "4.以表序号查询";
static const uchar *ChineseQueryMtArchByNumStr = "5.以表号查询"; 

static const uchar *ChineseSetLVCAddrStr = "1.设置集中器地址";
static const uchar *ChineseQueryLVCAddrStr = "2.读取集中器地址";
static const uchar *ChineseSetUpCommParamStr = "3.设置上行参数";
static const uchar *ChineseRdUpCommParamStr = "4.读取上行参数";
static const uchar *ChineseSetCasLVCCommParamStr = "5.设置级联参数";
static const uchar *ChineseRdCasLVCCommParamStr = "6.读取级联参数";
static const uchar *ChineseSetCascadeLVCAddrStr = "7.设置级联地址";
static const uchar *ChineseRdCascadeLVCAddrStr = "8.读取级联地址";

static const uchar *ChineseSetVPNModeStr = "1.VPN工作模式";
static const uchar *ChineseSetVPNIdPwdStr = "2.VPN用户及密码";
static const uchar *ChineseSetIPPortAPNStr = "3.主站IP端口APN";
static const uchar *ChineseSetPhoneMsgNumStr = "4.短信参数";

static const uchar *ChineseCalibrateTimeStr = "1.本机时钟";
static const uchar *ChineseModifyPwdStr = "2.修改密码";
static const uchar *ChineseCOMConfigStr = "3.串口配置";

static const uchar *ChineseCOMPortStr = "1.选择串口";
static const uchar *ChineseCOMBaudStr = "2.设置波特率";

static const uchar *ChineseQueryVPNModeStr = "1.VPN工作模式";
static const uchar *ChineseQueryVPNIdPwdStr = "2.VPN用户及密码";
static const uchar *ChineseQueryIpPortAPNStr = "3.主站IP端口APN";
static const uchar *ChineseQueryPhoneMsgNumStr = "4.短信参数";

static const uchar *ChineseMtArchivesStr = "1.居民电表档案";
static const uchar *ChineseClientArchivesStr = "2.测量点档案";

static const uchar *ChineseConfigMeasPointArchStr = "1.配置测量点档案";
static const uchar *ChineseRdMeasPointArchStr = "2.读取测量点档案";    
//static const uchar *ChineseQueryMeasPointArchStr = "3.查询测量点档案"; 
static const uchar *ChineseDelMeasPointArchStr = "3.删除测量点档案";

static const uchar *ChineseIPADDR = "IP地址:";
static const uchar *ChinesePORT = "端口号:";

static const uchar *ChineseViewDayFroDataStr = "1.查看日冻结数据";
static const uchar *ChineseClearDayFroDataStr = "2.清除日冻结数据";
static const uchar *ChineseViewMonFroDataStr = "3.查看月冻结数据";
static const uchar *ChineseClearMonFroDataStr = "4.清除月冻结数据";
static const uchar *ChineseClearMtArchStr = "5.清除电表档案";

/******************************************************************/

static const uchar *ChineseSending = "正在通信...";

static const uchar *ChineseSunday = "星期:日";
static const uchar *ChineseMonday = "星期:一";
static const uchar *ChineseTuesday = "星期:二";
static const uchar *ChineseWednesday = "星期:三";
static const uchar *ChineseThursday = "星期:四";
static const uchar *ChineseFriday = "星期:五";
static const uchar *ChineseSaturday = "星期:六";

static const uchar *ChineseOldPasswd = "旧密码:";
static const uchar *ChineseNewPasswd = "新密码:";
static const uchar *ChineseTypeAgain = "密码确认:";
static const uchar *ChineseModifyPasswdOk = "修改密码成功!";
static const uchar *ChinesePasswdNotMatch = "  密码不一致!";
/******************************************************************/
static const uchar *ChineseYesNo = "(y/n)?  ";
/****************************************************************************/
const uchar *Yes;
const uchar *No;
const uchar *SureExit;
const uchar *Userid;
const uchar *Passwd;
const uchar *UserIdErr;
const uchar *PasswdErr;
const uchar *KaiFa;

const uchar *Project;
const uchar *PosName;

const uchar *ReadDataStr;
const uchar *LVCParamStr;
const uchar *MaintainLVCStr;
const uchar *ArchivesStr;
const uchar *DataManageStr;
const uchar *SystemManageStr;

const uchar *MtDayFrozenEnergyStr;
const uchar *MtMonFrozenEnergyStr;
const uchar *MeasPointDataStr;
const uchar *CurrMtDataStr;

const uchar *MPCurrEngyMusterStr;
const uchar *MPCurrPlusEngyStr;
const uchar *MPClockStatWordStr;

const uchar *MtCurrEngyStr;
const uchar *MtClockStr;
const uchar *MtStatWordStr;

const uchar *SetLVCAddrStr;
const uchar *QueryLVCAddrStr;
const uchar *SetUpCommParamStr;
const uchar *RdUpCommParamStr;
const uchar *SetCasLVCCommParamStr;
const uchar *RdCasLVCCommParamStr;
const uchar *SetCascadeLVCAddrStr;
const uchar *RdCascadeLVCAddrStr;

const uchar *SetVPNModeStr;
const uchar *SetVPNIdPwdStr;
const uchar *SetIPPortAPNStr;
const uchar *SetPhoneMsgNumStr;

const uchar *CalibrateTimeStr;
const uchar *ModifyPwdStr;
const uchar *COMConfigStr;

const uchar *COMPortStr;
const uchar *COMBaudStr;

const uchar *QueryVPNModeStr;
const uchar *QueryVPNIdPwdStr;
const uchar *QueryIpPortAPNStr;
const uchar *QueryPhoneMsgNumStr;

const uchar *QueryCascadeLVCAddrStr;
const uchar *QueryLVCCommParamStr;

const uchar *LVCStatStr;
const uchar *RdDiagnoseInfoStr;
const uchar *SetLVCClockStr;
const uchar *QueryLVCRtcStr;
const uchar *ResetLVCStr;

const uchar *ResetMainBoardStr;
const uchar *ResetPlcBoardStr;
const uchar *ResetWirelessModemStr;

const uchar *MtArchivesStr;
const uchar *ClientArchivesStr;

const uchar *ConfigMeasPointArchStr;
const uchar *RdMeasPointArchStr;
//const uchar *QueryMeasPointArchStr;
const uchar *DelMeasPointArchStr;

const uchar *IPADDR;
const uchar *PORT;

const uchar *QueryAutoFlagStr;
const uchar *QuerySoftHardVersionStr;
const uchar *QueryProtocolVersionStr;
const uchar *QueryChooseMtCntStr;
const uchar *QueryFinishRdMtCntStr;
const uchar *QueryStatMeterCntStr;

const uchar *AddMtArchStr;
const uchar *DelMtArchStr;
const uchar *RdMtArchStr;
const uchar *QueryMtArchByIdxStr;
const uchar *QueryMtArchByNumStr;                           

const uchar *ViewDayFroDataStr;
const uchar *ClearDayFroDataStr;
const uchar *ViewMonFroDataStr;
const uchar *ClearMonFroDataStr;
const uchar *ClearMtArchStr;

/******************************************************************/

const uchar *Sending;

const uchar *Sunday;
const uchar *Monday;
const uchar *Tuesday;
const uchar *Wednesday;
const uchar *Thursday;
const uchar *Friday;
const uchar *Saturday;

const uchar *OldPasswd;
const uchar *NewPasswd;
const uchar *TypeAgain;
const uchar *ModifyPasswdOk;
const uchar *PasswdNotMatch;
/******************************************************************/
const uchar *YesNo;
/****************************************************************************/

void ChineseVerInit(void)
{
	Yes                     =  ChineseYes;
	No                      =  ChineseNo;
	SureExit                =  ChineseSureExit;
	Userid                  =  ChineseUserid;  
	Passwd                  =  ChinesePasswd;  
	UserIdErr               =  ChineseUserIdErr;
	PasswdErr               =  ChinesePasswdErr;
	KaiFa                   =  ChineseKaiFa;   
	
	PosName				    =  ChinesePosName;
	
	ReadDataStr				=	ChineseReadDataStr;
	LVCParamStr		        =	ChineseLVCParamStr;
	MaintainLVCStr			=	ChineseMaintainLVCStr;
	ArchivesStr				=	ChineseArchivesStr;
	DataManageStr           =  ChineseDataManageStr;
	SystemManageStr         =  ChineseSystemManageStr;
	
	MtDayFrozenEnergyStr	=  ChineseMtDayFrozenEnergyStr;
	MtMonFrozenEnergyStr	=  ChineseMtMonFrozenEnergyStr;
	MeasPointDataStr		=	ChineseMeasPointDataStr;
	CurrMtDataStr			=	ChineseCurrMtDataStr;
	
	MPCurrEngyMusterStr		=	ChineseMPCurrEngyMusterStr;
	MPCurrPlusEngyStr		=	ChineseMPCurrPlusEngyStr;
	MPClockStatWordStr		=	ChineseMPClockStatWordStr;
	
	MtCurrEngyStr			=	ChineseMtCurrEngyStr;
	MtClockStr				=	ChineseMtClockStr;
	MtStatWordStr			=	ChineseMtStatWordStr;
	
	AddMtArchStr			=	ChineseAddMtArchStr;
	DelMtArchStr			=	ChineseDelMtArchStr;
	RdMtArchStr				=	ChineseRdMtArchStr;
	QueryMtArchByIdxStr		=	ChineseQueryMtArchByIdxStr;
	QueryMtArchByNumStr		=	ChineseQueryMtArchByNumStr; 
	
	SetLVCAddrStr			=	ChineseSetLVCAddrStr;
	QueryLVCAddrStr			=	ChineseQueryLVCAddrStr;
	SetUpCommParamStr		=	ChineseSetUpCommParamStr;
	RdUpCommParamStr		=	ChineseRdUpCommParamStr;
	SetCasLVCCommParamStr	=	ChineseSetCasLVCCommParamStr;
	RdCasLVCCommParamStr	=	ChineseRdCasLVCCommParamStr;
	SetCascadeLVCAddrStr	=	ChineseSetCascadeLVCAddrStr;
	RdCascadeLVCAddrStr		=	ChineseRdCascadeLVCAddrStr;
	
	SetVPNModeStr			=	ChineseSetVPNModeStr;
	SetVPNIdPwdStr			=	ChineseSetVPNIdPwdStr;
	SetIPPortAPNStr			=	ChineseSetIPPortAPNStr;
	SetPhoneMsgNumStr		=	ChineseSetPhoneMsgNumStr;
	
	CalibrateTimeStr		=	ChineseCalibrateTimeStr;
	ModifyPwdStr			=	ChineseModifyPwdStr;
	COMConfigStr			=	ChineseCOMConfigStr;
	
	COMPortStr				=	ChineseCOMPortStr;
	COMBaudStr				=	ChineseCOMBaudStr;
	
	QueryVPNModeStr			=	ChineseQueryVPNModeStr;
	QueryVPNIdPwdStr		=	ChineseQueryVPNIdPwdStr;
	QueryIpPortAPNStr		=	ChineseQueryIpPortAPNStr;
	QueryPhoneMsgNumStr		=	ChineseQueryPhoneMsgNumStr;
	
	IPADDR					=  ChineseIPADDR;
	PORT					=  ChinesePORT;
	
	LVCStatStr				=	ChineseLVCStatStr;
	RdDiagnoseInfoStr		=	ChineseRdDiagnoseInfoStr;
	SetLVCClockStr			=	ChineseSetLVCClockStr;
	QueryLVCRtcStr			=	ChineseQueryLVCRtcStr;
	ResetLVCStr				=	ChineseResetLVCStr;	
	
	ResetMainBoardStr		=	ChineseResetMainBoardStr;
	ResetPlcBoardStr		=	ChineseResetPlcBoardStr;
	ResetWirelessModemStr	=	ChineseResetWirelessModemStr;
	
	QueryAutoFlagStr		=  ChineseQueryAutoFlagStr;
	QuerySoftHardVersionStr	=  ChineseQuerySoftHardVersionStr;
	QueryProtocolVersionStr	=	ChineseQueryProtocolVersionStr;
	QueryChooseMtCntStr		=	ChineseQueryChooseMtCntStr;
	QueryFinishRdMtCntStr	=	ChineseQueryFinishRdMtCntStr;
	QueryStatMeterCntStr	=	ChineseQueryStatMeterCntStr;
	
	MtArchivesStr			=	ChineseMtArchivesStr;
	ClientArchivesStr		=	ChineseClientArchivesStr;
	
	ConfigMeasPointArchStr	=	ChineseConfigMeasPointArchStr;
	RdMeasPointArchStr		=	ChineseRdMeasPointArchStr;
	//QueryMeasPointArchStr	=	ChineseQueryMeasPointArchStr;
	DelMeasPointArchStr		=	ChineseDelMeasPointArchStr;	
	
	ViewDayFroDataStr		=	ChineseViewDayFroDataStr;
	ClearDayFroDataStr		=	ChineseClearDayFroDataStr;
	ViewMonFroDataStr		=	ChineseViewMonFroDataStr;
	ClearMonFroDataStr		=	ChineseClearMonFroDataStr;
	ClearMtArchStr			=	ChineseClearMtArchStr;
	
	Sending					=	ChineseSending;
	
	Sunday                  =  ChineseSunday;  
	Monday                  =  ChineseMonday;  
	Tuesday                 =  ChineseTuesday; 
	Wednesday               =  ChineseWednesday;
	Thursday                =  ChineseThursday;
	Friday                  =  ChineseFriday;  
	Saturday                =  ChineseSaturday;
	
	OldPasswd				=	ChineseOldPasswd;
	NewPasswd				=	ChineseNewPasswd;
	TypeAgain				=	ChineseTypeAgain;
	ModifyPasswdOk			=	ChineseModifyPasswdOk;
	PasswdNotMatch			=	ChinesePasswdNotMatch;
}            

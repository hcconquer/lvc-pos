 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <stdio.h>
#include <conio.h>
#include <mem.h>
#include <dos.h>
#include "typedef.h"
#include "func.h"
#include "menu.h"
#include "strdef.h"
#include "input.h"
#include "lcdout.h"
#include "common.h"
#include "lvc_pos.h"
#include "global.h"

extern uchar pointer[4];

static void SetClock(void)
{
	struct time time;
	struct date date;
	uchar InputLen = 0;
	uchar input[8];
	
	while (1) {
		LcdStart();
		LcdPrintS(2, 0, "YYYYMMDDHHMMSSWW");
		RtcReadClock(input);
		InputLen = LcdInput(input, 3, 0, 16, 0, NOAUTOSKIP);
		if (input[0] == EXIT_VALUE)
			return;
		if (InputLen == 16)
			break;
		if (InputLen == 'H' || InputLen == 'P')
			continue;
		LcdPrintS(7, 0, "  输入时间有误!");
		KeyWait();
	}
	date.da_year = (input[0] >> 4) * 1000 + (input[0] & 0x0F) * 100 + (input[1] >> 4) * 10 + (input[1] & 0x0F);
	date.da_mon = (input[2] >> 4) * 10 + (input[2] & 0x0F);
	date.da_day = (input[3] >> 4) * 10 + (input[3] & 0x0F);
	time.ti_hour = (input[4] >> 4) * 10 + (input[4] & 0x0F);
	time.ti_min = (input[5] >> 4) * 10 + (input[5] & 0x0F);
	time.ti_sec = (input[6] >> 4) * 10 + (input[6] & 0x0F);
	time.ti_hund = 0;
	setdate(&date);
	settime(&time);
	LcdStart();
	LcdPrintS(1, 1, "设置成功!");
	KeyWait();
}

static void CalibrateTime(void)
{
	uchar key, clock[16], second;
	//const uchar *week[7] = {Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday};
	const uchar *week[7];
	
	week[0] = "星期天"; week[1] = "星期一"; week[2] = "星期二";
	week[3] = "星期三"; week[4] = "星期四"; week[5] = "星期五"; week[6] = "星期六";
	LcdStart();
	LcdPrintS(0, 7, "-");
	LcdPrintS(0, 10, "-");
	LcdPrintS(1, 9, ":");
	LcdPrintS(1, 12, ":");
	LcdPrintS(7, 0, "退出");
	LcdPrintS(7, 11, "设置");
	second = 0;
	while (1) {
		TimeToClock(clock);
		if (second != clock[13]) {
			LcdPrintC(0, 3, clock[0]); // year
			LcdPrintC(0, 4, clock[1]);
			LcdPrintC(0, 5, clock[2]);
			LcdPrintC(0, 6, clock[3]);
			LcdPrintC(0, 8, clock[4]); // month
			LcdPrintC(0, 9, clock[5]);
			LcdPrintC(0, 11, clock[6]); // day
			LcdPrintC(0, 12, clock[7]);
			LcdPrintS(1, 0, week[AscToValue(clock[15])]); // week
			LcdPrintC(1, 7, clock[8]); // hour
			LcdPrintC(1, 8, clock[9]);
			LcdPrintC(1, 10, clock[10]); // minute
			LcdPrintC(1, 11, clock[11]);
			LcdPrintC(1, 13, clock[12]); // second
			LcdPrintC(1, 14, clock[13]);
			second = clock[13];
		}
		if (kbhit()) {
			key = KeyScan();
			switch (key) {
				case '\r':
					SetClock();		
					return;
				case 27:
					return;
				default:
					break;
			}
		}
	}
}

static void ModifyPwd(void)
{
	uchar PasswdAscii[5], PasswdAsciiSec[5];
	FILE *fp;
	USERINFO userinfo;
	
	while (1) {
		LcdStart();
		LcdPrintS(2, 2, OldPasswd);
		gotoxy(10, 3);
		LcdInput(pointer, 2, 9, 4, 1, NOAUTOSKIP);
		if (pointer[0] == EXIT_VALUE)
			return;
		BcdToAscii(PasswdAscii, pointer, 2);
		if (passwdcmp(PasswdAscii) == False) {
			LcdPrintS(2, 9, "    ");
			LcdPrintS(7, 1, PasswdErr);
			KeyWait();
			continue;
		}
		while (1) {
			LcdStart();
			LcdPrintS(2, 2, NewPasswd);
			LcdInput(pointer, 2, 9, 4, 1, NOAUTOSKIP);
			if (pointer[0] == EXIT_VALUE)
				return;
			BcdToAscii(PasswdAscii, pointer, 2);
			LcdStart();
			LcdPrintS(2, 2, TypeAgain);
			LcdInput(pointer, 2, 11, 4, 1, NOAUTOSKIP);
			if (pointer[0] == EXIT_VALUE)
				return;
			BcdToAscii(PasswdAsciiSec, pointer, 2);
			if (memcmp(PasswdAscii, PasswdAsciiSec, 4) == 0) {
				fp = fopen("LVCPOS.INI", "rb+");
				fread((uchar *)(&userinfo), sizeof(userinfo), 1, fp);
				memcpy(userinfo.passwd, PasswdAscii, 4);
				fseek(fp, 0l, SEEK_SET);
				fwrite((uchar *)(&userinfo), sizeof(userinfo), 1, fp);
				fclose(fp);
				LcdStart();
				LcdPrintS(2, 0, ModifyPasswdOk);				
				KeyWait();
				return;
			}
			LcdStart();
			LcdPrintS(2, 1, PasswdNotMatch);
			KeyWait();
		}		
	}
}

static void COMPort()
{
	LcdStart();
	printf("    选择串口\n");
	printf("当前串口:COM%d\n", G_COMPort);
	if (G_COMPort == COM1)
		printf("是否更改为COM2?\n");
	if (G_COMPort == COM2)
		printf("是否更改为COM1?\n");
	printf("\n\n\n\n否            是");
	if (GetYesOrNo() == 1){
		switch (G_COMPort){
			case COM1:G_COMPort = COM2;break;
			case COM2:G_COMPort = COM1;break;
		}
	}
}

static void COMBaud()
{
	int cmd = -1;
	
	while(1){
		LcdStart();
		printf(" 设置串口波特率\n");
		printf("当前串口:COM%d\n",G_COMPort);
		printf("当前波特率:%ld\n", GBaud);
		printf("\n\n\n\n返回        设置");
		cmd = GetYesOrNo();
		if (cmd == 1)
			SetCOMBaud();
		if (cmd == 0)
			return;
	}
}

static void COMConfig()
{
	void (*p[])(void) = {COMPort, COMBaud};
	const uchar *buf[2];

	buf[0] = COMPortStr;
	buf[1] = COMBaudStr;
	
	InitMenu(buf, p, 2, 0);	
}

void ManageSys(void)
{
	void (*p[])(void) = {CalibrateTime, ModifyPwd, COMConfig};
	const uchar *buf[3];

	buf[0] = CalibrateTimeStr;
	buf[1] = ModifyPwdStr;
	buf[2] = COMConfigStr;
	
	InitMenu(buf, p, 3, 0);
}
# GNU make makefile in windows and Dos, for Turbo C 2.0 and Borland C 3.1
# Confirm GNU make is in you PATH
SHELL=cmd.exe

CC_HOME=D:/BORLANDC
BIN=$(CC_HOME)/BIN
INCLUDES=$(CC_HOME)/INCLUDE;./INCLUDE
LIBS=$(CC_HOME)/LIB;./LIBS

export PATH=%PATH%;$(BIN)
CC=$(BIN)/bcc

DIST=Release
TARGET=pos.exe

CFLAGS=-c -mm -O2 -G -I$(INCLUDES) -n$(DIST)
LFLAGS=-mm -L$(LIBS) -n$(DIST)

all:link

link:compile
	$(CC) -e$(TARGET) $(LFLAGS) $(DIST)/*.obj
compile:setup
	$(CC) $(CFLAGS) *.c
setup:
	if not exist $(DIST) mkdir $(DIST)

clean:
	del /Q $(DIST)

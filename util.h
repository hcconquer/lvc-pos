/*
 * Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 * util.h
 * Utility platform-independent functions.
 *
 * Author:	HanChen
 * Create:	2008-09-21
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <stdio.h>
#include "typedef.h"

BYTE bcd_to_bin(BYTE val);

BYTE bin_to_bcd(BYTE val);

WORD ctos(const BYTE *buf);

void stoc(BYTE *buf, WORD val);

DWORD ctol(const BYTE *buf);

void ltoc(BYTE *buf, DWORD val);

WORD msb_ctos(const BYTE *buf);

DWORD msb_ctol(const BYTE *buf);

void msb_stoc(BYTE *buf, WORD val);

void msb_ltoc(BYTE *buf, DWORD val);

long fsize(FILE *fp);

void PRINTB(const BYTE *buf, int len);

void MD5Iter(BYTE *hash, const BYTE *buf, int len, int seg_len);

#endif /* UTIL_H_ */

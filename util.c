/*
 * Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 * util.h
 * Utility platform-independent functions.
 *
 * Author:	ChenHan
 * Create:	2008-9-19
 */

#include <stdio.h>
#include "md5.h"
#include "typedef.h"
#include "util.h"

BYTE bcd_to_bin(BYTE val)
{
	return (val >> 4) * 10 + (val & 0x0f);
}

BYTE bin_to_bcd(BYTE val)
{
	return ((val / 10) << 4) + (val % 10);
}

WORD ctos(const BYTE *buf)
{
	return buf[0] + (buf[1] << 8);
}

void stoc(BYTE *buf, WORD val)
{
	buf[0] = val;
	buf[1] = val >> 8;
}

DWORD ctol(const BYTE *buf)
{
	return buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
}

void ltoc(BYTE *buf, DWORD val)
{
	buf[0] = val;
	buf[1] = val >> 8;
	buf[2] = val >> 16;
	buf[3] = val >> 24;
}

WORD msb_ctos(const BYTE *buf)
{
	return buf[1] + (buf[0] << 8);
}

DWORD msb_ctol(const BYTE *buf)
{
	return buf[3] + (buf[2] << 8) + (buf[1] << 16) + (buf[0] << 24);
}

void msb_stoc(BYTE *buf, WORD val)
{
	buf[0] = val >> 8;
	buf[1] = val;
}

void msb_ltoc(BYTE *buf, DWORD val)
{
	buf[0] = val >> 24;
	buf[1] = val >> 16;
	buf[2] = val >> 8;
	buf[3] = val;
}

long fsize(FILE *fp)
{
	long cur, size;

	cur = ftell(fp);
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, cur, SEEK_SET);
	return size;
}

void PRINTB(const BYTE *buf, int len)
{
	while (len-- > 0)
	{
		printf("%02X ", *buf++);
	}
}

void MD5Iter(BYTE *hash, const BYTE *buf, int len, int seg_len)
{
	MD5_CTX md5;
	int tmp;
	const BYTE *ptr = buf;

	MD5Init(&md5);
	while(len > 0) {
		if (len > seg_len)
			tmp = seg_len;
		else
			tmp = len;
		MD5Update(&md5, ptr, tmp);
		ptr += tmp; len -= tmp;
	}
	MD5Final(hash, &md5);
}

 /*	Copyright (C) 2007-2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *	Author: Song Hao
 *	Created on: 2008-01-10
 */

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <mem.h>
#include <string.h>
#include <dos.h>
#include <stddef.h>
#include "typedef.h"
#include "strdef.h"
#include "htxlcd.h"
#include "menu.h"
#include "lvc_pos.h"
#include "lcdout.h"
#include "common.h"
#include "rs232.h"
#include "global.h"
#include "func.h"

static int FillQueryLVCInfoData(int fn, uchar *data)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	return ptr - data;
}

static void GetRecvStr(uchar *data, uchar *APN)
{
	uchar *pData = data, *pAPN = APN;
	int i;
	
	for(i = 0; i<16; i++)
	{
		if(*pData == 0x00){
			*pAPN = '\0';
			break;
		}
		*pAPN++ = *pData;
		pData++;
	}
}

static void PrintVPNMode(uchar *data)
{
	switch(*data)
	{
		case 0x01:
			printf("永久在线\n");
			break;
		case 0x02:
			printf("短信唤醒\n");
			break;
		case 0xff:
			printf("非GPRS/CDMA\n");
			break;
		default:
			break;
	}
}

int FillQueryLVCAddrData(uchar *data)
{
	uchar *ptr = data;
	
	memcpy(ptr, "\x00\x00\x20\x0D\x00\x90\x01", 7);
	ptr += 7;
	memcpy(ptr, GSetStr, 6);
	ptr += 6;
	
	return ptr - data;
}

static void PrintPhoneMsgNum(uchar *num)
{
	uchar *ptr = num;
	int i, j;
	int gao, di;
	
	for(i = 0; i < 8; i++){
		gao = (*ptr >> 4) & 0x0f;
		di = (*ptr) & 0x0f;
		if(gao == 0x0f){
			for(j = 0;j<8-i;j++)
				printf("FF");
			break;
		}
		if(di == 0x0f){
			if(gao > 0x09)
				printf("%c", ValueToAsc(gao));
			else
				printf("%d", gao);
			printf("F");
			for(j=0;j<8-i-1;j++)
				printf("FF");
			break;
		}
		if((gao > 0x09) && (di > 0x09))
			printf("%c%c", ValueToAsc(gao), ValueToAsc(di));
		else if(gao > 0x09)
			printf("%c%d", ValueToAsc(gao), di);
		else if(di > 0x09)
			printf("%d%c", gao, ValueToAsc(di));
		else
			printf("%d%d", gao, di);
		ptr++;
	}	
}

static void PrintCommCtrlWord(uchar *data)
{
	printf("波特率:");
	switch(((*data >> 5) & 0x07)){
		case 0:printf("300\n");break;
		case 1:printf("600\n");break;
		case 2:printf("1200\n");break;
		case 3:printf("2400\n");break;
		case 4:printf("4800\n");break;
		case 5:printf("7200\n");break;
		case 6:printf("9600\n");break;
		case 7:printf("19200\n");break;
		default:printf("未知\n");break;
	}
	printf("停止位:%d位\n", ((*data >> 4) & 0x01) + 1);
	if(((*data >> 3) & 0x01) == 0)
		printf("校验位:无\n");
	else 
		printf("校验位:有\n");
	if(((*data >> 2) & 0x01) == 0)
		printf("校验方式:偶校验\n");
	else 
		printf("校验方式:奇校验\n");
}

static void PrintCascadeLVCAddr(uchar *addr, int len, int flag)
{
	if(memcmp(addr, "\xff\xff", len) == 0)
		printf("ffff");
	else if(memcmp(addr, "\x00\x00", len) == 0)
		printf("无效地址");
	else{
		switch(flag){
			case ORDER:
				printf("%ld", Scan2HexToData(addr, HighToLow));
				break;
			case NOTORDER:
				printf("%ld", Scan2HexToData(addr, LowToHigh));
				break;
			default:
				break;
		}
	}
}

static int GetLVCAddrInput(uchar *addr)
{
	uchar AddrTemp[10] = "\0", *ptr = addr;
	int i;
	
	do{
		LcdStart();
		printf("   集中器地址\n");
		printf("请输入新地址:\n");
		if(GetString(AddrTemp, 8, TYPE_NUMBER) == 0) return 0;
	}while(CheckStrLen(AddrTemp, 8, 8) == 0);
	
	*ptr++ = (AscToValue(AddrTemp[0]) << 4) + AscToValue(AddrTemp[1]); 
	*ptr++ = (AscToValue(AddrTemp[2]) << 4) + AscToValue(AddrTemp[3]); 
	*ptr++ = (AscToValue(AddrTemp[4]) << 4) + AscToValue(AddrTemp[5]); 
	*ptr++ = (AscToValue(AddrTemp[6]) << 4) + AscToValue(AddrTemp[7]); 
	
	return 1;
}

static int FillLVCAddrData(uchar *addr, uchar *data)
{
	uchar *ptr = data;

	memcpy(ptr, "\x00\x00\x20\x0d\x00\x90\x01\x00\x04", 9);
	ptr += 9;
	memcpy(ptr, addr, 4);
	ptr += 4;
	memcpy(ptr, "\x00\x00", 2);
	ptr += 2;
	memcpy(ptr, GSetStr, 6);
	ptr += 6;
	return ptr - data;
}

static int CheckIPInput(uchar *ip)
{
	int NumCnt = 0, PointCnt = 0, LastPoint = -1;
	int i, IPSection=0;
	uchar str[5];
	
	for(i=0; i<strlen(ip); i++){
		if(ip[i] == '.'){
			PointCnt++;
			NumCnt = 0;
			if (i - LastPoint > 1){
				memcpy(str, ip + LastPoint + 1, i - LastPoint - 1);
				str[i - LastPoint] = '\0';
				IPSection = atoi(str);
				if (IPSection > 255){
					printf("错误: IP地址格式\n不对!");
					getch();return 0;	
				}
			}
			else{
				printf("错误: IP地址格式\n不对!");
				getch();return 0;	
			}
			LastPoint=i;
		}
		else{
			NumCnt++;
			if(NumCnt > 3){
				printf("错误: IP地址格式\n不对!");
				getch();return 0;
			}
		}			
	}
	if(PointCnt != 3){
		printf("错误: IP地址格式\n不对!");
		getch();return 0;
	}
	
	return 1;
}

static int GetIpPort(uchar *IpPort, uchar *notice)
{
	uchar IP[20] = "\0", PortStr[8] = "\0";
	long port;
	
	do{
		LcdStart();
		printf("%s\n", notice);
		printf("%s\n", IPADDR);
		if(GetString(IP, 15, TYPE_FLOAT) == 0) return 0;
	}while(CheckIPInput(IP) == 0);
	ScanIP(IP, strlen(IP), IpPort);
	
	do{
		LcdStart();
		printf("%s\n", notice);
		printf("%s\n", PORT);
		if(GetString(PortStr, 5, TYPE_NUMBER) == 0) return 0;
		port = atol(PortStr);
	}while(CheckRange(port, 1, 65535) == 0);
	ScanDataTo2Hex(port, IpPort + 4, HighToLow);
	
	return 1;
}

static int GetAPN(uchar *APN, int len)
{
	uchar str[20] = "\0";
	
	LcdStart();
	printf("    APN输入\n");
	printf("APN:\n");	
	if(GetString(str, len, TYPE_ALPHANUMERIC) == 0) return 0;
	ScanInputStr(APN, str, len);
	
	return 1;
}

static int FillSetIpPortAPNData(int fn, uchar *data, uchar *MainIpPort, uchar *StandbyIpPort, uchar *GatewayPort, uchar *AgencyPort, uchar *APN)
{
	uchar FnChar[4];
	uchar *ptr = data;
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, MainIpPort, 6);
	ptr += 6;
	memcpy(ptr, StandbyIpPort, 6);
	ptr += 6;
	memcpy(ptr, GatewayPort, 6);
	ptr += 6;
	memcpy(ptr, AgencyPort, 6);
	ptr += 6;
	memcpy(ptr, APN, 16);
	ptr += 16;
	return ptr - data;	
}

static int GetVPNID(uchar *id)
{	
	char str[20] = "\0";
	
	LcdStart();
	printf(" VPN用户及密码\n");
	printf("VPN 用户名:\n");
	if(GetString(str, 16, TYPE_ALPHANUMERIC) == 0) return 0;
	ScanInputStr(id, str, 16);
	
	return 1;
}

static int GetVPNPwd(uchar *pwd)
{	
	char str[20] = "\0";
	
	LcdStart();
	printf("  VPN ID&密码\n");
	printf("VPN 密码:\n");
	if(GetString(str, 16, TYPE_ALPHANUMERIC) == 0) return 0;
	ScanInputStr(pwd, str, 16);
	
	return 1;
}

static int FillSetVPNIdPwdData(int fn, uchar *data, uchar *id, uchar *pwd)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, id, 16);
	ptr += 16;
	memcpy(ptr, pwd, 16);
	ptr += 16;
	return ptr - data;	
}

static int CheckPhoneMsgNum(uchar *num)
{
	int i;
	
	if(strlen(num) != 16){
		printf("错误: 数据长度应\n为16!");
		getch();return 0;
	}
	for(i = 0; i < 16; i++){
		if(num[i] == '\0') 
			break;		
		if((num[i] >=  48) && (num[i] <= 57))
			continue;
		else if((num[i] == 'a') || (num[i] == 'A') || (num[i] == 'b') || (num[i] == 'B') || (num[i] == 'f') || (num[i] == 'F'))
			continue;
		else{
			printf("错误: 输入字符应\n在0-9,a-b,A-B之\n间.");
			getch();return 0;
		}
	}
	
	return 1;
}

static void ScanPhoneMsgInput(uchar *Dst, uchar *Src, int DstCnt)
{
	int i, j, k;
	
	for(i = 0; i < DstCnt; i++){
		if(Src[2*i] == '\0'){
			k = i;
			for(j = 0; j < DstCnt - i; j++){
				memcpy(&Dst[k], "\xff", 1);
				k++;
			}
			break;
		}
		else if(Src[2*i + 1] == '\0'){
			Dst[i] = ((AscToValue(Src[2*i]) << 4) & 0xf0) + 0x0f;
			k = i + 1;
			for(j = 0; j < DstCnt - i - 1; j++){
				memcpy(&Dst[k], "\xff", 1);
				k++;
			}
			break;
		}
		else
			Dst[i] = ((AscToValue(Src[2*i]) << 4) & 0xf0) + (AscToValue(Src[2*i + 1]) & 0x0f);
	}
}

static int GetPhoneNum(uchar *num)
{
	uchar Phone[20] = "\0";
	
	do{
		LcdStart();
		printf("    短信参数\n");
		printf("主站电话号码:\n");
		if(GetString(Phone, 16, TYPE_ALPHANUMERIC) == 0) return 0;
	}while(CheckPhoneMsgNum(Phone) == 0);
	ScanPhoneMsgInput(num, Phone, 8);
	
	return 1;
}

static int GetMsgNum(uchar *num)
{
	uchar Msg[20] = "\0";
	
	do{
		LcdStart();
		printf("    短信参数\n");
		printf("短信中心号码:\n");
		if(GetString(Msg, 16, TYPE_ALPHANUMERIC) == 0) return 0;		
	}while(CheckPhoneMsgNum(Msg) == 0);
	ScanPhoneMsgInput(num, Msg, 8);	
	
	return 1;
}

static int FillSetPhoneMsgNumData(int fn, uchar *data, uchar *PhoneNum, uchar *MsgNum)
{
	uchar FnChar[4];
	uchar *ptr = data;
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, PhoneNum, 8);
	ptr += 8;
	memcpy(ptr, MsgNum, 8);
	ptr += 8;
	return ptr - data;
}

static int GetCtrlWordBaud(uchar *word)
{
	int cmd;
	char str[3] = "6";
	
	do{
		LcdStart();
		printf("0:300  1:600\n2 : 1200\n3 : 2400\n4 : 4800\n5 : 7200\n6 : 9600\n7 : 19200\n");
		printf("请选择波特率:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd = atoi(str);
	}while(CheckRange((long)cmd, 0, 7) == 0);
	*word = (cmd << 5) & 0xe0;
	
	return 1;
}

static int GetCtrlWordStopBit(uchar *word)
{
	int cmd;
	char str[3] = "0";
	
	do{
		LcdStart();
		printf("   通信控制字\n");
		printf("0 : 停止位1位\n1 : 停止位2位\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd = atoi(str);
	}while(CheckRange((long)cmd, 0, 1) == 0);
	*word = (*word) | ((cmd << 4) & 0x10);
	
	return 1;
}

static int GetCtrlWordVerifyBit(uchar *word)
{
	int cmd;
	char str[3] = "1";
	
	do{
		LcdStart();
		printf("   通信控制字\n");
		printf("0 : 无校验位\n1 : 有校验位\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd = atoi(str);
	}while(CheckRange((long)cmd, 0, 1) == 0);
	*word = (*word) | ((cmd << 3) & 0x08);
	
	return 1;
}

static int GetCtrlWordVerifyMode(uchar *word)
{
	int cmd;
	char str[3] = "0";
	
	do{
		LcdStart();
		printf("   通信控制字\n");
		printf("0 : 偶校验\n1 : 奇校验\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd = atoi(str);
	}while(CheckRange((long)cmd, 0, 1) == 0);
	*word = (*word) | ((cmd << 2) & 0x04);
	
	return 1;
}

static int GetCtrlWordBitCnt(uchar *word)
{
	int cmd;
	char str[3] = "3";
	
	do{
		LcdStart();
		printf("   通信控制字\n");
		printf("数据位数:\n0 : 5位\n1 : 6位\n2 : 7位\n3 : 8位\n");
		printf("请选择:");
		if(GetString(str, 1, TYPE_NUMBER) == 0) return 0;
		cmd = atoi(str);
	}while(CheckRange((long)cmd, 0, 3) == 0);
	*word = (*word) | cmd;
	
	return 1;
}

static int GetCommCtrlWordInput(uchar *word)
{	
	if(GetCtrlWordBaud(word) == 0) return 0;
	if(GetCtrlWordStopBit(word) == 0) return 0;
	if(GetCtrlWordVerifyBit(word) == 0) return 0;
	if(GetCtrlWordVerifyMode(word) == 0) return 0;
	if(GetCtrlWordBitCnt(word) == 0) return 0;
	
	return 1;
}

static int GetRecvCascadeTimeOut(int *RecvCascadeTimeOut)
{
	char str[3] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("  级联通信参数\n");
		printf("主LVC接收级联LVC\n的等待报文超时时\n间(100ms):");
		if(GetString(str, 3, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 0, 255) == 0);
	*RecvCascadeTimeOut = val;
	
	return 1;
}

static int GetRecvTimeOut(int *RecvTimeOut)
{
	char str[3] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("  级联通信参数\n");
		printf("接收等待字节超时\n时间(10ms):");
		if(GetString(str, 3, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 0, 255) == 0);
	*RecvTimeOut = val;
	
	return 1;
}

static int GetDelayTimeOut(int *DelayTimeOut)
{
	char str[3] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("  级联通信参数\n");
		printf("集中器发送传输延\n时时间(分):");
		if(GetString(str, 3, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 0, 255) == 0);
	*DelayTimeOut = val;
	
	return 1;
}

static int GetResponseTimeOut(int *ResponseTimeOut)
{
	char str[3] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("  级联通信参数\n");
		printf("集中器等待从动站\n响应超时时间(s):\n");
		if(GetString(str, 3, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 0, 255) == 0);
	*ResponseTimeOut = val;
	
	return 1;
}

static int GetReSendTimes(int *ReSendTimes)
{
	char str[3] = "\0";
	int val;
	
	do{
		LcdStart();
		printf("  级联通信参数\n");
		printf("集中器等待从动站\n响应重试次数:\n");
		if(GetString(str, 3, TYPE_NUMBER) == 0) return 0;
		val = atoi(str);
	}while(CheckRange((long)val, 0, 255) == 0);
	*ReSendTimes = val;
	
	return 1;
}

static int FillSetLVCCommParamData(int fn, uchar *data, uchar CommCtrlWord, int RecvCascadeTimeOut, 
			int RecvTimeOut, int DelayTimeOut, int ResponseTimeOut, int ReSendTimes)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = CommCtrlWord;
	*ptr++ = RecvCascadeTimeOut;
	*ptr++ = RecvTimeOut;
	*ptr++ = DelayTimeOut;
	*ptr++ = ResponseTimeOut;
	*ptr++ = ReSendTimes;
	
	return ptr - data;
}

static int GetVPNModeInput(uchar *VPNMode)
{
	char cmd[3] = "3";
	int val;
	
	do{
		LcdStart();
		printf("虚拟专网工作方式\n");
		printf("1 : 永久在线\n2 : 短信唤醒\n3 : 非GPRS/CDMA\n");
		printf("请选择:");
		if(GetString(cmd, 1, TYPE_NUMBER) == 0) return 0;
		val = atoi(cmd);
	}while(CheckRange((long)val, 1, 3) == 0);
	
	switch(cmd[0]){
		case '1':
			*VPNMode = 0x01;
			break;
		case '2':
			*VPNMode = 0x02;
			break;
		case '3':
			*VPNMode = 0xff;
			break;
		default:
			break;
	}
	return 1;
}

static int GetReDialTimeOutInput(long *TimeOut)
{
	char str[6] = "\0";
	long val;
	
	do{
		LcdStart();
		printf("  VPN工作模式\n");
		printf("永久在线模式重拨\n间隔(秒):");
		if(GetString(str, 5, TYPE_NUMBER) == 0) return 0;
		val = atol(str);
	}while(CheckRange(val, 1, 65535) == 0);
	*TimeOut = val;
	
	return 1;
}

static int GetReDialTimesInput(int *times)
{
	char str[5] = "\0";
	int t;
	
	do{
		LcdStart();
		printf("  VPN工作模式\n");
		printf("被动激活模式重拨\n次数:");
		if(GetString(str, 3, TYPE_NUMBER) == 0) return 0;
		t = atoi(str);
	}while(CheckRange((long)t, 0, 255) == 0);
	*times = t;
	
	return 1;
}

static int GetCutOffTimeOutInput(int *timeout)
{
	char str[5] = "\0";
	int t;
	
	do{
		LcdStart();
		printf("  VPN工作模式\n");
		printf("被动激活模式连续\n无通信自动断线时\n间(分钟):");
		if(GetString(str, 3, TYPE_NUMBER) == 0) return 0;
		t = atoi(str);
	}while(CheckRange((long)t, 0, 255) == 0);
	*timeout = t;
	
	return 1;
}

static int FillSetVPNModeData(int fn, uchar *data, uchar VPNMode, uchar *ReDialTimeOut, int ReDialTimes, int CutOffTimeOut)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	*ptr++ = VPNMode;
	*ptr++ = ReDialTimeOut[0];
	*ptr++ = ReDialTimeOut[1];
	*ptr++ = ReDialTimes;
	*ptr++ = CutOffTimeOut;
	
	return ptr - data;
}

static int FillSetCasAddrData(int fn, uchar *data, uchar *Cas1Code, uchar *Cas1Addr, 
			uchar *Cas2Code, uchar *Cas2Addr, uchar *Cas3Code, uchar *Cas3Addr, uchar *Cas4Code, uchar *Cas4Addr)
{
	uchar *ptr = data;
	uchar FnChar[4];
	
	GetFn(fn, FnChar);
	memcpy(ptr, FnChar, 4);
	ptr += 4;
	memcpy(ptr, Cas1Code, 2);
	ptr += 2;
	memcpy(ptr, Cas1Addr, 2);
	ptr += 2;
	memcpy(ptr, Cas2Code, 2);
	ptr += 2;
	memcpy(ptr, Cas2Addr, 2);
	ptr += 2;
	memcpy(ptr, Cas3Code, 2);
	ptr += 2;
	memcpy(ptr, Cas3Addr, 2);
	ptr += 2;
	memcpy(ptr, Cas4Code, 2);
	ptr += 2;
	memcpy(ptr, Cas4Addr, 2);
	ptr += 2;
	
	return ptr - data;
}

static int GetCasCode(uchar *CasCode, uchar *notice)
{
	char str[6] = "\0";
	
	do{
		LcdStart();
		printf("  设置级联地址\n");
		if(notice != NULL)
			printf("%s\n", notice);
		if(GetString(str, 4, TYPE_NUMBER) == 0) return 0;
	}while(CheckStrLen(str, 4, 4) == 0);
	CasCode[0] = ((AscToValue(str[0]) << 4) & 0xf0) + (AscToValue(str[1]) & 0x0f);
	CasCode[1] = ((AscToValue(str[2]) << 4) & 0xf0) + (AscToValue(str[3]) & 0x0f);
	
	return 1;
}

static int GetCasAddr(uchar *CasAddr, uchar *notice)
{
	char str[7] = "\0";
	long addr;
	
	do{
		LcdStart();
		printf("  设置级联地址\n");
		if(notice != NULL)
			printf("%s\n", notice);
		if(GetString(str, 5, TYPE_NUMBER) == 0) return 0;
		addr = atol(str);
	}while(CheckRange(addr, 1, 65535) == 0);
	ScanDataTo2Hex(addr, CasAddr, LowToHigh);
	
	return 1;
}

static void SetLVCAddr(void)
{	
	ST_MSG MsgOut, MsgIn;
	uchar data[25], addr[4], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;

	if(GetLVCAddrInput(addr) == 0) return;
	DataLen = FillLVCAddrData(addr, data);
	FillMsgOut(&MsgOut, 0x84, data, DataLen, 0x00, 1, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	/*LcdStart();
	LcdPrintS(1,0,Sending);
	OpenCom(G_COMPort, GBaud);
	Send(PacketOut, PacketOutLen);
	PacketInLen = Receive(PacketIn, 0x16, 0x16, 0, RECVTIMEOUT_SHORT);
	CloseCom(G_COMPort);*/
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if(MsgIn.afn != 0x00)
			PrintCommStat(COMMFAIL);
		else if(memcmp(MsgIn.data + 4, GSetStr, 6) != 0){
			PrintCommStat(SETFAIL);
		}
		else
			PrintCommStat(SETSUC);		
		WaitForESCKey();
		return;
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryLVCAddr(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
		
	DataLen = FillQueryLVCAddrData(data);
	FillMsgOut(&MsgOut, 0x8a, data, DataLen, 0x00, 1, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if(MsgIn.afn != 0x8a){
			PrintCommStat(COMMFAIL);
		}
		/*else if(memcmp(MsgIn.data + 10, GSetStr, 6) != 0){
			PrintCommStat(SETFAIL);
		}*/
		else{
			LcdStart();
			//memcpy(GAddr, MsgIn.data+6, 4);
			CopyLVCAddr(GAddr, MsgIn.data+6, 4);
			GAddr[4] = 0x2c;
			printf("集中器地址为：\n   %02d%02d%02d%02d", BcdToChar(*(MsgIn.data + 7)), BcdToChar(*(MsgIn.data + 6)), BcdToChar(*(MsgIn.data + 9)), BcdToChar(*(MsgIn.data + 8)));
		}
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryVPNMode(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
		
	DataLen = FillQueryLVCInfoData(62, data);
	FillMsgOut(&MsgOut, 0x0a, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x0a) || (GetIntFn(MsgIn.data) != 62)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("虚拟专用网工作模\n式:");
		PrintVPNMode(MsgIn.data + 4);
		printf("永久在线模式重拨\n间隔: %ld秒\n", Scan2HexToData(MsgIn.data + 5, HighToLow));
		printf("被动激活模式重拨\n次数: %d次\n", *(MsgIn.data + 7));
		printf("被动模式无通信自\n动断线时间:%d次", *(MsgIn.data + 8));
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryVPNIdPwd(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], PacketOut[127], PacketIn[255], VPNId[20], VPNPwd[20];
	int PacketOutLen, DataLen, PacketInLen;
		
	DataLen = FillQueryLVCInfoData(16, data);
	FillMsgOut(&MsgOut, 0x0a, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x0a) || (GetIntFn(MsgIn.data) != 16)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("  VPN ID&密码\n");
		GetRecvStr(MsgIn.data + 4, VPNId);
		printf("VPN用户名:\n%s\n", VPNId);
		GetRecvStr(MsgIn.data + 20, VPNPwd);
		printf("VPN密码:\n%s\n", VPNPwd);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryIpPortAPN(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], PacketOut[127], PacketIn[255], RecvAPN[17];
	int PacketOutLen, DataLen, PacketInLen;
		
	DataLen = FillQueryLVCInfoData(3, data);
	FillMsgOut(&MsgOut, 0x0a, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x0a) || (GetIntFn(MsgIn.data) != 3)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("    查询结果\n");
		printf("主用IP:\n%d.%d.%d.%d\n", *(MsgIn.data + 4), *(MsgIn.data+5), *(MsgIn.data+6), *(MsgIn.data+7));
		printf("端口:%ld\n", Scan2HexToData(MsgIn.data + 8, HighToLow));
		printf("备用IP:\n%d.%d.%d.%d\n", *(MsgIn.data + 10), *(MsgIn.data+11), *(MsgIn.data+12), *(MsgIn.data+13));
		printf("端口:%ld\n", Scan2HexToData(MsgIn.data + 14, HighToLow));
		if (GetYesOrNo() == 0) return;
		LcdStart();
		printf("网关地址:\n%d.%d.%d.%d\n", *(MsgIn.data + 16), *(MsgIn.data+17), *(MsgIn.data+18), *(MsgIn.data+19));
		printf("端口:%ld\n", Scan2HexToData(MsgIn.data + 20, HighToLow));
		printf("代理服务器地址:\n%d.%d.%d.%d\n", *(MsgIn.data + 22), *(MsgIn.data+23), *(MsgIn.data+24), *(MsgIn.data+25));
		printf("端口:%ld\n", Scan2HexToData(MsgIn.data + 26, HighToLow));
		GetRecvStr(MsgIn.data + 28, RecvAPN);
		printf("APN:\n%s", RecvAPN);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void QueryPhoneMsgNum(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], PacketOut[127], PacketIn[255], RecvAPN[17];
	int PacketOutLen, DataLen, PacketInLen;
		
	DataLen = FillQueryLVCInfoData(4, data);
	FillMsgOut(&MsgOut, 0x0a, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x0a) || (GetIntFn(MsgIn.data) != 4)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("    短信参数\n");
		printf("主站电话号码:\n");
		PrintPhoneMsgNum(MsgIn.data + 4);
		printf("\n短信中心号码:\n");
		PrintPhoneMsgNum(MsgIn.data + 4 + 8);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void SetVPNMode(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar VPNMode, ReDialTimeOut[3];
	int ReDialTimes, CutOffTimeOut; 
	uchar data[10], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	long TimeOut;
	
	if(GetVPNModeInput(&VPNMode) == 0) return;
	if(GetReDialTimeOutInput(&TimeOut) == 0) return;
	ScanDataTo2Hex(TimeOut, ReDialTimeOut, HighToLow);
	//TestBuf(ReDialTimeOut, 2);
	if(GetReDialTimesInput(&ReDialTimes) == 0) return;
	if(GetCutOffTimeOutInput(&CutOffTimeOut) == 0) return;	
	DataLen = FillSetVPNModeData(62, data, VPNMode, ReDialTimeOut, ReDialTimes, CutOffTimeOut);
	FillMsgOut(&MsgOut, 0x04, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
			PrintCommStat(SETFAIL);
		else
			PrintCommStat(SETSUC);
		WaitForESCKey();
		return;
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}	
}

static void SetVPNIdPwd(void)
{
	uchar ID[16], Pwd[16];
	ST_MSG MsgOut, MsgIn;
	uchar data[40], addr[4], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;
	
	if(GetVPNID(ID) == 0) return;
	if(GetVPNPwd(Pwd) == 0) return;
	DataLen = FillSetVPNIdPwdData(16, data, ID, Pwd);
	//TestBuf(data, DataLen);
	FillMsgOut(&MsgOut, 0x04, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
			PrintCommStat(SETFAIL);
		else
			PrintCommStat(SETSUC);
		WaitForESCKey();
		return;
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}	
}

static void SetIPPortAPN(void)
{
	uchar MainIpPort[6], StandbyIpPort[6], GatewayPort[6], AgencyPort[6], APN[16];
	ST_MSG MsgOut, MsgIn;
	uchar data[50], addr[4], PacketOut[128], PacketIn[256];
	int PacketOutLen, DataLen, PacketInLen;
	
	if(GetIpPort(MainIpPort, "   主IP及端口") == 0) return;
	//TestBuf(MainIpPort, 6);
	if(GetIpPort(StandbyIpPort, "  备用IP及端口") == 0) return;
	//TestBuf(StandbyIpPort, 6);
	if(GetIpPort(GatewayPort, "  网关IP及端口") == 0) return;
	//TestBuf(GatewayPort, 6);
	if(GetIpPort(AgencyPort, "   代理服务器") == 0) return;
	//TestBuf(AgencyPort, 6);
	if(GetAPN(APN, sizeof(APN)) == 0) return;
	//TestBuf(APN, 16);
	DataLen = FillSetIpPortAPNData(3, data, MainIpPort, StandbyIpPort, GatewayPort, AgencyPort, APN);
	FillMsgOut(&MsgOut, 0x04, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
			PrintCommStat(SETFAIL);
		else
			PrintCommStat(SETSUC);
		WaitForESCKey();
		return;
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}	
}

static void SetPhoneMsgNum(void)
{
	uchar PhoneNum[10], MsgNum[10];
	ST_MSG MsgOut, MsgIn;
	uchar data[25], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
	
	if(GetPhoneNum(PhoneNum) == 0) return;
	if(GetMsgNum(MsgNum) == 0) return;
	DataLen = FillSetPhoneMsgNumData(4, data, PhoneNum, MsgNum);
	FillMsgOut(&MsgOut, 0x04, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
			PrintCommStat(SETFAIL);
		else
			PrintCommStat(SETSUC);
		WaitForESCKey();
		return;
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}	
}

static void SetUpCommParam(void)
{
	void (*p[])(void) = {SetVPNMode, SetVPNIdPwd, SetIPPortAPN, SetPhoneMsgNum};
	const uchar *buf[4];

	buf[0] = SetVPNModeStr;
	buf[1] = SetVPNIdPwdStr;
	buf[2] = SetIPPortAPNStr;
	buf[3] = SetPhoneMsgNumStr;
	
	InitMenu(buf, p, 4, 0);
}

static void RdUpCommParam(void)
{
	void (*p[])(void) = {QueryVPNMode, QueryVPNIdPwd, QueryIpPortAPN, QueryPhoneMsgNum};
	const uchar *buf[4];

	buf[0] = QueryVPNModeStr;
	buf[1] = QueryVPNIdPwdStr;
	buf[2] = QueryIpPortAPNStr;
	buf[3] = QueryPhoneMsgNumStr;
	
	InitMenu(buf, p, 4, 0);
}

static void SetCascadeLVCAddr(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[25], PacketOut[127], PacketIn[255];
	uchar Cas1Addr[2], Cas1Code[2], Cas2Addr[2], Cas2Code[2], Cas3Addr[2], Cas3Code[2], Cas4Addr[2], Cas4Code[2];
	int PacketOutLen, DataLen, PacketInLen;
	
	if(GetCasCode(Cas1Code, "级联集中器1行政\n区划码:") == 0) return;
	if(GetCasAddr(Cas1Addr, "级联集中器1终端\n地址:") == 0) return;
	if(GetCasCode(Cas2Code, "级联集中器2行政\n区划码:") == 0) return;
	if(GetCasAddr(Cas2Addr, "级联集中器2终端\n地址:") == 0) return;
	if(GetCasCode(Cas3Code, "级联集中器3行政\n区划码:") == 0) return;
	if(GetCasAddr(Cas3Addr, "级联集中器3终端\n地址:") == 0) return;
	if(GetCasCode(Cas4Code, "级联集中器4行政\n区划码:") == 0) return;
	if(GetCasAddr(Cas4Addr, "级联集中器4终端\n地址:") == 0) return;
	DataLen = FillSetCasAddrData(9, data, Cas1Code, Cas1Addr, Cas2Code, Cas2Addr, Cas3Code, Cas3Addr, Cas4Code, Cas4Addr);
	FillMsgOut(&MsgOut, 0x84, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
			PrintCommStat(SETFAIL);
		else
			PrintCommStat(SETSUC);
		WaitForESCKey();
		return;
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void SetCasLVCCommParam(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[15], PacketOut[127], PacketIn[255];
	uchar CommCtrlWord;
	int RecvCascadeTimeOut, RecvTimeOut, DelayTimeOut, ResponseTimeOut, ReSendTimes;
	int PacketOutLen, DataLen, PacketInLen;
	
	//if(GetCommCtrlWordInput(&CommCtrlWord) == 0) return;
	CommCtrlWord = 0xcb;  //集中器中此通信控制字不允许改，所以此处直接写死，也不允许用户输入通信控制字
	if(GetRecvCascadeTimeOut(&RecvCascadeTimeOut) == 0) return;
	if(GetRecvTimeOut(&RecvTimeOut) == 0) return;
	if(GetDelayTimeOut(&DelayTimeOut) == 0) return;
	if(GetResponseTimeOut(&ResponseTimeOut) ==0) return;
	if(GetReSendTimes(&ReSendTimes) ==0) return;
	DataLen = FillSetLVCCommParamData(10, data, CommCtrlWord, RecvCascadeTimeOut, RecvTimeOut, DelayTimeOut, ResponseTimeOut, ReSendTimes);
	FillMsgOut(&MsgOut, 0x84, data, DataLen, 0x01, 0, 1, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x00) || (GetIntFn(MsgIn.data) != 1))
			PrintCommStat(SETFAIL);
		else
			PrintCommStat(SETSUC);
		WaitForESCKey();
		return;
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void RdCasLVCCommParam(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
		
	DataLen = FillQueryLVCInfoData(10, data);
	FillMsgOut(&MsgOut, 0x8a, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x8a) || (GetIntFn(MsgIn.data) != 10)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("通信控制字结果:\n");
		PrintCommCtrlWord(MsgIn.data + 4);
		printf("主LVC接收级联LVC\n等待报文超时时间\n%d(100ms)", *(MsgIn.data + 5));
		if (GetYesOrNo() == 0) return;
		LcdStart();
		printf("接收等待字节超时\n时间:%d(10ms)\n", *(MsgIn.data + 6));
		printf("集中器允许发送延\n时时间:%d分钟\n", *(MsgIn.data + 7));
		printf("集中器等待从动站\n响应的超时时间:\n%d秒\n重发次数:%d", *(MsgIn.data + 8), *(MsgIn.data + 9));
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

static void RdCascadeLVCAddr(void)
{
	ST_MSG MsgOut, MsgIn;
	uchar data[5], PacketOut[127], PacketIn[255];
	int PacketOutLen, DataLen, PacketInLen;
		
	DataLen = FillQueryLVCInfoData(9, data);
	FillMsgOut(&MsgOut, 0x8a, data, DataLen, 0x01, 0, 0, 0);
	PacketOutLen = pack(PacketOut, 128, &MsgOut);
	PacketInLen = MsgComm(PacketOut, PacketOutLen, PacketIn, RECVTIMEOUT_SHORT);
	if(PacketInLen > 0){
		if (CheckPacketIn(PacketIn, PacketInLen) == 0){
			PrintCommStat(RECVPACKERR);
			WaitForESCKey(); return;
		}
		unpack(PacketIn, PacketInLen, &MsgIn);
		if((MsgIn.afn != 0x8a) || (GetIntFn(MsgIn.data) != 9)){
			PrintCommStat(COMMFAIL);
			WaitForESCKey();
			return;
		}
		LcdStart();
		printf("级联集中器1行政\n区划码:");
		PrintBuf(MsgIn.data+4, 2, ORDER, NOSPAC);
		printf("\n级联集中器1终端\n地址:");
		PrintCascadeLVCAddr(MsgIn.data+6, 2, NOTORDER);
		printf("\n级联集中器2行政\n区划码:");
		PrintBuf(MsgIn.data+8, 2, ORDER, NOSPAC);
		printf("\n级联集中器2终端\n地址:");
		PrintCascadeLVCAddr(MsgIn.data+10, 2, NOTORDER);
		if (GetYesOrNo() == 0) return;
		LcdStart();
		printf("级联集中器3行政\n区划码:");
		PrintBuf(MsgIn.data+12, 2, ORDER, NOSPAC);
		printf("\n级联集中器3终端\n地址:");
		PrintCascadeLVCAddr(MsgIn.data+14, 2, NOTORDER);
		printf("\n级联集中器4行政\n区划码:");
		PrintBuf(MsgIn.data+16, 2, ORDER, NOSPAC);
		printf("\n级联集中器4终端\n地址:");
		PrintCascadeLVCAddr(MsgIn.data+18, 2, NOTORDER);
		WaitForESCKey();
	}
	else{
		PrintCommStat(COMMFAIL);
		WaitForESCKey();
	}
}

void LVCParam(void)
{
	void (*p[])(void) = {SetLVCAddr, QueryLVCAddr, SetUpCommParam, RdUpCommParam, SetCasLVCCommParam, RdCasLVCCommParam, SetCascadeLVCAddr, RdCascadeLVCAddr};
	const uchar *buf[8];

	buf[0] = SetLVCAddrStr;
	buf[1] = QueryLVCAddrStr;
	buf[2] = SetUpCommParamStr;
	buf[3] = RdUpCommParamStr;
	buf[4] = SetCasLVCCommParamStr;
	buf[5] = RdCasLVCCommParamStr;
	buf[6] = SetCascadeLVCAddrStr;
	buf[7] = RdCascadeLVCAddrStr;
	
	InitMenu(buf, p, 8, 0);
}